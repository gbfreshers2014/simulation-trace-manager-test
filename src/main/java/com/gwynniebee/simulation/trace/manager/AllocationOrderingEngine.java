/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.simulation.trace.manager;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Set;
import java.util.HashSet;
import java.util.Arrays;

import org.apache.commons.io.IOUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gwynniebee.async.job.api.AsyncJobRequestMap;
import com.gwynniebee.async.job.api.AsyncJobStatus;
import com.gwynniebee.async.job.helper.AsyncJobResourceBase;
import com.gwynniebee.matching.result.MatchingResult;
import com.gwynniebee.matching.result.RIDAllocation;
import com.gwynniebee.matching.setup.MatchingSetup;
import com.gwynniebee.matching.setup.RIDAllocationRequest;
import com.gwynniebee.matching.setup.UserCloset;
import com.gwynniebee.matching.setup.SKUSupply;
import com.gwynniebee.simulation.trace.manager.AllocationOrderingProcess;
import com.gwynniebee.simulation.trace.manager.SimulationTraceManagerProperties;
import com.gwynniebee.simulation.trace.object.MatchingInstance;
import com.gwynniebee.simulation.trace.object.CustomerAllocationGarmentMultiColors;
import com.gwynniebee.simulation.trace.store.AWSS3Client;
import com.gwynniebee.simulation.trace.store.InputStreamProvider;
import com.gwynniebee.simulation.trace.store.S3InputStreamProvider;

/**
 * @author dj
 */
public class AllocationOrderingEngine {
    private static final Logger LOG = LoggerFactory.getLogger(AllocationOrderingEngine.class);

    public static final String LATEST_MATCHING_SETUP_FILE_KEYS = "latestMatchingSetupFileKeys";
    public static final String LATEST_MATCHING_RESULT_FILE_KEYS = "latestMatchingResultFileKeys";
    private static final int ESTIMATED_SECONDS = 60 * 15;
    private String nameSpacePrefix = "";

    // support namespace
    public static final String ASYNC_PARAM_NAMESPACE_ID = "namespaceId";

    /**
     * @param requestClass
     */
    public AllocationOrderingEngine() {
        super();
        // TODO Auto-generated constructor stub
        setNameSpacePrefix("");
    }

    public void allocationOrderingEngineJob(String nameSpacePrefix) {
        // algorithm knobs
        double costWeightOrdering = 0.0;
        double costWeightTimeInQueue = 1.0;
        boolean twoPassesAllocationEnabled = false;
        String balanceDemandAlgorithm = AllocationOrderingProcess.BALANCE_DEMAND_ALGORITHMS_TIME;

        if (AllocationOrderingEngineProperties.getInstance().getTwoPassesAllocationEnabled().equals(AllocationOrderingProcess.KNOB_YES_KEY)) {
            twoPassesAllocationEnabled = true;
            LOG.info("Two passes allocation is enabled");
        } else {
            LOG.info("Two passes allocation is disabled");
        }
   LOG.info("currently balanceDemandAlgorithm can only be \"time\" when two passes are enabled");     // currently balanceDemandAlgorithm can only be "time" when two passes
        // are enabled
        if (twoPassesAllocationEnabled == true) {
            LOG.info("Balance demand algorithm = " + balanceDemandAlgorithm);
        } else {
            LOG.info("Two passes allocation is disabled, therefore balance demand algorithm has no effect");
        }
        try {
            costWeightOrdering = Double.parseDouble(AllocationOrderingEngineProperties.getInstance().getCostFunctionWeightOrdering());
        } catch (NumberFormatException e) {
            costWeightOrdering = 0.0;
            LOG.info("Invalid cost weight for ordering. Set to 0.0");
        }
        LOG.info("Cost weight for ordering = " + costWeightOrdering);
        try {
            costWeightTimeInQueue = Double.parseDouble(AllocationOrderingEngineProperties.getInstance().getCostFunctionWeightTimeInQueue());
        } catch (NumberFormatException e) {
            costWeightTimeInQueue = 0.0;
            LOG.info("Invalid cost weight for waiting time in queue. Set to 0.0");
        }
        LOG.info("Cost weight for waiting time in queue = " + costWeightTimeInQueue);

        try {
            LOG.info("Start fetching matching setup");
            MatchingSetup matchingSetup = fetchMatchingSetup();
            LOG.info("End fetching matching setup");
            if (matchingSetup != null) {
                MatchingResult matchingResult = null;

                if (twoPassesAllocationEnabled == false) {
                    // run one-pass algorithm
                    LOG.info("Start running one-pass algorithm");
                    AllocationOrderingProcess aop = new AllocationOrderingProcess();
                    matchingResult =
                            aop.run(nameSpacePrefix, matchingSetup, costWeightOrdering, costWeightTimeInQueue,
                                    AllocationOrderingProcess.STEP_BASELINE, balanceDemandAlgorithm);
                    LOG.info("End of running one-pass algorithm");
                } else {
                    // run two-pass algorithm
                    LOG.info("Start running two-pass algorithm");
                    AllocationOrderingProcess aop1 = new AllocationOrderingProcess();
                    LOG.info("Start step 1 in two-pass algorithm");
                    MatchingResult matchingResultStep1 =
                            aop1.run(nameSpacePrefix, matchingSetup, costWeightOrdering, costWeightTimeInQueue,
                                    AllocationOrderingProcess.STEP_ENFORCE_FULL_ALLOCATION, balanceDemandAlgorithm);
                    LOG.info("End step 1 in two-pass algorithm");

                    LOG.info("Start adjusting matching setup after step 1");
                    MatchingSetup matchingSetupAdjusted = adjustMatchingSetupByMatchingResult(matchingSetup, matchingResultStep1);
                    LOG.info("End adjusting matching setup after step 1");

                    AllocationOrderingProcess aop2 = new AllocationOrderingProcess();
                    LOG.info("Start step 2 in two-pass algorithm");
                    MatchingResult matchingResultStep2 =
                            aop2.run(nameSpacePrefix, matchingSetupAdjusted, costWeightOrdering, costWeightTimeInQueue,
                                    AllocationOrderingProcess.STEP_BASELINE, balanceDemandAlgorithm);
                    LOG.info("End step 2 in two-pass algorithm");

                    LOG.info("Start merging matching restuls from two steps");
                    matchingResult = mergeMatchingResult(matchingResultStep1, matchingResultStep2);
                    LOG.info("End merging matching results from two steps");

                    LOG.info("Start enhancement of ensuring a single color of the same style-size in each RID");
                    int retry = 0;
                    try {
                        retry =
                                Integer.parseInt(AllocationOrderingEngineProperties.getInstance()
                                        .getReallocationTriesForGarmentSingleColor());
                    } catch (Exception e) {
                        LOG.info("Invalid reallocation retry configuration for garment single color: " + e.getMessage());
                        LOG.info("Skip.");
                        retry = 0;
                    }
                    // reallocation to ensure garment single color
                    int tries;
                    for (tries = 0; tries < retry; tries++) {
                        LOG.info("Reallocation try #" + tries);
                        MatchingInstance mi = new MatchingInstance();
                        mi.setMatchingResult(matchingResult);
                        mi.setMatchingSetup(matchingSetup);
                        LOG.info("Start adjusting matching setup and matching result");
                        MatchingInstance adjustedMI = adjustMatchingSetupByGarmentSingleColor(mi, true);
                        LOG.info("End adjusting matching setup and matching result");
                        if (adjustedMI == null) {
                            LOG.info("Nothing to be adjusted for the enhancement. Exit.");
                            break;
                        }
                        AllocationOrderingProcess aop = new AllocationOrderingProcess();
                        LOG.info("Start reallocation for ensuring garment of single color per RID");
                        MatchingResult matchingResultEnhanced =
                                aop.run(nameSpacePrefix, adjustedMI.getMatchingSetup(), costWeightOrdering, costWeightTimeInQueue,
                                        AllocationOrderingProcess.STEP_BASELINE, balanceDemandAlgorithm);
                        LOG.info("End reallocation for ensuring garment of single color per RID");

                        LOG.info("Start merging matching restuls");
                        matchingResult = mergeMatchingResult(adjustedMI.getMatchingResult(), matchingResultEnhanced);
                        LOG.info("End merging matching results");
                    }
                    if (tries > 0) { // have run the enhancement for garment of
                                     // single color per RID
                        // scan matching result to remove different colors of
                        // the same style-size per RID
                        LOG.info("Start final scan of matching results");
                        MatchingInstance mi = new MatchingInstance();
                        mi.setMatchingResult(matchingResult);
                        mi.setMatchingSetup(matchingSetup);
                        MatchingInstance adjustedMI = adjustMatchingSetupByGarmentSingleColor(mi, false);
                        if (adjustedMI != null) {
                            matchingResult = adjustedMI.getMatchingResult();
                        }
                        LOG.info("End final scan of matching results");
                    }

                    int overlyAllocated = 0;
                    int totalRids = 0;
                    int fullyAllocated = 0;
                    int partiallyAllocated = 0;
                    int totalSlots = 0;
                    int fullyAllocatedSlots = 0;
                    int partiallyAllocatedSlots = 0;
                    for (RIDAllocation ra : matchingResult.getNewAllocations()) {
                        totalRids += 1;
                        totalSlots += ra.getSlots();
                        if (ra.getFulfilledSlots() == ra.getSlots()) {
                            fullyAllocated += 1;
                            fullyAllocatedSlots += ra.getSlots();
                        } else if (ra.getFulfilledSlots() > ra.getSlots()) {
                            overlyAllocated += 1;
                            fullyAllocatedSlots += ra.getSlots();
                        } else if (ra.getFulfilledSlots() > 0) {
                            partiallyAllocated += 1;
                            partiallyAllocatedSlots += ra.getSlots();
                        }
                    }
                    double fullAllocationRate = (double) fullyAllocated * 100 / (double) totalRids;
                    double fullAllocationSlotsRate = (double) fullyAllocatedSlots * 100 / (double) totalSlots;
                    double partiallyAllocationRate = (double) partiallyAllocated * 100 / (double) totalRids;
                    double partiallyAllocationSlotsRate = (double) partiallyAllocatedSlots * 100 / (double) totalSlots;
                    LOG.info("Total number of RIDs = " + totalRids);
                    LOG.info("Fully allocated RIDs = " + fullyAllocated + "(" + Double.toString(fullAllocationRate) + "%)");
                    LOG.info("Partially allocated RIDs = " + partiallyAllocated + "(" + Double.toString(partiallyAllocationRate) + "%)");
                    LOG.info("Overly allocated RIDs = " + overlyAllocated);
                    LOG.info("Total number of slots = " + totalSlots);
                    LOG.info("Fully allocated slots = " + fullyAllocatedSlots + "(" + Double.toString(fullAllocationSlotsRate) + "%)");
                    LOG.info("Partially allocated slots = " + partiallyAllocated + "(" + Double.toString(partiallyAllocationSlotsRate)
                            + "%)");
                    LOG.info("End running two-pass algorithm");
                }
                if (matchingResult != null) {
                    LOG.info("Start uploading allocation results to S3");
                    uploadResultJsonToS3(matchingResult);
                    LOG.info("End uploading allocation results to S3");
                } else {
                    LOG.info("No matching result was uploaded on S3");
                }
            } else {
                LOG.info("No matching setup was fetched");
            }
        } catch (Exception e) {
            LOG.error("allocationOrderingEngineJob: " + e.getMessage());
            LOG.info("allocationOrderingEngineJob Failed");
            e.printStackTrace();
        }
    }

    public void setNameSpacePrefix(String nameSpaceId) {
        if ((nameSpaceId == null) || ((nameSpaceId != null) && (nameSpaceId.isEmpty()))) {
            this.nameSpacePrefix = "";
        } else {
            try {
                int id = Integer.parseInt(nameSpaceId);
                if (id > 0) {
                    this.nameSpacePrefix = "ns/" + nameSpaceId;
                } else {
                    this.nameSpacePrefix = "";
                }
            } catch (Exception e) {
                this.nameSpacePrefix = "";
                LOG.info("Namespace ID is not vaild. Treat it as default\n");
            }
        }
    }

    public String getNameSpacePrefix() {
        return this.nameSpacePrefix;
    }

    /**
     * @return matchingSetup
     * @throws IOException IOException
     */
    public MatchingSetup fetchMatchingSetup() throws IOException {
        // fetch from matching setup landscape in S3
        boolean result = true;
        // String location = LATEST_MATCHING_SETUP_FILE_KEYS;
        String objectName = AllocationOrderingEngineProperties.getInstance().getMatchingSetupS3ObjectName();
        String simulationLocation =
                SimulationTraceManagerProperties.getInstance().getSimulationS3ModuleName() + "/"
                        + SimulationTraceManagerProperties.getInstance().getNamespaceId();
        objectName = simulationLocation + "/" + objectName;
        String location = null;
        LOG.info("Matching setup location : " + objectName);
        InputStreamProvider provider =
                new S3InputStreamProvider(AllocationOrderingEngineProperties.getInstance().getMatchingSetupS3BucketName());
        InputStream is = null;
        // fetch file name in LATEST_MATCHING_SETUP_FILE_KEYS
        if (true) {
            location = objectName + "/" + LATEST_MATCHING_SETUP_FILE_KEYS;
            try {
                is = provider.getInputStream(location);
                LOG.info("acutual matching setup " + location);
                location = IOUtils.toString(new InputStreamReader(is, "UTF-8"));
            } catch (Exception e) {
                LOG.error("fetchMatchingSetup: " + e.getMessage());
                LOG.info("Fetching matching setup FAILED");
                result = false;
            } finally {
                if (is != null) {
                    IOUtils.closeQuietly(is);
                }
            }
        }

        InputStream matchingSetupInput = null;
        MatchingSetup matchingSetup = null;

        if (result) {
            // fetch matching setup file
            try {
                try {
                    location = objectName + "/" + location;
                    matchingSetupInput = provider.getInputStream(location);
                } catch (Exception e) {
                    LOG.error("fetchMatchingSetup: " + e.getMessage());
                    matchingSetupInput = null;
                }
                if (matchingSetupInput != null) {
                    matchingSetup = new ObjectMapper().readValue(matchingSetupInput, MatchingSetup.class);
                    LOG.info("Fetched matching setup data frp," + location);
                    // LOG.info(new
                    // ObjectMapper().writeValueAsString(matchingSetup));
                } else {
                    LOG.info("Cannot fetch matching setup from S3");
                }
            } finally {
                if (matchingSetupInput != null) {
                    IOUtils.closeQuietly(matchingSetupInput);
                }
            }
        }

        return matchingSetup;
    }

    /**
     * @param matchingSetup matching setup
     * @param matchingResult matching result
     * @return merge the new allocations from matching results 1 and 2
     */
    public MatchingSetup adjustMatchingSetupByMatchingResult(MatchingSetup matchingSetup, MatchingResult matchingResult) {
        MatchingSetup matchingSetupAdjusted = null;

        // remove the RIDs in matchingResult from matchingSetup
        // remove the allocated SKUs in matchingResult from matchingSetup

        if (matchingResult == null) { // nothing to be adjusted
            return matchingSetup;
        }

        if (matchingSetup != null) {
            matchingSetupAdjusted = new MatchingSetup();
            matchingSetupAdjusted.setDateTimeKey(matchingSetup.getDateTimeKey());
            matchingSetupAdjusted.setReservedAllocations(matchingSetup.getReservedAllocations());
            matchingSetupAdjusted.setUserClosets(matchingSetup.getUserClosets());

            Set<Integer> allocatedRIDs = new HashSet<Integer>();
            Map<String, Integer> allocatedSKUCount = new HashMap<String, Integer>();
            for (RIDAllocation a : matchingResult.getNewAllocations()) {
                // collect allocated RIDs
                allocatedRIDs.add(a.getRid());
                // collect allocated SKUs and count
                for (String s : a.getAllocatedSKUs()) {
                    if (allocatedSKUCount.containsKey(s)) {
                        allocatedSKUCount.put(s, allocatedSKUCount.get(s) + 1);
                    } else {
                        allocatedSKUCount.put(s, 1);
                    }
                }
            }

            // remove allocatedRIDs from matching setup
            List<RIDAllocationRequest> adjustedRIDAllocationRequests = new ArrayList<RIDAllocationRequest>();
            for (RIDAllocationRequest ar : matchingSetup.getAllocationRequests()) {
                if (!allocatedRIDs.contains(ar.getRid())) {
                    adjustedRIDAllocationRequests.add(ar);
                }
            }
            matchingSetupAdjusted.setAllocationRequests(adjustedRIDAllocationRequests);

            // remove allocate SKUs from matching setup
            List<SKUSupply> adjustedSupplies = new ArrayList<SKUSupply>();
            for (SKUSupply ss : matchingSetup.getSupplies()) {
                if (!allocatedSKUCount.containsKey(ss.getSku())) {
                    adjustedSupplies.add(ss);
                } else {
                    // adjust availability count if the sku was allocated
                    if (ss.getAtGwynnieBee() >= allocatedSKUCount.get(ss.getSku())) {
                        ss.setAtGwynnieBee(ss.getAtGwynnieBee() - allocatedSKUCount.get(ss.getSku()));
                    } else if (ss.getAtGwynnieBee() + ss.getAtLaudary() >= allocatedSKUCount.get(ss.getSku())) {
                        ss.setAtLaudary(ss.getAtGwynnieBee() + ss.getAtLaudary() - allocatedSKUCount.get(ss.getSku()));
                        ss.setAtGwynnieBee(0);
                    } else {
                        ss.setAtLaudary(0);
                        ss.setAtGwynnieBee(0);
                    }
                    // keep the sku spply only when it has availability after
                    // adjustment
                    if (ss.getAtGwynnieBee() + ss.getAtLaudary() > 0) {
                        adjustedSupplies.add(ss);
                    }
                }
            }
            matchingSetupAdjusted.setSupplies(adjustedSupplies);
        }
        return matchingSetupAdjusted;
    }

    /**
     * @param matchingInstance matching instance that includes setup and result
     * @return adjusted matchingInstance
     */
    public MatchingInstance adjustMatchingSetupByGarmentSingleColor(MatchingInstance matchingInstance, boolean reallocatePartialFulfillment) {
        MatchingSetup matchingSetup = null;
        MatchingSetup matchingSetupAdjusted = null;
        MatchingResult matchingResult = null;
        MatchingResult matchingResultAdjusted = null;
        MatchingInstance matchingInstanceAdjusted = null;

        if (matchingInstance == null
                || (matchingInstance != null && (matchingInstance.getMatchingResult() == null || matchingInstance.getMatchingSetup() == null))) {
            return matchingInstanceAdjusted; // nothing to do
        }

        matchingSetup = matchingInstance.getMatchingSetup();
        matchingResult = matchingInstance.getMatchingResult();
        if (matchingSetup == null || matchingResult == null) {
            return matchingInstanceAdjusted; // nothing to do
        }

        if (matchingSetup.getAllocationRequests() == null || matchingSetup.getSupplies() == null || matchingSetup.getUserClosets() == null
                || matchingResult.getNewAllocations() == null) {
            return matchingInstanceAdjusted; // nothing to do
        }

        try {
            matchingSetupAdjusted = new MatchingSetup();
            matchingResultAdjusted = new MatchingResult();

            matchingSetupAdjusted.setDateTimeKey(matchingSetup.getDateTimeKey());
            matchingSetupAdjusted.setReservedAllocations(matchingSetup.getReservedAllocations());
            matchingResultAdjusted.setDateTimeKey(matchingSetup.getDateTimeKey());
            matchingResultAdjusted.setReservedAllocations(matchingSetup.getReservedAllocations());
            matchingResultAdjusted.setMatchingEngineMetaData(matchingResult.getMatchingEngineMetaData());

            // 1. Scan matching setup and result to record certain information
            // for later adjustment

            Map<String, Integer> uuidToRid = new HashMap<String, Integer>(); // record
                                                                             // uuid
                                                                             // to
                                                                             // Rid
                                                                             // lookup
                                                                             // (assuming
                                                                             // one
                                                                             // uuid
                                                                             // has
                                                                             // only
                                                                             // one
                                                                             // rid
            Map<String, Integer> allocatedSKUCount = new HashMap<String, Integer>(); // record
                                                                                     // the
                                                                                     // count
                                                                                     // of
                                                                                     // allocated
                                                                                     // SKU
            // record SKUs for {rid, Style, Size}
            Map<Integer, Map<String, CustomerAllocationGarmentMultiColors>> allocatedRidStyleSize =
                    new HashMap<Integer, Map<String, CustomerAllocationGarmentMultiColors>>();

            for (RIDAllocation a : matchingResult.getNewAllocations()) {
                int rid = a.getRid();

                // 1.1 collect style-size and skus per rid
                if (a.getAllocatedSKUs() == null) {
                    continue;
                }
                for (String s : a.getAllocatedSKUs()) {
                    String[] skuFields = s.split("-"); // split sku
                    if (skuFields.length == 3) { // only for a SKU in
                                                 // style-color-size form
                        String style = skuFields[0];
                        String size = skuFields[2];
                        String styleSize = style + "-" + size;
                        Map<String, CustomerAllocationGarmentMultiColors> garments;
                        CustomerAllocationGarmentMultiColors garment;
                        if (allocatedRidStyleSize.containsKey(rid)) {
                            garments = allocatedRidStyleSize.get(rid);
                        } else {
                            garments = new HashMap<String, CustomerAllocationGarmentMultiColors>();
                        }

                        if (garments.containsKey(styleSize)) {
                            garment = garments.get(styleSize);
                            garment.addMultiColorSKU(s);
                        } else {
                            Set<String> skus = new HashSet<String>();
                            skus.add(s);
                            garment = new CustomerAllocationGarmentMultiColors(a.getUuid(), rid, style, size, null, null, skus);
                        }
                        garments.put(styleSize, garment);
                        allocatedRidStyleSize.put(rid, garments);
                    }

                    // 1.2 collect allocated SKUs and count
                    if (allocatedSKUCount.containsKey(s)) {
                        allocatedSKUCount.put(s, allocatedSKUCount.get(s) + 1);
                    } else {
                        allocatedSKUCount.put(s, 1);
                    }
                }
            }

            // 1.3 check whether we have allocations that have multiple colors
            // of the same style-size
            // one rid can have multiple {style, size} that has multiple colors
            // 1.4 build uuid to Rid map for only the rids that have multiple
            // colors for the same style-size
            Map<Integer, Map<String, CustomerAllocationGarmentMultiColors>> ridStyleSizeMultiColors = null;
            for (int rid : allocatedRidStyleSize.keySet()) {
                Map<String, CustomerAllocationGarmentMultiColors> garments = allocatedRidStyleSize.get(rid);
                Map<String, CustomerAllocationGarmentMultiColors> adjustedGarments = null;
                for (String styleSize : garments.keySet()) {
                    Set<String> skus = garments.get(styleSize).getMultiColorSKUs();
                    if (skus.size() > 1) { // multiple colors
                        String uuid = garments.get(styleSize).getUuid();
                        uuidToRid.put(uuid, rid); // 1.4 uuid to Rid map
                        String style = garments.get(styleSize).getStyle();
                        String size = garments.get(styleSize).getSize();
                        CustomerAllocationGarmentMultiColors garment =
                                new CustomerAllocationGarmentMultiColors(uuid, rid, style, size, null, null, skus);
                        if (adjustedGarments == null) {
                            adjustedGarments = new HashMap<String, CustomerAllocationGarmentMultiColors>();
                        }
                        adjustedGarments.put(styleSize, garment);
                    }
                }
                if (adjustedGarments != null) { // there are multi-color skus
                                                // for the same style-size
                    if (ridStyleSizeMultiColors == null) {
                        ridStyleSizeMultiColors = new HashMap<Integer, Map<String, CustomerAllocationGarmentMultiColors>>();
                    }
                    ridStyleSizeMultiColors.put(rid, adjustedGarments);
                }
            }

            if (ridStyleSizeMultiColors == null) { // there are no multi-color
                                                   // skus in allocations per
                                                   // uuid
                return matchingInstanceAdjusted; // nothing to do then return
            }

            // continue when there are multi-color skus in allocations of some
            // rid

            // 1.5 collect SKU availability
            // we use the allocated SKU count and available SKU count as a
            // heuristic in picking single color of a garment
            Map<String, Integer> availableSKUCount = new HashMap<String, Integer>();
            for (SKUSupply ss : matchingSetup.getSupplies()) {
                String sku = ss.getSku();
                int available = ss.getAtGwynnieBee() + ss.getAtLaudary();
                if (availableSKUCount.containsKey(sku)) {
                    availableSKUCount.put(sku, availableSKUCount.get(sku) + available);
                } else {
                    availableSKUCount.put(sku, available);
                }
            }

            // 1.6 adjust user closets in matching setup
            // scan the closets for the allocated RIDs that have garments with
            // multiple colors;
            // remove these garments that have the same style-size with multiple
            // colors for next re-allocation
            List<UserCloset> adjustedUserClosets = new ArrayList<UserCloset>();
            for (UserCloset uc : matchingSetup.getUserClosets()) {
                String uuid = uc.getUuid();
                if (uuidToRid.containsKey(uuid)) {
                    // only adjust closets that have allocated RIDs which
                    // includes multiple colors for the same style-size
                    Map<String, Integer> orders = uc.getOrders();
                    Map<String, Integer> adjustedOrders = null;
                    if (orders != null) {
                        adjustedOrders = new HashMap<String, Integer>();
                    }
                    Set<String> skus = uc.getSkus();
                    Set<String> adjustedSkus = new HashSet<String>();
                    String styleSize = "";
                    for (String s : skus) {
                        String[] skuFields = s.split("-"); // split sku
                        if (skuFields.length == 3) { // only for a SKU in
                                                     // style-color-size form
                            String style = skuFields[0];
                            String size = skuFields[2];
                            styleSize = style + "-" + size;
                        }
                        int rid = uuidToRid.get(uuid);
                        if (!ridStyleSizeMultiColors.get(rid).containsKey(styleSize)) {
                            // keep only the skus that do not have same
                            // style-size with multiple colors in current
                            // allocation
                            adjustedSkus.add(s);
                            if (orders != null && orders.containsKey(s)) {
                                adjustedOrders.put(s, orders.get(s));
                            }
                        } else {
                            // update ridStyleSizeMultiColors :
                            // a. order ids of skus that have multiple colors
                            // b. availability of the skus that have multiple
                            // colors
                            // order ids and availability after current
                            // allocation will be used to decide which sku is
                            // kept in the
                            // allocation when we have to remove the rest of the
                            // skus that share same style-size with different
                            // colors
                            for (String sku : ridStyleSizeMultiColors.get(rid).get(styleSize).getMultiColorSKUs()) {
                                if (orders != null && orders.containsKey(sku)) {
                                    ridStyleSizeMultiColors.get(rid).get(styleSize).addMultiColorSKUOrderId(sku, orders.get(sku));
                                } else {
                                    ridStyleSizeMultiColors.get(rid).get(styleSize).addMultiColorSKUOrderId(sku, Integer.MAX_VALUE);
                                }
                                if (availableSKUCount.containsKey(sku)) {
                                    ridStyleSizeMultiColors.get(rid).get(styleSize)
                                            .addMultiColorSKUsAvailable(sku, (availableSKUCount.get(sku) - allocatedSKUCount.get(sku)));
                                } else {
                                    ridStyleSizeMultiColors.get(rid).get(styleSize).addMultiColorSKUsAvailable(sku, 0);
                                }
                            }
                        }
                    }

                    UserCloset adjustedUc = new UserCloset();
                    adjustedUc.setUuid(uuid);
                    adjustedUc.setOrders(adjustedOrders);
                    adjustedUc.setSkus(adjustedSkus);
                    adjustedUserClosets.add(adjustedUc);
                } else {
                    adjustedUserClosets.add(uc);
                }
            }
            matchingSetupAdjusted.setUserClosets(adjustedUserClosets);

            // 1.7 sort skus of the same style-size and multiple colors firstly
            // by order id, then by availability
            for (int rid : ridStyleSizeMultiColors.keySet()) {
                Map<String, CustomerAllocationGarmentMultiColors> garments = ridStyleSizeMultiColors.get(rid);
                for (String styleSize : garments.keySet()) {
                    // sort skus of multiple colors firstly by ascending order
                    // of order id then by descending order of availability
                    garments.get(styleSize).sortSKUs();
                    LOG.info("RID("
                            + rid
                            + ") needs to remove "
                            + Arrays.toString(garments.get(styleSize).getMultiColorSKUs()
                                    .toArray(new String[garments.get(styleSize).getMultiColorSKUs().size()]))
                            + " from its allocation to ensure single color of the garment");
                }
            }

            // 1.8 collect allocated RIDs and slots for later adjustment of
            // allocation requests
            // 1.9 adjust new allocations in matching result
            // 1.10 adjust allocated SKU count
            Map<Integer, Integer> allocatedRIDsSlots = new HashMap<Integer, Integer>();
            List<RIDAllocation> adjustedNewAllocations = new ArrayList<RIDAllocation>();
            Map<String, Integer> adjustedAllocatedSKUCount = new HashMap<String, Integer>(); // record
                                                                                             // the
                                                                                             // count
                                                                                             // of
                                                                                             // allocated
                                                                                             // SKU
                                                                                             // after
                                                                                             // adjustment
            for (RIDAllocation a : matchingResult.getNewAllocations()) {
                int rid = a.getRid();
                if (ridStyleSizeMultiColors.containsKey(rid)) { // allocation
                                                                // has multiple
                                                                // colors for
                                                                // the same
                                                                // style-size
                    Map<String, CustomerAllocationGarmentMultiColors> garments = ridStyleSizeMultiColors.get(rid);
                    int slotsToBeReleased = 0;
                    Set<String> skusToBeReleased = new HashSet<String>();

                    // 1.9 adjust new allocations in matching result
                    RIDAllocation adjustedAllocation = new RIDAllocation();
                    adjustedAllocation.setUuid(a.getUuid());
                    adjustedAllocation.setRid(rid);
                    for (String styleSize : garments.keySet()) { // count
                        Set<String> skus = garments.get(styleSize).getMultiColorSKUs();
                        slotsToBeReleased += skus.size(); // skus will be
                                                          // released
                        for (String s : skus) {
                            skusToBeReleased.add(s);
                        }
                    }
                    allocatedRIDsSlots.put(rid, a.getFulfilledSlots() - slotsToBeReleased);
                    // adjust allocated skus
                    Set<String> adjustedAllocatedSKUs = a.getAllocatedSKUs();
                    for (Iterator<String> i = adjustedAllocatedSKUs.iterator(); i.hasNext();) {
                        String sku = i.next();
                        if (skusToBeReleased.contains(sku)) {
                            i.remove();
                        }
                    }
                    adjustedAllocation.setAllocatedSKUs(adjustedAllocatedSKUs);
                    adjustedAllocation.setSlots(a.getSlots());
                    adjustedAllocation.setFulfilledSlots(a.getFulfilledSlots() - slotsToBeReleased);
                    adjustedNewAllocations.add(adjustedAllocation);
                    // 1.10 adjust allocated SKU count
                    for (String s : adjustedAllocatedSKUs) {
                        if (adjustedAllocatedSKUCount.containsKey(s)) {
                            adjustedAllocatedSKUCount.put(s, adjustedAllocatedSKUCount.get(s) + 1);
                        } else {
                            adjustedAllocatedSKUCount.put(s, 1);
                        }
                    }
                } else {
                    allocatedRIDsSlots.put(rid, a.getFulfilledSlots());
                    boolean keepAllocation = false;
                    if (reallocatePartialFulfillment) {
                        // keep only fully fulfilled rids; partially allocated
                        // rids that do not have the same
                        // style-size with multiple colors will be re-allocated
                        if (a.getSlots() == a.getFulfilledSlots()) {
                            keepAllocation = true;
                        }
                    } else {
                        keepAllocation = true;
                    }
                    if (keepAllocation) {
                        // 1.9 adjust new allocations in matching result
                        RIDAllocation adjustedAllocation = new RIDAllocation();
                        adjustedAllocation.setUuid(a.getUuid());
                        adjustedAllocation.setRid(rid);
                        adjustedAllocation.setSlots(a.getSlots());
                        adjustedAllocation.setFulfilledSlots(a.getFulfilledSlots());
                        adjustedAllocation.setAllocatedSKUs(a.getAllocatedSKUs());
                        adjustedNewAllocations.add(adjustedAllocation);
                        // 1.10 adjust allocated SKU count
                        for (String s : a.getAllocatedSKUs()) {
                            if (adjustedAllocatedSKUCount.containsKey(s)) {
                                adjustedAllocatedSKUCount.put(s, adjustedAllocatedSKUCount.get(s) + 1);
                            } else {
                                adjustedAllocatedSKUCount.put(s, 1);
                            }
                        }
                    }
                }
            }
            matchingResultAdjusted.setNewAllocations(adjustedNewAllocations);

            // 1.11 adjust allocation requests in matching setup
            List<RIDAllocationRequest> adjustedRIDAllocationRequests = new ArrayList<RIDAllocationRequest>();
            for (RIDAllocationRequest ar : matchingSetup.getAllocationRequests()) {
                int rid = ar.getRid();
                if (!allocatedRIDsSlots.containsKey(rid)) { // keep un-allocated
                                                            // RID requests for
                                                            // re-allocation
                    adjustedRIDAllocationRequests.add(ar);
                } else {
                    if (ar.getSlots() > allocatedRIDsSlots.get(rid)) { // not
                                                                       // fully
                                                                       // fulfilled
                                                                       // in
                                                                       // current
                                                                       // allocation
                        if (ridStyleSizeMultiColors.containsKey(rid)) { // due
                                                                        // to
                                                                        // multiple
                                                                        // colors
                                                                        // of
                                                                        // the
                                                                        // same
                                                                        // style-size
                            int adjustedSlots = ar.getSlots() - allocatedRIDsSlots.get(rid);
                            RIDAllocationRequest adjustedRequest =
                                    new RIDAllocationRequest(rid, ar.getUuid(), adjustedSlots, ar.getHoursInQueue());
                            adjustedRIDAllocationRequests.add(adjustedRequest);
                        } else {
                            if (reallocatePartialFulfillment) {
                                adjustedRIDAllocationRequests.add(ar); // keep
                                                                       // originally
                                                                       // partially
                                                                       // allocated
                                                                       // RID
                                                                       // requests
                                                                       // for
                                                                       // re-allocation

                            }
                        }
                    }
                }
            }
            matchingSetupAdjusted.setAllocationRequests(adjustedRIDAllocationRequests);

            // 1.12 remove allocated SKUs from matching setup
            List<SKUSupply> adjustedSupplies = new ArrayList<SKUSupply>();
            for (SKUSupply ss : matchingSetup.getSupplies()) {
                if (!adjustedAllocatedSKUCount.containsKey(ss.getSku())) {
                    adjustedSupplies.add(ss);
                } else {
                    // adjust availability count if the sku was allocated
                    if (ss.getAtGwynnieBee() >= adjustedAllocatedSKUCount.get(ss.getSku())) {
                        ss.setAtGwynnieBee(ss.getAtGwynnieBee() - adjustedAllocatedSKUCount.get(ss.getSku()));
                    } else if (ss.getAtGwynnieBee() + ss.getAtLaudary() >= adjustedAllocatedSKUCount.get(ss.getSku())) {
                        ss.setAtLaudary(ss.getAtGwynnieBee() + ss.getAtLaudary() - adjustedAllocatedSKUCount.get(ss.getSku()));
                        ss.setAtGwynnieBee(0);
                    } else {
                        ss.setAtLaudary(0);
                        ss.setAtGwynnieBee(0);
                    }
                    // keep the sku spply only when it has availability after
                    // adjustment
                    if (ss.getAtGwynnieBee() + ss.getAtLaudary() > 0) {
                        adjustedSupplies.add(ss);
                    }
                }
            }
            matchingSetupAdjusted.setSupplies(adjustedSupplies);

            matchingInstanceAdjusted = new MatchingInstance();
            matchingInstanceAdjusted.setMatchingResult(matchingResultAdjusted);
            matchingInstanceAdjusted.setMatchingSetup(matchingSetupAdjusted);
        } catch (Exception e) {
            LOG.error("adjustMatchingSetupByGarmentSingleColor: " + e.getMessage());
            LOG.info("adjustMatchingSetupByGarmentSingleColor Failed");
            e.printStackTrace();
        }

        return matchingInstanceAdjusted;
    }

    /**
     * @param matchingResult1 matching result 1
     * @param matchingResult2 matching result 2
     * @return merge the new allocations from matching results 1 and 2
     */
    public MatchingResult mergeMatchingResult(MatchingResult matchingResult1, MatchingResult matchingResult2) {
        MatchingResult matchingResult = null;

        if (matchingResult1 == null && matchingResult2 != null) {
            return matchingResult2;
        }
        if (matchingResult1 != null && matchingResult2 == null) {
            return matchingResult1;
        }
        if (matchingResult1 != null && matchingResult2 != null) {
            matchingResult = new MatchingResult();
            matchingResult.setMatchingEngineMetaData(matchingResult1.getMatchingEngineMetaData());
            matchingResult.setReservedAllocations(matchingResult1.getReservedAllocations());
            matchingResult.setDateTimeKey(matchingResult1.getDateTimeKey());

            // merge new allocations
            List<RIDAllocation> allocations = new ArrayList<RIDAllocation>();
            Map<Integer, RIDAllocation> ridToAllocation = new HashMap<Integer, RIDAllocation>();
            for (RIDAllocation a1 : matchingResult1.getNewAllocations()) {
                allocations.add(a1);
                ridToAllocation.put(a1.getRid(), a1);
            }

            if (matchingResult1.getNewAllocations() != null) {
                for (RIDAllocation a2 : matchingResult2.getNewAllocations()) {
                    if (ridToAllocation.containsKey(a2.getRid())) {
                        // merge allocation results for the same rid from a1 and
                        // a2
                        RIDAllocation ridAllocation = ridToAllocation.get(a2.getRid());
                        int slots = ridAllocation.getSlots() > a2.getSlots() ? ridAllocation.getSlots() : a2.getSlots();
                        int fulfilledSlots = ridAllocation.getFulfilledSlots() + a2.getFulfilledSlots();
                        if (slots < fulfilledSlots) {
                            LOG.error("Error in merging matching result, slots" + "(" + slots + ") < fulfilled(" + fulfilledSlots
                                    + ") for RID " + a2.getRid());
                        }
                        ridAllocation.setSlots(slots);
                        ridAllocation.setFulfilledSlots(fulfilledSlots);
                        Set<String> allocatedSKUs = ridAllocation.getAllocatedSKUs();
                        if (a2.getAllocatedSKUs() != null) {
                            for (String s : a2.getAllocatedSKUs()) {
                                allocatedSKUs.add(s);
                            }
                        }
                    } else {
                        allocations.add(a2);
                    }
                }
            }
            matchingResult.setNewAllocations(allocations);
        }

        return matchingResult;
    }

    /**
     * @param result matching result
     * @throws Exception Exception
     */
    public void uploadResultJsonToS3(MatchingResult result) throws Exception {
        if (result != null) {
            try {
                String bucketName = AllocationOrderingEngineProperties.getInstance().getMatchingResultS3BucketName();
                String inputJson = new ObjectMapper().writeValueAsString(result);

                LOG.debug("Final matching result json: " + inputJson);
                AWSS3Client s3client = new AWSS3Client();

                String name = result.getDateTimeKey() + "-result.json";
                String objectName = AllocationOrderingEngineProperties.getInstance().getMatchingResultS3ObjectName();
                String simulationLocation =
                        SimulationTraceManagerProperties.getInstance().getSimulationS3ModuleName() + "/"
                                + SimulationTraceManagerProperties.getInstance().getNamespaceId();
                objectName = simulationLocation + "/" + objectName + "/";
                String latestObjectName = objectName;
                objectName += name;
                latestObjectName += LATEST_MATCHING_RESULT_FILE_KEYS;

                s3client.putObject(bucketName, objectName, inputJson.getBytes("UTF-8"), "application/json");
                s3client.putObject(bucketName, latestObjectName, name.getBytes("UTF-8"), "text/plain");
                LOG.info("Uploaded matching result json to S3: " + bucketName + "." + objectName);
            } catch (Exception e) {
                LOG.error("uploadResultJsonToS3: " + e.getMessage());
                LOG.info("Cannot upload matching result to S3");
            }
        } else {
            LOG.info("Matching result is NULL. No result to upload to S3");
        }
    }

}
