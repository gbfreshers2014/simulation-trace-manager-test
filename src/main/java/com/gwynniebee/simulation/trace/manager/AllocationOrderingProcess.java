package com.gwynniebee.simulation.trace.manager;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Collections;

import org.apache.commons.io.IOUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gwynniebee.simulation.trace.store.InputStreamProvider;
import com.gwynniebee.simulation.trace.store.FileInputStreamProvider;
import com.gwynniebee.simulation.trace.utils.RIDAllocationRequestsSortBySlots;
import com.gwynniebee.matching.result.MatchingResult;
import com.gwynniebee.matching.result.RIDAllocation;
import com.gwynniebee.matching.result.MatchingEngineMetaData;
import com.gwynniebee.matching.setup.MatchingSetup;
import com.gwynniebee.matching.setup.RIDAllocationRequest;
import com.gwynniebee.matching.setup.SKUSupply;
import com.gwynniebee.matching.setup.UserCloset;
import com.gwynniebee.simulation.trace.object.CustomerAllocationRequest;
import com.gwynniebee.simulation.trace.object.CustomerAllocation;
import com.gwynniebee.simulation.trace.object.Relax4ModelArc;
import com.gwynniebee.simulation.trace.object.Relax4ModelMatrix;
import com.gwynniebee.simulation.trace.utils.CustomerAllocationRequestsSortByHoursInQueue;

public class AllocationOrderingProcess {
    private static final Logger LOG = LoggerFactory.getLogger(AllocationOrderingProcess.class);

    public static final int RELAX4_MODEL_ARC_CAPACITY_UPPER_BOUND = 1;
    public static final int MAX_COST_SCALE = 100;
    public static final int MAX_ARC_CAPACITY_SCALE = 10;
    public static final int COST_WEIGHT_SCALE = 100;

    public static final int STEP_ENFORCE_FULL_ALLOCATION = 1;
    public static final int STEP_BASELINE = 2;
    public static final String KNOB_YES_KEY = "yes";
    public static final String KNOB_NO_KEY = "no";
    public static final String BALANCE_DEMAND_ALGORITHMS_TIME = "time";
    public static final String BALANCE_DEMAND_ALGORITHMS_RANDOM = "random";

    // support namespace
    private String nameSpacePrefix = "";

    // map from SKU to an integer index
    private Map<String, Integer> skuToIndex;
    // map from index to SKU
    private Map<Integer, String> indexToSKU;
    // map from customer to an integer index
    private Map<String, Integer> customerToIndex;
    // map from index to customer
    private Map<Integer, String> indexToCustomer;

    private Map<String, SKUSupply> skuToSkuSupply;
    private Map<String, CustomerAllocationRequest> uuidToCustomerAllocationRequest;
    private List<CustomerAllocationRequest> customerAllocationRequests;

    private int numSkus = 0;
    private int numCustomers = 0;
    private int numArcs = 0;
    private int maxHoursInQueue = 0;
    private int maxOrderId = 0;
    private List<Relax4ModelArc> arcs;
    private Relax4ModelMatrix costMatrix;
    private Relax4ModelMatrix capacityMatrix;
    private List<Integer> skuSupplies;
    private List<Integer> customerDemands;

    private int numCustomersCannotBeFulfilled = 0;
    private int numCustomersRemovedForBalance = 0;

    // carry reservedAllocations from matching setup
    private List<RIDAllocation> matchingSetupReservedAllocations;
    private String matchingSetupDateTimeKey;

    public AllocationOrderingProcess() {
        super();
    }

    public MatchingResult run(String nameSpacePrefix, MatchingSetup matchingSetup, double weightOrdering, double weightInQueue, int step,
            String balanceDemandAlgorithm) {

        LOG.info("Allocation Process step " + step + " started");
        this.nameSpacePrefix = nameSpacePrefix;

        MatchingResult matchingResult = null;
        try {
            if (prepareSolverInputJob(matchingSetup, step, weightOrdering, weightInQueue, balanceDemandAlgorithm)) {
                String solverStartDateTimeKey = getDateTimePrefix(new Date());
                if (runSolverJob()) {
                    String solverEndDateTimeKey = getDateTimePrefix(new Date());
                    matchingResult = produceAllocationPlanJob(solverStartDateTimeKey, solverEndDateTimeKey);
                } else {
                    LOG.info("runSolverJob FAILED");
                }
            } else {
                LOG.info("prepareSolverInputJob FAILED");
            }
        } catch (Exception e) {
            LOG.info("AllocationOrderingProcess run FAILED");
            LOG.error("AllocationOrderingProcess run: " + e.getMessage());
        }
        return matchingResult;
    }

    /**
     * @return boolean success or failure
     * @throws Exception exception
     */
    public boolean prepareSolverInputJob(MatchingSetup matchingSetup, int step, double weightOrdering, double weightInQueue,
            String balanceDemandAlgorithm) throws Exception {
        LOG.info("Start prepareSolverInputJob (step=" + step + ", weightOrdering=" + weightOrdering + ", weightInQuque=" + weightInQueue
                + ")");
        clearDataLocation();

        boolean result = false;

        if (matchingSetup != null) {
            matchingSetupReservedAllocations = matchingSetup.getReservedAllocations();
            matchingSetupDateTimeKey = matchingSetup.getDateTimeKey();
            loadMatchingSetupData(matchingSetup);
            if (produceRelax4Model(matchingSetup, step, weightOrdering, weightInQueue, balanceDemandAlgorithm)) {
                storeRelax4ModelFileForSolver();

                // check whether MatchingInput exists
                String location = AllocationOrderingEngineProperties.getInstance().getAllocationSolverDataLocation();
                String inputFileName = AllocationOrderingEngineProperties.getInstance().getAllocationSolverInput();
                String solverInputFileName = location + "/";
                if (!nameSpacePrefix.isEmpty()) {
                    solverInputFileName += nameSpacePrefix + "/";
                }
                solverInputFileName += inputFileName;
                File f = new File(solverInputFileName);
                LOG.info("Check existence of " + solverInputFileName);
                if (f.exists()) {
                    LOG.info(solverInputFileName + " exists");
                    result = true;
                } else {
                    LOG.info(solverInputFileName + " does not exist");
                }
            } else {
                LOG.info("Relax4 model was not produced successfully");
            }
        }

        LOG.info("End prepareSolverInputJob");
        return result;
    }

    /**
     * @return
     */
    public boolean runSolverJob() {
        LOG.info("Start runSolverJob################################################################");
        boolean result = true;
        try {
            String solverLocation = AllocationOrderingEngineProperties.getInstance().getAllocationSolverProgramLocation();
            String solver = AllocationOrderingEngineProperties.getInstance().getAllocationSolverProgramName();
            String solverDataLocation = AllocationOrderingEngineProperties.getInstance().getAllocationSolverDataLocation();
            String solverInput = AllocationOrderingEngineProperties.getInstance().getAllocationSolverInput();
            String solverOutput = AllocationOrderingEngineProperties.getInstance().getAllocationSolverOutput();
            StringBuilder solverCommand = new StringBuilder();
            solverCommand.append(solverLocation);
            solverCommand.append("/");
            solverCommand.append(solver);
            solverCommand.append(" ");
            solverCommand.append(solverDataLocation);
            solverCommand.append("/");
            if (!nameSpacePrefix.isEmpty()) {
                solverCommand.append(nameSpacePrefix + "/");
            }
            solverCommand.append(solverInput);
            solverCommand.append(" ");
            solverCommand.append(solverDataLocation);
            solverCommand.append("/");
            if (!nameSpacePrefix.isEmpty()) {
                solverCommand.append(nameSpacePrefix + "/");
            }
            solverCommand.append(solverOutput);
            LOG.info(solverCommand.toString());

            Process p = Runtime.getRuntime().exec(solverCommand.toString());
            p.waitFor();
        } catch (Exception e) {
            result = false;
            LOG.error("runSolverJob: " + e.getMessage());
        }

        LOG.info("End runSolverJob############################################################");
        return result;
    }

    /**
     * 
     */
    public MatchingResult produceAllocationPlanJob(String solverStartDateTimeKey, String solverEndDateTimeKey) throws IOException {
        LOG.info("Start produceAllocationPlanJob");
        MatchingResult matchingResult = null;

        // read in MatchingOuput from the allocation solver
        String location = AllocationOrderingEngineProperties.getInstance().getAllocationSolverDataLocation();
        String outputFileName = AllocationOrderingEngineProperties.getInstance().getAllocationSolverOutput();
        String solverOutputFileName = location + "/";
        if (!nameSpacePrefix.isEmpty()) {
            solverOutputFileName += nameSpacePrefix + "/";
        }
        solverOutputFileName += outputFileName;
        LOG.info("Read allocation solver output from " + solverOutputFileName);

        InputStream is = null;
        DataInputStream in = null;
        BufferedReader br = null;
        try {
            InputStreamProvider provider = new FileInputStreamProvider();
            is = provider.getInputStream(solverOutputFileName);
            in = new DataInputStream(is);
            br = new BufferedReader(new InputStreamReader(in));
            String strLine;

            LOG.info("Construct allocation flow matrix: " + numCustomers + " customers, " + numSkus + " skus");
            // Read File Line By Line
            int index = 0;
            Relax4ModelMatrix flowMatrix = new Relax4ModelMatrix(numSkus, numCustomers, 0);
            while ((strLine = br.readLine()) != null) {
                // populate flowMatrix
                int row = index / numCustomers;
                int col = index % numCustomers;
                strLine = strLine.replace("\r\n", "");

                flowMatrix.setValueAt(row, col, Integer.parseInt(strLine));
                // if(index % 10000 == 0) LOG.info("flowMatrix[" + row + "][" +
                // col + "] = " + flowMatrix.getValueAt(row, col) + ".");
                index++;
            }

            // Close the input stream
            br.close();
            in.close();
            is.close();

            // produce customer allocation plan
            LOG.info("Produce RID allocation plans");
            int skuDuplicateAllocated = 0;
            int overlyAllocated = 0;
            int totalRids = 0;
            int fullyAllocated = 0;
            int partiallyAllocated = 0;
            int totalSlots = 0;
            int fullyAllocatedSlots = 0;
            int partiallyAllocatedSlots = 0;
            Set<Integer> duplicateSKUIndex = new HashSet<Integer>();
            Set<Integer> duplicateCustomerIndex = new HashSet<Integer>();
            List<RIDAllocation> allocations = new ArrayList<RIDAllocation>();
            for (int j = 0; j < numCustomers; j++) {
                if (indexToCustomer.containsKey(j) && customerDemands.get(j) > 0) { // customer
                                                                                    // exists
                                                                                    // and
                                                                                    // was
                                                                                    // not
                                                                                    // removed
                    // scan through skus
                    Set<String> allocatedSkus = new HashSet<String>();
                    int allocatedSlots = 0;
                    for (int i = 0; i < numSkus; i++) {
                        if (indexToSKU.containsKey(i) && flowMatrix.getValueAt(i, j) > 0) { // has
                                                                                            // to
                                                                                            // be
                                                                                            // a
                                                                                            // valid
                                                                                            // sku
                            allocatedSlots += flowMatrix.getValueAt(i, j);
                            allocatedSkus.add(indexToSKU.get(i));
                        }
                        if (flowMatrix.getValueAt(i, j) > 1) {
                            skuDuplicateAllocated += 1;
                            duplicateSKUIndex.add(i);
                            duplicateCustomerIndex.add(j);
                        }
                    }
                    String uuid = indexToCustomer.get(j);
                    CustomerAllocationRequest customerReq = uuidToCustomerAllocationRequest.get(uuid);
                    List<RIDAllocationRequest> ridReqs = customerReq.getRIDAllocationRequests();
                    String[] allocatedSkusArray = allocatedSkus.toArray(new String[0]);
                    CustomerAllocation ca = new CustomerAllocation(ridReqs, uuid, allocatedSlots, allocatedSkusArray);
                    for (RIDAllocation allocation : ca.getRIDAllocations()) {
                        allocations.add(allocation);
                        totalRids += 1;
                        totalSlots += allocation.getSlots();
                        if (allocation.getFulfilledSlots() == allocation.getSlots()) {
                            fullyAllocated += 1;
                            fullyAllocatedSlots += allocation.getSlots();
                        } else if (allocation.getFulfilledSlots() > allocation.getSlots()) {
                            overlyAllocated += 1;
                            fullyAllocatedSlots += allocation.getSlots();
                        } else if (allocation.getFulfilledSlots() > 0) {
                            partiallyAllocated += 1;
                            partiallyAllocatedSlots += allocation.getSlots();
                        }
                    }
                }
            }

            double fullAllocationRate = (double) fullyAllocated * 100 / (double) totalRids;
            double fullAllocationSlotsRate = (double) fullyAllocatedSlots * 100 / (double) totalSlots;
            double partiallyAllocationRate = (double) partiallyAllocated * 100 / (double) totalRids;
            double partiallyAllocationSlotsRate = (double) partiallyAllocatedSlots * 100 / (double) totalSlots;
            LOG.info("Total number of RIDs = " + totalRids);
            LOG.info("Fully allocated RIDs = " + fullyAllocated + "(" + Double.toString(fullAllocationRate) + "%)");
            LOG.info("Partially allocated RIDs = " + partiallyAllocated + "(" + Double.toString(partiallyAllocationRate) + "%)");
            LOG.info("Overly allocated RIDs = " + overlyAllocated);
            LOG.info("Number of duplicate SKUs allocated = " + skuDuplicateAllocated);
            LOG.info("Duplicated Customers: " + duplicateCustomerIndex.toString());
            LOG.info("Duplicated SKUs: " + duplicateSKUIndex.toString());
            LOG.info("Total number of slots = " + totalSlots);
            LOG.info("Fully allocated slots = " + fullyAllocatedSlots + "(" + Double.toString(fullAllocationSlotsRate) + "%)");
            LOG.info("Partially allocated slots = " + partiallyAllocated + "(" + Double.toString(partiallyAllocationSlotsRate) + "%)");

            // produce matching result
            LOG.info("Produce matching result");
            int matchingEngineID = Integer.parseInt(AllocationOrderingEngineProperties.getInstance().getAllocationEngineID());
            String matchingEngineName = AllocationOrderingEngineProperties.getInstance().getAllocationEngineName();
            String solverName = AllocationOrderingEngineProperties.getInstance().getAllocationSolverName();
            String solverVersion = AllocationOrderingEngineProperties.getInstance().getAllocationSolverVersion();
            MatchingEngineMetaData matchingEngineMetaData =
                    new MatchingEngineMetaData(matchingEngineID, matchingEngineName, solverName, solverVersion, solverStartDateTimeKey,
                            solverEndDateTimeKey);

            matchingResult = new MatchingResult();
            matchingResult.setMatchingEngineMetaData(matchingEngineMetaData);
            matchingResult.setNewAllocations(allocations);
            matchingResult.setReservedAllocations(matchingSetupReservedAllocations);
            matchingResult.setDateTimeKey(matchingSetupDateTimeKey);

        } catch (Exception e) {
            matchingResult = null;
            LOG.info("produceAllocationPlanJob: " + e.getMessage());
            LOG.info("Cannot produce allocation plans");
        } finally {
            if (is != null) {
                IOUtils.closeQuietly(is);
            }
            if (in != null) {
                IOUtils.closeQuietly(in);
            }
            if (br != null) {
                IOUtils.closeQuietly(br);
            }
        }

        LOG.info("End produceAllocationPlanJob");
        return matchingResult;
    }

    /**
     * clear data location
     */
    protected void clearDataLocation() {
        String dataLocation = AllocationOrderingEngineProperties.getInstance().getAllocationSolverDataLocation();
        if (!nameSpacePrefix.isEmpty()) {
            dataLocation += "/" + nameSpacePrefix + "/";
        }
        File file = new File(dataLocation);
        if (file.exists()) {
            String[] dataFiles;
            dataFiles = file.list();
            if (dataFiles.length > 0) {
                for (int i = 0; i < dataFiles.length; i++) {
                    File dataFile = new File(file, dataFiles[i]);
                    dataFile.delete();
                }
            }
        }
        LOG.info("Cleared data location " + dataLocation);
    }

    /**
     *
     */
    protected void loadMatchingSetupData(MatchingSetup matchingSetup) {
        skuToIndex = new HashMap<String, Integer>();
        indexToSKU = new HashMap<Integer, String>();
        skuToSkuSupply = new HashMap<String, SKUSupply>();
        int skuIndex = 0;
        for (SKUSupply s : matchingSetup.getSupplies()) {
            if (skuToSkuSupply.containsKey(s.getSku())) {
                continue; // skip duplicate sku
            }

            skuToIndex.put(s.getSku(), skuIndex);
            indexToSKU.put(skuIndex, s.getSku());
            skuToSkuSupply.put(s.getSku(), s);
            skuIndex++;
        }
        LOG.info("Loaded SKU supply data into memory: " + skuIndex + " SKUs");
        numSkus = skuIndex;

        customerToIndex = new HashMap<String, Integer>();
        indexToCustomer = new HashMap<Integer, String>();
        uuidToCustomerAllocationRequest = new HashMap<String, CustomerAllocationRequest>();
        int customerIndex = 0;
        int numRIDs = 0;
        for (RIDAllocationRequest req : matchingSetup.getAllocationRequests()) {
            if (uuidToCustomerAllocationRequest.containsKey(req.getUuid())) {
                uuidToCustomerAllocationRequest.get(req.getUuid()).addRIDAllocationRequest(req);
            } else {
                List<RIDAllocationRequest> reqs = new ArrayList<RIDAllocationRequest>();
                reqs.add(req);
                CustomerAllocationRequest car = new CustomerAllocationRequest(req.getUuid(), req.getSlots(), req.getHoursInQueue(), reqs);
                uuidToCustomerAllocationRequest.put(req.getUuid(), car);
                customerToIndex.put(req.getUuid(), customerIndex);
                indexToCustomer.put(customerIndex, req.getUuid());
                customerIndex++;
            }
            if (maxHoursInQueue < req.getHoursInQueue()) {
                maxHoursInQueue = req.getHoursInQueue();
            }
            numRIDs++;
        }
        numCustomers = customerIndex;

        // scan through user closets to obtain maxOrderId
        for (UserCloset uc : matchingSetup.getUserClosets()) {
            if (customerToIndex.containsKey(uc.getUuid())) { // customer has
                                                             // demand
                Map<String, Integer> skuToOrder = uc.getOrders();
                if (skuToOrder != null) {
                    for (String sku : skuToOrder.keySet()) {
                        if (maxOrderId < skuToOrder.get(sku)) {
                            maxOrderId = skuToOrder.get(sku);
                        }
                    }
                }
            }
        }
        LOG.info("Loaded RID demand data into memory: " + numRIDs + " RIDs, " + customerIndex + " customers");
    }

    /**
     *
     */
    protected boolean produceRelax4Model(MatchingSetup matchingSetup, int step, double weightOrdering, double weightInQueue,
            String balanceDemandAlgorithm) {
        loadRelax4ModelArcs(matchingSetup, weightOrdering, weightInQueue);
        return computeRelax4Model(matchingSetup, step, balanceDemandAlgorithm);
    }

    /**
     *
     */
    protected void loadRelax4ModelArcs(MatchingSetup matchingSetup, double weightOrdering, double weightInQueue) {
        LOG.info("Start loading relax4 mode arcs");
        arcs = new ArrayList<Relax4ModelArc>();
        for (UserCloset uc : matchingSetup.getUserClosets()) {
            if (customerToIndex.containsKey(uc.getUuid())) { // customer has
                                                             // demand
                Map<String, Integer> skuToOrder = uc.getOrders();
                int y = customerToIndex.get(uc.getUuid());
                for (String sku : uc.getSkus()) {
                    if (skuToIndex.containsKey(sku)) { // sku is available
                        int x = skuToIndex.get(sku);
                        int orderId = 0;
                        if (skuToOrder != null && skuToOrder.containsKey(sku)) { // sku
                                                                                 // has
                                                                                 // an
                                                                                 // order
                                                                                 // id
                            orderId = skuToOrder.get(sku);
                        } else {
                            orderId = maxOrderId + 1; // assign max order id to
                                                      // sku that does not have
                                                      // a valid order id
                        }
                        // convert waiting time in days and assign cost
                        // reversely
                        double costInQueue =
                                (maxHoursInQueue - uuidToCustomerAllocationRequest.get(uc.getUuid()).getHoursInQueue() + 1) / 24;
                        // LOG.info("maxHoursInQueue = " + maxHoursInQueue +
                        // " customer in queue = " +
                        // uuidToCustomerAllocationRequest.get(uc.getUuid()).getHoursInQueue()
                        // + " costInQueue = " + costInQueue);
                        // we use logarithm to break ties in total cost of
                        // different allocation plans due to linear combination
                        // of the factors
                        // ties in total cost (a+b) may be broken by ln(a)+ln(b)
                        // while the relative relationship among individual cost
                        // values do not change
                        int cost =
                                (int) ((Math.log((weightOrdering * orderId + weightInQueue * costInQueue) * COST_WEIGHT_SCALE) + 1) * COST_WEIGHT_SCALE);
                        // LOG.info(sku + "->" + uc.getUuid() + ": cost = " +
                        // cost);
                        int capacity = RELAX4_MODEL_ARC_CAPACITY_UPPER_BOUND;
                        Relax4ModelArc arc = new Relax4ModelArc(x, y, cost, capacity);
                        arcs.add(arc);
                        numArcs++;
                    }
                }
            }
        }
        LOG.info("Loaded Relax4 model arcs into memory: " + numArcs + " arcs");
    }

    /**
     *
     */
    protected boolean computeRelax4Model(MatchingSetup matchingSetup, int step, String balanceDemandAlgorithm) {
        LOG.info("Start computeRelax4Model");

        // save a row for potential dummy supply and a column for potential
        // dummy demand
        costMatrix = new Relax4ModelMatrix(numSkus + 1, numCustomers + 1, 0);
        capacityMatrix = new Relax4ModelMatrix(numSkus + 1, numCustomers + 1, 0);
        skuSupplies = new ArrayList<Integer>();
        customerDemands = new ArrayList<Integer>();

        // record outFlow of sku nodes (supplies) and inFlow of customer nodes
        // (demands)
        // we have to make local balance at each supply node and demand node
        // whose capacities
        // can be covered by inFlow to customer node and by outFlow from sku
        // node
        Map<Integer, Integer> inFlow = new HashMap<Integer, Integer>();
        Map<Integer, Integer> outFlow = new HashMap<Integer, Integer>();

        // scan through all arcs
        // 1. populate cost and capacity matrix
        // 2. populate inFlow for each demand node and outFlow for each supply
        // node
        for (Relax4ModelArc arc : arcs) {
            costMatrix.setValueAt(arc.getX(), arc.getY(), arc.getCost());
            capacityMatrix.setValueAt(arc.getX(), arc.getY(), arc.getCapacity());

            int x = arc.getX();
            int y = arc.getY();
            if (outFlow.containsKey(x)) {
                outFlow.put(x, outFlow.get(x) + arc.getCapacity());
            } else {
                outFlow.put(x, arc.getCapacity());
            }
            if (inFlow.containsKey(y)) {
                inFlow.put(y, inFlow.get(y) + arc.getCapacity());
            } else {
                inFlow.put(y, arc.getCapacity());
            }
        }

        // scan through skus and customers to make sure each node's in and out
        // flows are
        // balanced; if not, we create a dummy demand node or a dummy supply
        // node
        int dummyCustomerFlow = 0;
        int dummySkuFlow = 0;
        int totalSupplies = 0;
        int totalDemands = 0;

        // 3. scan through skus to make sure each node's out_flow is balanced
        // (can be fulfilled)
        // if not, we create a dummy demand node
        for (int i = 0; i < numSkus; i++) {
            if (!outFlow.containsKey(i)) {
                outFlow.put(i, 0);
            }
            String sku = indexToSKU.get(i);
            int skuAvailability = skuToSkuSupply.get(sku).getAtGwynnieBee() + skuToSkuSupply.get(sku).getAtLaudary();
            if (step == STEP_BASELINE) {
                if (outFlow.get(i) < skuAvailability) {
                    // the supply sku node cannot be fulfilled so we will add a
                    // dummy demand node
                    int flowGap = skuAvailability - outFlow.get(i);
                    dummyCustomerFlow += flowGap;
                }
            } else if (step == STEP_ENFORCE_FULL_ALLOCATION) {
                // in step 1 of the two-pass algorithm
                if (outFlow.get(i) < skuAvailability) {
                    // count availability according to actual out flows if this
                    // sku's supply capacity cannot be consumed
                    // effectively remove the extra supply capacities that do
                    // not have demands
                    skuAvailability = outFlow.get(i);
                }
            }
            totalSupplies += skuAvailability;

            // populate skuSupplies
            skuSupplies.add(skuAvailability);
        }

        // 4. scan through customers to make sure each node's in_flow is
        // balanced (can be fulfilled)
        for (int j = 0; j < numCustomers; j++) {
            if (!inFlow.containsKey(j)) {
                inFlow.put(j, 0);
            }
            String customer = indexToCustomer.get(j);
            int customerSlots = uuidToCustomerAllocationRequest.get(customer).getSlots();
            if (step == STEP_BASELINE) {
                if (inFlow.get(j) < customerSlots) {
                    // the demand node cannot be fulfilled so we will add a
                    // dummy supply node
                    int flowGap = customerSlots - inFlow.get(j);
                    dummySkuFlow += flowGap;
                }
            } else if (step == STEP_ENFORCE_FULL_ALLOCATION) {
                // in step 1 of the two-pass algorithm
                if (inFlow.get(j) < customerSlots) {
                    // remove the demand if it cannot be fulfilled
                    // do not add dummy supply node
                    customerSlots = 0;
                    inFlow.put(j, 0);
                    numCustomersCannotBeFulfilled += 1;
                }
            }
            totalDemands += customerSlots;

            // populate customerDemands
            customerDemands.add(customerSlots);
        }
        LOG.info("Number of customers who cannot be fulfilled and removed = " + numCustomersCannotBeFulfilled);

        // 5. adjust capacities of dummy sku and dummy customer to make sure
        // total supplies and
        // total demands are balanced globally
        if (step == STEP_BASELINE) {
            // dummySkuFlow needs to be adjusted to handle overall demands
            dummySkuFlow += totalDemands;
            // dummyCustomerFlow needs to be adjusted to handle overall supplies
            dummyCustomerFlow += totalSupplies;
            int totalSuppliesPlusDummy = totalSupplies + dummySkuFlow;
            int totalDemandsPlusDummy = totalDemands + dummyCustomerFlow;
            if (totalSuppliesPlusDummy != totalDemandsPlusDummy) {
                int globalCapacityGap = totalSuppliesPlusDummy - totalDemandsPlusDummy;
                if (globalCapacityGap < 0) {
                    dummySkuFlow -= globalCapacityGap;
                } else {
                    dummyCustomerFlow += globalCapacityGap;
                }
            }
        } else if (step == STEP_ENFORCE_FULL_ALLOCATION) {
            // since we avoid dummy skus we need to remove demands necessary to
            // get total demands
            // no higher than total supplies
            if (totalSupplies == 0) {
                return false; // terminate since there is no supply
            }
            if (totalSupplies < totalDemands) {
                // sort demand nodes according to balanceDemandAlgorithm
                List<CustomerAllocationRequest> customerAllocationRequests = new ArrayList<CustomerAllocationRequest>();
                for (String key : uuidToCustomerAllocationRequest.keySet()) {
                    customerAllocationRequests.add(uuidToCustomerAllocationRequest.get(key));
                }
                if (BALANCE_DEMAND_ALGORITHMS_TIME.equals(balanceDemandAlgorithm)) {
                    // sort the customer allocation requests based on
                    // hoursInQueue, top with lowest hours in queue
                    Collections.sort(customerAllocationRequests, new CustomerAllocationRequestsSortByHoursInQueue());
                    int index = 0;
                    int length = customerAllocationRequests.size();
                    while ((totalSupplies < totalDemands) && (index < length)) {
                        if (customerAllocationRequests.get(index).getSlots() > 0) {
                            // remove demands from the customer
                            customerDemands.set(customerToIndex.get(customerAllocationRequests.get(index).getUuid()), 0);
                            totalDemands -= customerAllocationRequests.get(index).getSlots();
                            numCustomersRemovedForBalance += 1;
                        }
                        index++;
                    }
                    if (index >= length) {
                        return false; // terminate since all demands are removed
                    }
                }
            }
            if (totalSupplies >= totalDemands) {
                dummyCustomerFlow += totalSupplies - totalDemands;
            }
        }
        LOG.info("Number of customers removed for demand balance = " + numCustomersRemovedForBalance);

        LOG.info("Total supplies = " + totalSupplies);
        LOG.info("Total demands = " + totalDemands);
        LOG.info("dummySkuFlow = " + dummySkuFlow);
        LOG.info("dummyCustomerFlow = " + dummyCustomerFlow);

        // 6. adjust the final dimensions of skus and customers
        int finalDimSkus = numSkus;
        int finalDimCustomers = numCustomers;
        if (dummySkuFlow > 0) {
            finalDimSkus += 1;
        }
        if (dummyCustomerFlow > 0) {
            finalDimCustomers += 1;
        }

        // 7. adjust max arc capacity to make sure both of the local balance and
        // global balance can be satisfied through the arcs
        int maxArcCapacity = totalSupplies > totalDemands ? totalSupplies : totalDemands;

        // 8. add an arc from the dummy sku to all customers
        // make sure the dummy arcs have super high cost and super high capacity
        if (dummySkuFlow > 0) {
            for (int j = 0; j < finalDimCustomers; j++) {
                costMatrix.setValueAt(numSkus, j, (int) ((Math.log((maxOrderId + 1 + (maxHoursInQueue + 1) / 24) * COST_WEIGHT_SCALE) + 1)
                        * COST_WEIGHT_SCALE * MAX_COST_SCALE));
                capacityMatrix.setValueAt(numSkus, j, maxArcCapacity * MAX_ARC_CAPACITY_SCALE);
                numArcs += 1;
            }
            // add dummy sku's supply
            // make sure the dummy sku can satisfy all demands
            // note totalSupplies + dummySkuFlow == totalDemands +
            // dummyCustomerFlow
            // skuSupplies.add(dummySkuFlow + totalSupplies + dummySkuFlow);
            skuSupplies.add(dummySkuFlow);
        }

        // 9. add an arc from all skus to the dummy customer
        // make sure the dummy arcs have super high cost and super high capacity
        if (dummyCustomerFlow > 0) {
            for (int i = 0; i < finalDimSkus; i++) {
                costMatrix
                        .setValueAt(i, numCustomers,
                                (int) ((Math.log((maxOrderId + 1 + (maxHoursInQueue + 1) / 24) * COST_WEIGHT_SCALE) + 1)
                                        * COST_WEIGHT_SCALE * MAX_COST_SCALE));
                capacityMatrix.setValueAt(i, numCustomers, maxArcCapacity * MAX_ARC_CAPACITY_SCALE);
                numArcs += 1;
            }
            // add dummy customer's demand
            // make sure the dummy customer can satisfy all supplies
            // note totalDemands + dummyCustomerFlow == totalSupplies +
            // dummySkuFlow
            // customerDemands.add(dummyCustomerFlow + totalDemands +
            // dummyCustomerFlow);
            customerDemands.add(dummyCustomerFlow);
        }

        if (dummyCustomerFlow > 0 && dummySkuFlow > 0) {
            numArcs -= 1; // avoid double counting the arc between dummy sku and
                          // dummy customer
        }
        numSkus = finalDimSkus;
        numCustomers = finalDimCustomers;
        LOG.info("Computed Relax4 model: " + numSkus + " skus, " + numCustomers + " customers, " + numArcs + " arcs");

        return true;
    }

    /**
    *
    */
    public void storeRelax4ModelFileForSolver() throws IOException {
        String location = AllocationOrderingEngineProperties.getInstance().getAllocationSolverDataLocation();
        File f = new File(location);
        if (!f.exists()) { // create directory if it does not exist
            f.mkdir();
        }
        if (!nameSpacePrefix.isEmpty()) {
            String nsDir = location + "/" + "ns";
            File ns = new File(nsDir);
            if (!ns.exists()) { // create ns sub-directory if it does not exist
                ns.mkdir();
            }
            location += "/" + nameSpacePrefix;
            File l = new File(location);
            if (!l.exists()) { // create final ns sub-directory if it does not
                               // exist
                l.mkdir();
            }
        }
        LOG.info("storeRelax4ModelFileForSolver to location " + location);
        String solverInputFileName = location + "/" + AllocationOrderingEngineProperties.getInstance().getAllocationSolverInput();
        OutputStream out = null;
        PrintStream printStream = null;
        try {
            out = new FileOutputStream(new File(solverInputFileName), true);
            printStream = new PrintStream(out);

            // numSkus numCustomers numArcs
            LOG.info("Store Relax4 Model: " + numSkus + " skus, " + numCustomers + " customers, " + numArcs + " arcs");
            StringBuilder counts = new StringBuilder();
            counts.append(numSkus);
            counts.append(" ");
            counts.append(numCustomers);
            counts.append(" ");
            counts.append(numArcs);
            counts.append(System.getProperty("line.separator"));
            printStream.print(counts.toString());

            // cost matrix
            for (int i = 0; i < numSkus; i++) {
                StringBuilder costMatrixRow = new StringBuilder();
                for (int j = 0; j < numCustomers; j++) {
                    costMatrixRow.append(costMatrix.getValueAt(i, j));
                    if (j < numCustomers - 1) {
                        costMatrixRow.append(" ");
                    }
                }
                costMatrixRow.append(System.getProperty("line.separator"));
                printStream.print(costMatrixRow.toString());
            }

            // sku supplies
            StringBuilder supplies = new StringBuilder();
            for (Integer s : skuSupplies) {
                supplies.append(s);
                supplies.append(" ");
            }
            supplies.append(System.getProperty("line.separator"));
            printStream.print(supplies);

            // customer demands
            StringBuilder demands = new StringBuilder();
            for (Integer d : customerDemands) {
                demands.append(d);
                demands.append(" ");
            }
            demands.append(System.getProperty("line.separator"));
            printStream.print(demands);

            // capacity matrix
            StringBuilder capacityMatrixRow = new StringBuilder();
            for (int i = 0; i < numSkus; i++) {
                for (int j = 0; j < numCustomers; j++) {
                    if (capacityMatrix.getValueAt(i, j) != 0) {
                        capacityMatrixRow.append(capacityMatrix.getValueAt(i, j));
                        capacityMatrixRow.append(" ");
                    }
                }
            }
            capacityMatrixRow.append(System.getProperty("line.separator"));
            printStream.print(capacityMatrixRow.toString());

            out.flush();
            out.close();
            printStream.close();
            LOG.info("Relax4 model has been written to file " + solverInputFileName);
        } catch (Exception e) {
            LOG.error("storeRelax4ModelFileForSolver: " + e.getMessage());
            LOG.info("Cannot store Relax4 model");
        } finally {
            if (out != null) {
                IOUtils.closeQuietly(out);
            }
            if (printStream != null) {
                IOUtils.closeQuietly(printStream);
            }
        }
    }

    private String getDateTimePrefix(Date date) {
        SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd-HHmmss");
        return df.format(date);
    }

    public static final DateTimeFormatter FORMATTER = ISODateTimeFormat.dateTime().withZone(DateTimeZone.UTC);

}
