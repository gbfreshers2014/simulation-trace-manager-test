/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.simulation.trace.manager;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gwynniebee.matching.result.MatchingResult;
import com.gwynniebee.matching.result.RIDAllocation;
import com.gwynniebee.matching.setup.MatchingSetup;
import com.gwynniebee.matching.setup.RIDAllocationRequest;
import com.gwynniebee.matching.setup.SKUSupply;
import com.gwynniebee.matching.setup.UserCloset;
import com.gwynniebee.simulation.trace.service.AllocationEngineJob;
import com.gwynniebee.simulation.trace.object.Closet;
import com.gwynniebee.simulation.trace.object.ClosetTrace;
import com.gwynniebee.simulation.trace.object.CustomerReturnData;
import com.gwynniebee.simulation.trace.object.RID;
import com.gwynniebee.simulation.trace.object.RIDTrace;
import com.gwynniebee.simulation.trace.object.SKUAvailability;
import com.gwynniebee.simulation.trace.object.SKUAvailabilityTrace;
import com.gwynniebee.simulation.trace.object.SKUReturn;
import com.gwynniebee.simulation.trace.object.SKUReturnTrace;
import com.gwynniebee.simulation.trace.object.SKUReturnPlan;
import com.gwynniebee.simulation.trace.object.UserPlan;
import com.gwynniebee.simulation.trace.object.SimulationMetrics;
import com.gwynniebee.simulation.trace.store.InputStreamProvider;
import com.gwynniebee.simulation.trace.store.S3InputStreamProvider;
import com.gwynniebee.simulation.trace.store.SimulationTraceDataOperator;
import com.gwynniebee.simulation.trace.store.SimulationTraceDataOperatorImpl;

public class SimulationTraceManager {
    private static final Logger LOG = LoggerFactory.getLogger(SimulationTraceManager.class);

    public static final String LATEST_MATCHING_SETUP_FILE_KEYS = "latestMatchingSetupFileKeys";
    public static final String LATEST_MATCHING_RESULT_FILE_KEYS = "latestMatchingResultFileKeys";
    public static final String AT_GB = "atGwynnieBee";
    public static final String AT_LAUNDRY = "atLaundry";
    public static final int INCREMENT_ROUNDS_IN_RIDS = 24;

    private SimulationTraceManagerProperties props;
    private SimulationTraceDataOperator dataOperator;
    private MatchingSetup matchingSetup;
    private MatchingResult matchingResult;
    
    private int maxRid = 0;
    private ClosetTrace closetTrace = null;
    private Set<String> existingClosets = null;
    private SKUAvailabilityTrace skuAvailabilityTrace = null;
    private SKUReturnTrace skuReturnTrace = null;
    private RIDTrace ridTrace = null;
    private Map<String, UserPlan> userPlan = null;
    private Map<String, SKUReturnPlan> skuReturnPlan = null;
    private Map<String, CustomerReturnData> customerReturnData = null;

    /**
     */
    public SimulationTraceManager() {
        super();
        this.props = SimulationTraceManagerProperties.getInstance();
    }

    /**
     * @param dataOperator SimulationDataOperator
     */
    public SimulationTraceManager(SimulationTraceDataOperator dataOperator) {
        this.dataOperator = dataOperator;
    }

    /**
     * 
     * @return max rid
     */
    public int getMaxRid() {
        return this.maxRid;
    }
    
    /**
     * 
     * @param maxRid
     */
    public void setMaxRid(int maxRid) {
        this.maxRid = maxRid;
    }
    
    /**
     * NOTE: this should be called after this.props has been setup.
     *
     * @return SimulationDataOperator
     */
    protected SimulationTraceDataOperator getDataOperator() {
        if (this.dataOperator == null) {
            this.dataOperator = new SimulationTraceDataOperatorImpl(this.props);
        }
        return this.dataOperator;
    }

    /**
     * 
     */
    public static void main(String[] args) throws Exception {
        SimulationTraceManager simulationTraceManager = new SimulationTraceManager();
        simulationTraceManager.run();
    }

    /**
     * run simulation
     * @throws Exception
     */
    public void run() throws Exception {
        long startTime = System.currentTimeMillis();
        int startSequenceId = this.props.getSimulationStartSequenceId();
        int endSequenceId = this.props.getSimulationEndSequenceId();
        String allocationResultKey = null;
        Map<String, RIDAllocation> allocationPlan = null;
        
        LOG.info("Start simulation run");
        // load input data for simulation
        if (loadSimulationData(startSequenceId, endSequenceId)) {
            // run simulation sequences
            for (int i = startSequenceId; i <= endSequenceId; i ++ ) {   
                // update simulation data with last allocation result
                updateSimulationData(i, allocationPlan);
                // store updated simulation data
                SimulationMetrics simulationMetrics = storeSimulationData(i);
                // run allocation engine job
                allocationResultKey = runAllocationEngineJob();
                // load result from allocation engine job
                allocationPlan = loadAllocationResult(allocationResultKey);
                // store result from allocation engine job to trace manager
                storeAllocationResult(i, allocationPlan);
                // store simulation metrics from this sequence of simulation
                storeSimulationMetrics(i, allocationPlan, simulationMetrics);
            }
            // store simulation meta data
            this.dataOperator.storeSimulationMetaData();
        }
        long duration = System.currentTimeMillis() - startTime;
        LOG.info("Simulation completed in " + duration + " ms");
    }

    /**
     * @param startSequenceId the start sequence id
     * @param endSequenceId the end sequence id
     */
    public boolean loadSimulationData(int startSequenceId, int endSequenceId) {
        boolean success = true;
                
        LOG.info("Start loadSimulationData");
        dataOperator = this.getDataOperator();
        String s3Input = dataOperator.getS3SimulationInputPrefix();
        
        // 1. load input closet trace
        boolean initialization = true;
        boolean clone = false;
        this.closetTrace = dataOperator.fetchClosetTraceData(startSequenceId, endSequenceId, initialization, clone);
        if (this.closetTrace == null) {
            LOG.info("Missing closet input data. Terminate simulation.");
            return false;
        }
        
        // 2. load sku availability input
        String skuAvailabilityInputLocation = s3Input + "/" + this.props.getSimulationSKUAvailabilityInput();
        Map<String, SKUAvailability> skuAvailabilityInput = dataOperator.fetchSKUAvailabilityData(0, skuAvailabilityInputLocation);
        if (skuAvailabilityInput == null) {
            LOG.info("Missing sku availability input data. Terminate simulation.");
            return false;            
        }
        else {
            this.skuAvailabilityTrace = new SKUAvailabilityTrace();
            this.skuAvailabilityTrace.setSequence(startSequenceId, skuAvailabilityInput);
        }
        
        // 3. load rid input
        Map<String, RID> ridInput = dataOperator.fetchRIDInput();
        if (ridInput == null) {
            LOG.info("Do not use rid input data.");
        }
        else {
            this.ridTrace = new RIDTrace();
            this.ridTrace.setSequence(startSequenceId, ridInput);
            this.setMaxRid(dataOperator.getSimulationMaxRid());
        }
        
        // 4. load user plan
        this.userPlan = dataOperator.fetchUserPlanData();
        if (this.userPlan == null) {
            LOG.info("Missing user plan input data. Terminate simulation.");
            return false;                        
        }
        
        // 5. load sku return plan 
        this.skuReturnPlan = dataOperator.fetchSKUReturnPlanData();
        
        // 6. load customer return data
        this.customerReturnData = dataOperator.fetchCustomerReturnData();
                
        LOG.info("End loadSimulationData");
        return success;
    }
    
    /**
     * 
     * @param sequenceId the sequence id
     */
    public void updateSimulationData(int sequenceId, Map<String, RIDAllocation> allocationPlan) {
        LOG.info("Start updateSimulationData");
        int start = this.props.getSimulationStartSequenceId();
        int end = this.props.getSimulationEndSequenceId();
        if (sequenceId == start) {
            // first sequence in simulation
            if (this.customerReturnData != null) {
                // initialize SKUReturn trace from customer return plan
                for (String uuid : this.customerReturnData.keySet()) {
                    for (String sku : this.customerReturnData.get(uuid).getSkusToBeReturned().keySet()) {
                        int returnSequenceId = this.customerReturnData.get(uuid).getSkusToBeReturned().get(sku);
                        if (returnSequenceId <= end && returnSequenceId >= start) {
                            int atLaundry = returnSequenceId;
                            int atGB = atLaundry;
                            // if sku return plan doesn't include the sku, we assume it never returns and time spend in laundry is 0
                            if (this.skuReturnPlan.containsKey(sku)) {
                                atGB += this.skuReturnPlan.get(sku).getRoundsAtLaundry();
                            }
                            addSkuReturn(uuid, sku, AT_LAUNDRY, atLaundry);
                            addSkuReturn(uuid, sku, AT_GB, atGB);          
                        }
                        else {
                            LOG.info("Return sequence id " + returnSequenceId + " for uuid " + uuid + " sku " + sku + " is not in simulation sequence range, ignored.");
                        }
                    }
                }
                LOG.info("Initialized sku return trace in sequence " + sequenceId);
            }
            
            // collecting uuid for existing closets so that we can track new users from new closets
            if (this.props.getSimulationSkuRidSnapshotFlag()) {    // if false we need to generate rid from start 
                                                                   // then treat all closets as new from start
                for (String uuid : this.closetTrace.getSequence(sequenceId).keySet()) {
                    if (this.existingClosets == null) {
                        this.existingClosets = new HashSet<String>();
                    }
                    this.existingClosets.add(uuid);
                }
            }
        }
        else {
            // prepare sequence data for sku availability and rid
            this.skuAvailabilityTrace.setSequence(sequenceId, this.skuAvailabilityTrace.getSequence(sequenceId - 1));
            this.ridTrace.setSequence(sequenceId, this.ridTrace.getSequence(sequenceId - 1));
        }
        
        if (allocationPlan != null) {
            // scan allocation plan
            for (String uuid : allocationPlan.keySet()) {
                for (String sku : allocationPlan.get(uuid).getAllocatedSKUs()) {
                    // 1. adjust closets according to allocation plan
                    // remove allocated skus from all closets in trace
                    for (int i = start; i <= end; i ++) {
                        Map<String, Closet> closets = this.closetTrace.getSequence(sequenceId);
                        closets.get(uuid).getSkus().remove(sku);
                        if (closets.get(uuid).getOrders() != null) {
                            closets.get(uuid).getOrders().remove(sku);
                        }
                    }
                
                    // 2. adjust sku available count according to allocation plan
                    Map<String, SKUAvailability> skuAvailabilityData = this.skuAvailabilityTrace.getSequence(sequenceId);
                    int atCustomer = skuAvailabilityData.get(sku).getAtCustomer();
                    int atGwynnieBee = skuAvailabilityData.get(sku).getAtGwynnieBee();
                    skuAvailabilityData.get(sku).setAtCustomer(atCustomer + 1);
                    skuAvailabilityData.get(sku).setAtGwynnieBee(atGwynnieBee - 1);
                    
                    // 3. adjust sku return trace from allocation plan and sku return plan
                    if (this.skuReturnPlan != null && this.skuReturnPlan.containsKey(sku)) {
                        int cyclesReturn = this.skuReturnPlan.get(sku).getRoundsAtCustomer();
                        int cyclesLaundry = this.skuReturnPlan.get(sku).getRoundsAtLaundry();
                        addSkuReturn(uuid, sku, AT_LAUNDRY, sequenceId + cyclesReturn);
                        addSkuReturn(uuid, sku, AT_GB, sequenceId + cyclesReturn + cyclesLaundry);
                    }
                }
                
                // 4. remove rids that are in allocation plan that contains only fully allocated rids
                this.ridTrace.getSequence(sequenceId).remove(uuid);                
            }
        }

        // 5. update roundsInQueue for the existing rids
        Map<String, RID> ridData = null;
        if (this.ridTrace != null) {
            ridData = this.ridTrace.getSequence(sequenceId);
            if (sequenceId > start) { 
                for (String uuid : ridData.keySet()) {
                    ridData.get(uuid).setRoundsInQueue(ridData.get(uuid).getRoundsInQueue() + INCREMENT_ROUNDS_IN_RIDS);
                }
            }
        }
        
        if (this.skuReturnTrace != null && this.skuReturnTrace.getSequence(sequenceId) != null) {
            // scan sku return trace
            Map<String, SKUReturn> skuReturnData = this.skuReturnTrace.getSequence(sequenceId);
            Map<String, SKUAvailability> skuAvailabilityData = this.skuAvailabilityTrace.getSequence(sequenceId);
            for (String uuid : skuReturnData.keySet()) {
                for (String sku : skuReturnData.get(uuid).getSkuStates().keySet()) {
                    // 6. adjust sku available count according to sku return trace
                    int atCustomer = 1;
                    int atLaundry = 0;
                    int atGB = 0;
                    if (skuAvailabilityData.containsKey(sku)) {
                        atCustomer = skuAvailabilityData.get(sku).getAtCustomer();
                        atLaundry = skuAvailabilityData.get(sku).getAtLaundry();
                        atGB = skuAvailabilityData.get(sku).getAtGwynnieBee();
                    }
                    else {
                        SKUAvailability skuAvailability = new SKUAvailability();
                        skuAvailability.setSku(sku);
                        skuAvailability.setAtCustomer(atCustomer);
                        skuAvailability.setAtGwynnieBee(atGB);
                        skuAvailability.setAtLaundry(atLaundry);
                        skuAvailabilityData.put(sku,  skuAvailability);
                    }
                    skuAvailabilityData.get(sku).setAtCustomer(atCustomer - 1);
                    if (AT_LAUNDRY.equals(skuReturnData.get(uuid).getSkuStates().get(sku))) {
                        skuAvailabilityData.get(sku).setAtLaundry(atLaundry + 1);
                    }
                    if (AT_GB.equals(skuReturnData.get(uuid).getSkuStates().get(sku))) {
                        skuAvailabilityData.get(sku).setAtGwynnieBee(atGB + 1);
                    }
                        
                    // 7. adjust rids according to sku return trace
                    if (ridData != null) {
                        if (ridData.containsKey(uuid)) {
                            // update the rid of uuid with more slots
                            ridData.get(uuid).setSlots(ridData.get(uuid).getSlots() + 1);
                        }
                        else {
                            // create a new rid
                            RID rid = new RID();
                            rid.setUuid(uuid);
                            rid.setRid(this.getMaxRid() + 1);
                            this.setMaxRid(this.getMaxRid() + 1);
                            rid.setRoundsInQueue(0);
                            rid.setSlots(1);
                            ridData.put(uuid,  rid);
                        }
                    }
                }
            }
        }
        
        for (String uuid : this.closetTrace.getSequence(sequenceId).keySet()) {
            // scan closet and check for new users
            if (this.existingClosets == null) {
                this.existingClosets = new HashSet<String>();
            }

            if (!this.existingClosets.contains(uuid)) {
                // 8. this is a new closet then create a new rid
                if (this.userPlan.containsKey(uuid)) {
                    if (this.ridTrace == null) {
                        this.ridTrace = new RIDTrace();
                        this.setMaxRid(dataOperator.getSimulationMaxRid());
                    }
                    if (ridData == null) {
                        ridData = new HashMap<String, RID>();
                        this.ridTrace.setSequence(sequenceId, ridData);
                    }
                    
                    RID rid = new RID();
                    rid.setUuid(uuid);
                    rid.setRid(this.getMaxRid() + 1);
                    this.setMaxRid(this.getMaxRid() + 1);
                    rid.setRoundsInQueue(0);
                    int slots = this.userPlan.get(uuid).getSlots();
                    rid.setSlots(slots);
                    ridData.put(uuid,  rid);
                }
                else {
                    LOG.info("New closet for uuid " + uuid + " does not have data in user plan therefore a new rid cannot be created for it");
                }
                
                // update existing closets set
                this.existingClosets.add(uuid);
            }            
        }

        LOG.info("End updateSimulationData");
    }
 
    /**
     * 
     * @param uuid
     * @param sku
     * @param returnSequenceId
     */
    private void addSkuReturn(String uuid, String sku, String state, int returnSequenceId) {
        Map<String, SKUReturn> skuReturnData;
        
        if (this.skuReturnTrace == null) {
            this.skuReturnTrace = new SKUReturnTrace();
        }
        if (this.skuReturnTrace.getSequence(returnSequenceId) == null) {
            skuReturnData = new HashMap<String, SKUReturn>();
            this.skuReturnTrace.setSequence(returnSequenceId, skuReturnData);                        
        }
        else {
            skuReturnData = this.skuReturnTrace.getSequence(returnSequenceId);
        }
        if (skuReturnData.containsKey(uuid)) {
            skuReturnData.get(uuid).addSkuState(sku, state);
        }
        else {
            SKUReturn skuReturn = new SKUReturn();
            skuReturn.setUuid(uuid);
            skuReturn.addSkuState(sku, state);
            skuReturnData.put(uuid,  skuReturn);
        }
    }
    
    /**
     * @param sequenceId the sequence id
     */
    public SimulationMetrics storeSimulationData(int sequenceId) {
        LOG.info("Start storeSimulationData");
        // 1. store closet trace
        String closetS3Location = this.dataOperator.getS3SimulationTraceManagerPrefix() + "/ClosetTrace/" + String.valueOf(sequenceId);
        this.dataOperator.storeClosetData(this.closetTrace.getSequence(sequenceId), closetS3Location);
        
        // 2. store sku availability trace and sku return trace
        String skuAvailabilityS3Location = this.dataOperator.getS3SimulationTraceManagerPrefix() + "/SKUAvailabilityTrace/" + String.valueOf(sequenceId);
        this.dataOperator.storeSKUAvailabilityData(this.skuAvailabilityTrace.getSequence(sequenceId), skuAvailabilityS3Location);
        if (this.skuReturnTrace != null) {
            String skuReturnS3Location = this.dataOperator.getS3SimulationTraceManagerPrefix() + "/SKUAvailabilityTrace/" + String.valueOf(sequenceId) + ".returns";
            this.dataOperator.storeSKUReturnData(this.skuReturnTrace.getSequence(sequenceId), skuReturnS3Location);
        }
        
        // 3. produce matching setup for allocation
        SimulationMetrics simulationMetrics = produceMatchingSetup(sequenceId);
        
        // 4. store rid trace
        String ridS3Location = this.dataOperator.getS3SimulationTraceManagerPrefix() + "/RIDTrace/" + String.valueOf(sequenceId);
        this.dataOperator.storeRIDData(this.ridTrace.getSequence(sequenceId), ridS3Location, INCREMENT_ROUNDS_IN_RIDS);
        
        LOG.info("End storeSimulationData");
        return simulationMetrics;
    }
    
    /**
     * produce matching setup for the allocation engine
     * @param sequenceId
     */
    public SimulationMetrics produceMatchingSetup(int sequenceId) {
        SimulationMetrics simulationMetrics = new SimulationMetrics();        
        LOG.info("Start produceMatchingSetup");
try {
        Map<String, Closet> closets = this.closetTrace.getSequence(sequenceId);        
        Map<String, SKUAvailability> skuAvailability = this.skuAvailabilityTrace.getSequence(sequenceId);
        Map<String, RID> rids = this.ridTrace.getSequence(sequenceId);

        // 1. scan closets and construct UserCloset
        List<UserCloset> closetList = new ArrayList<UserCloset>();
        Map<String, Integer> skuDemand = new HashMap<String, Integer>();
        Set<String> closetsNoEnoughItems = new HashSet<String>();
        if (closets != null && skuAvailability != null) {
            for (String uuid : closets.keySet()) {
                UserCloset userCloset = null;
                if (closets.get(uuid) != null && closets.get(uuid).getSkus() != null) {
                    int closetedItems = closets.get(uuid).getSkus().size();
                    if (this.userPlan.get(uuid) != null) {
                        int planSlots = this.userPlan.get(uuid).getSlots();
                        int requiredMinimum = Math.max(6, 2 * planSlots + 2);
                        if (closetedItems < requiredMinimum) {
                            closetsNoEnoughItems.add(uuid);
                        }
                        
                        userCloset = new UserCloset();
                        userCloset.setUuid(uuid);
                        Set<String> skus = new HashSet<String>(closets.get(uuid).getSkus());
                        userCloset.setSkus(skus);
                        Map<String, Integer> orders = null;
                        if (closets.get(uuid).getOrders() != null) {
                            orders = new HashMap<String, Integer>(closets.get(uuid).getOrders());
                        }
                        userCloset.setOrders(orders);
                        
                        // compute demand of each sku; remove sku that does not have supply
                        Iterator<String> iter = userCloset.getSkus().iterator();
                        while (iter.hasNext()) {
                            String sku = iter.next();
                            if (skuAvailability.containsKey(sku) && skuAvailability.get(sku).getAtGwynnieBee() > 0) {   // sku has supply
                                if (skuDemand.containsKey(sku)) {   // compute demand to the sku
                                    skuDemand.put(sku,  skuDemand.get(sku) + 1);
                                }
                                else {
                                    skuDemand.put(sku,  1);
                                }
                            }
                            else {  // remove sku from closet that does not have supply
                                iter.remove();
                                userCloset.removeOrder(sku);
                            }
                        }
                    }
                }
                else {
                    closetsNoEnoughItems.add(uuid);
                }
                if (userCloset != null) {
                    closetList.add(userCloset);
                }
            }
            simulationMetrics.addTotalClosets(closets.size());
            simulationMetrics.addBelowMinClosets(closetsNoEnoughItems.size());
            LOG.info("Collected closet and sku demand from closet sequence " + sequenceId);
        }
        else {
            LOG.info("closets is null");
        }
        
        // 2. construct SKUSupply : remove skus that either do not have supply or do not have demand
        List<SKUSupply> supplyList = new ArrayList<SKUSupply>();
        if (skuAvailability != null) {
            for (String sku : skuAvailability.keySet()) {
                if (skuAvailability.get(sku).getAtGwynnieBee() > 0 && skuDemand.containsKey(sku) && skuDemand.get(sku) > 0) {
                    SKUSupply supply = new SKUSupply();
                    supply.setSku(sku);
                    supply.setDemand(skuDemand.get(sku));
                    supply.setAtGwynnieBee(skuAvailability.get(sku).getAtGwynnieBee());
                    supply.setAtLaudary(skuAvailability.get(sku).getAtLaundry());
                    supplyList.add(supply);
                }
                if (skuAvailability.get(sku).getAtGwynnieBee() > 0) {
                    simulationMetrics.addTotalAvailableSKUs(1);
                    simulationMetrics.addTotalAvailableSnowflakes(skuAvailability.get(sku).getAtGwynnieBee());
                }
            }
            LOG.info("Collected supply from sku availability sequence " + sequenceId); 
        }
        else {
            LOG.info("sku availability is null");
        }
        
        // 3. construct RIDAllocationRequest : remove rids whose closets do not have enough items
        List<RIDAllocationRequest> allocationRequestList = new ArrayList<RIDAllocationRequest>();
        if (rids != null) {
            for (String uuid : closetsNoEnoughItems) {
                if (rids.containsKey(uuid)) {
                    rids.remove(uuid);  // remove rids whose closets do not have minimum items
                }
            }
            LOG.info("Removed " + closetsNoEnoughItems.size() + " rids for closets that do not satisfy minimum items");
            
            for (String uuid : rids.keySet()) {
                RIDAllocationRequest ridAllocationRequest = new RIDAllocationRequest();
                ridAllocationRequest.setUuid(uuid);
                ridAllocationRequest.setRid(rids.get(uuid).getRid());
                ridAllocationRequest.setSlots(rids.get(uuid).getSlots());
                int roundsInQueue = rids.get(uuid).getRoundsInQueue() < 0 ? 0 : rids.get(uuid).getRoundsInQueue() / INCREMENT_ROUNDS_IN_RIDS;
                ridAllocationRequest.setHoursInQueue(roundsInQueue);
                allocationRequestList.add(ridAllocationRequest);
                simulationMetrics.addTotalRids(1);
                simulationMetrics.addCustomerInQueueCounts(roundsInQueue, 1);
            }
            LOG.info("Collected rid allocation request from rid sequence " + sequenceId);
        }
        else {
            LOG.info("rids is null");
        }
        
        String dateTimePrefix = getDateTimePrefix(new Date());
        String s3Location = this.dataOperator.getS3SimulationMatchingSetupPrefix();
        if (closetList.size() > 0 && supplyList.size() > 0 && allocationRequestList.size() > 0) {
            MatchingSetup matchingSetup = new MatchingSetup(closetList, supplyList, allocationRequestList, null);
            matchingSetup.setDateTimeKey(dateTimePrefix);
            this.dataOperator.storeMatchingSetup(matchingSetup, s3Location, dateTimePrefix);
            LOG.info("Stored matching setup to " + s3Location);
        }
        else {
            LOG.info("closets size " + closetList.size() + ", supplies size " + supplyList.size() + 
                     ", allocationRequest size " + allocationRequestList.size());
            LOG.info("No matching setup can be created due to lack of input data");
            this.dataOperator.storeMatchingSetup(null, s3Location, dateTimePrefix);
        }
} catch (Exception e) {
    e.printStackTrace();
}

        LOG.info("End produceMatchingSetup");
        return simulationMetrics;
    }
    
    private String getDateTimePrefix(Date date) {
        SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd-HHmmss");
        return df.format(date);
    }

    /**
     * 
     * @return allocation result key the allocation result key
     */
    public String runAllocationEngineJob() {
        LOG.info("Start runAllocationEngineJob");
        AllocationOrderingEngine allocationEngineJob = new AllocationOrderingEngine();
        String previousResultKey = null;
        String resultKey = null;
        try {
            InputStreamProvider provider = new S3InputStreamProvider(this.props.getSimulationS3BucketName());
            InputStream is = null;
            // fetch file name in LATEST_MATCHING_RESULT_FILE_KEYS
            String location = this.dataOperator.getS3SimulationMatchingResultPrefix() + "/" + LATEST_MATCHING_RESULT_FILE_KEYS;
            try {
                is = provider.getInputStream(location);
                previousResultKey = IOUtils.toString(new InputStreamReader(is, "UTF-8"));
            } catch (Exception e) {
                LOG.error("Get previous result key in runAllocationEngineJob : " + e.getMessage());
            } finally {
                if (is != null) {
                    IOUtils.closeQuietly(is);
                    is = null;
                }
            }

            allocationEngineJob.allocationOrderingEngineJob("");

            try {
                is = provider.getInputStream(location);
                resultKey = IOUtils.toString(new InputStreamReader(is, "UTF-8"));
            } catch (Exception e) {
                LOG.error("Get result key in runAllocationEngineJob : " + e.getMessage());
            } finally {
                if (is != null) {
                    IOUtils.closeQuietly(is);
                    is = null;
                }
            }
            
            if (previousResultKey != null && resultKey != null && resultKey.equals(previousResultKey)) {
                resultKey = null;
            }
        } catch (Exception e) {
            LOG.info("Allocation engine error : " + e.getMessage());
        }
        LOG.info("End runAllocationEngineJob");
        return resultKey;
    }
    
    /**
     * 
     * @param allocationResultKey the allocation result key
     */
    public Map<String, RIDAllocation> loadAllocationResult(String allocationResultKey) {
        LOG.info("Start loadAllocationResult");
        Map<String, RIDAllocation> allocationPlan = null;
        if (allocationResultKey != null) {
            String allocationResultLocation = this.dataOperator.getS3SimulationMatchingResultPrefix() + "/" + allocationResultKey;
            allocationPlan = this.dataOperator.fetchRIDAllocationData(allocationResultLocation);
        }
        else {
            LOG.info("Allocation result key is null, no allocation plan to fetch.");
        }
        LOG.info("End loadAllocationResult");
        return allocationPlan;
    }
    
    /**
     * 
     * @param sequenceId
     * @param allocationPlan
     */
    public void storeAllocationResult(int sequenceId, Map<String, RIDAllocation> allocationPlan) {
        LOG.info("Start storeAllocationResult");
        String s3Location = this.dataOperator.getS3SimulationTraceManagerPrefix() + "/AllocationTrace/" + String.valueOf(sequenceId);
        this.dataOperator.storeAllocationData(allocationPlan, s3Location);
        LOG.info("End storeAllocationResult");
    }
    
    /**
     * 
     * @param sequenceId
     * @param allocationPlan
     */
    public void storeSimulationMetrics(int sequenceId, Map<String, RIDAllocation> allocationPlan, 
                                       SimulationMetrics simulationMetrics) {
        LOG.info("Start storeSimulationMetrics");
        
        if (simulationMetrics != null) {
            SimulationMetrics sm = new SimulationMetrics();
            sm.setBelowMinClosets(simulationMetrics.getBelowMinClosets());
            sm.setCustomerInQueueCounts(simulationMetrics.getCustomerInQueueCounts());
            sm.setTotalAvailableSKUs(simulationMetrics.getTotalAvailableSKUs());
            sm.setTotalAvailableSnowflakes(simulationMetrics.getTotalAvailableSnowflakes());
            sm.setTotalClosets(simulationMetrics.getTotalClosets());
            sm.setTotalRids(simulationMetrics.getTotalRids());
            
            Set<String> allocatedSKUs = new HashSet<String>();
            int allocatedSnowflakes = 0;
            int allocatedRids = 0;
            if (allocationPlan != null) {
                for (String uuid : allocationPlan.keySet()) {
                    if (allocationPlan.get(uuid).getFulfilledSlots() == allocationPlan.get(uuid).getSlots()) {
                        allocatedRids ++;
                        allocatedSnowflakes += allocationPlan.get(uuid).getFulfilledSlots();
                        allocatedSKUs.addAll(allocationPlan.get(uuid).getAllocatedSKUs());
                    }
                }
            }
            sm.setUnallocatedRids(simulationMetrics.getTotalRids() - allocatedRids);
            sm.setUnallocatedSKUs(simulationMetrics.getTotalAvailableSKUs() - allocatedSKUs.size());
            sm.setUnallocatedSnowflakes(simulationMetrics.getTotalAvailableSnowflakes() - allocatedSnowflakes);
            
            String s3Location = this.dataOperator.getS3SimulationTraceManagerPrefix() + "/AllocationTrace/" + String.valueOf(sequenceId) + ".stats";
            this.dataOperator.storeSimulationMetrics(sm, s3Location);
        }
        LOG.info("End storeSimulationMetrics");
    }
}
