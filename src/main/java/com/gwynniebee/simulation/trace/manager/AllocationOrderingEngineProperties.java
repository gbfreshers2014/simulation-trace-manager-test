/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.simulation.trace.manager;

import java.util.Map;
import java.util.Properties;

import com.gwynniebee.iohelpers.IOGBUtils;


/**
 * @author dj
 *
 */
public class AllocationOrderingEngineProperties extends Properties {

    private static final long serialVersionUID = 1927814328839874539L;

    /**
     * @author dj
     *
     */
    private static class PropertiesHolder {
        private static final AllocationOrderingEngineProperties INSTANCE
            = IOGBUtils.getPropertiesFromResource("/allocation.ordering.engine.properties", AllocationOrderingEngineProperties.class);
    }

    /**
     * @return properties
     */
    public static AllocationOrderingEngineProperties getInstance() {
        return PropertiesHolder.INSTANCE;
    }

    /**
     * default constructor.
     */
    public AllocationOrderingEngineProperties() {
        super();
    }

    /** Constructor that allows overrides.
     *
     * @param overrideProps properties that can override default property from configuration file
     */
    public AllocationOrderingEngineProperties(Map<String, Object> overrideProps) {
        super(AllocationOrderingEngineProperties.getInstance());
        if (overrideProps != null) {
            for (Map.Entry<String, Object> entry : overrideProps.entrySet()) {
                if (entry.getValue() == null) {
                    this.put(entry.getKey(), null);
                } else {
                    this.put(entry.getKey(), entry.getValue().toString());
                }
            }
        }
    }

    /**
     * @return matching setup S3 bucket name
     */
    public String getMatchingSetupS3BucketName() {
        return this.getProperty("matching.setup.s3.bucket.name");
    }
    
    /**
     * @return matching setup S3 object name
     */
    public String getMatchingSetupS3ObjectName() {
        return this.getProperty("matching.setup.s3.object.name");
    }
    
    /**
     * @return matching result S3 bucket name
     */
    public String getMatchingResultS3BucketName() {
        return this.getProperty("matching.result.s3.bucket.name");
    }

    /**
     * @return matching result S3 object name
     */
    public String getMatchingResultS3ObjectName() {
        return this.getProperty("matching.result.s3.object.name");
    }

    /**
     * @return matching result S3 directory name
     */
    public String getMatchingResultS3DirectoryName() {
        return this.getProperty("matching.result.s3.directory.name");
    }

    /**
     * @return allocation engine name
     */
    public String getAllocationEngineName() {
        return this.getProperty("allocation.engine.name");
    }

    /**
     * @return allocation engine id
     */
    public String getAllocationEngineID() {
        return this.getProperty("allocation.engine.id");
    }

    /**
     * @return allocation solver name
     */
    public String getAllocationSolverName() {
        return this.getProperty("allocation.solver.name");
    }
   
    /**
     * @return allocation solver version
     */
    public String getAllocationSolverVersion() {
        return this.getProperty("allocation.solver.version");
    }
    
    /**
     * @return allocation solver input
     */
    public String getAllocationSolverInput() {
        return this.getProperty("allocation.solver.input");
    }

    /**
     * @return allocation solver output
     */
    public String getAllocationSolverOutput() {
        return this.getProperty("allocation.solver.output");
    }

    /**
     * @return allocation solver data location
     */
    public String getAllocationSolverDataLocation() {
        return this.getProperty("allocation.solver.data.location");
    }
    
    /**
     * @return allocation solver program name
     */
    public String getAllocationSolverProgramName() {
        return this.getProperty("allocation.solver.program.name");
    }

    /**
     * @return allocation solver program location
     */
    public String getAllocationSolverProgramLocation() {
        return this.getProperty("allocation.solver.program.location");
    }
    
    /**
     * @return cost weight for ordering
     */
    public String getCostFunctionWeightOrdering() {
        return this.getProperty("cost.function.weight.ordering");
    }
    
    /**
     * @return cost weight for waiting time
     */
    public String getCostFunctionWeightTimeInQueue() {
        return this.getProperty("cost.function.weight.time.in.queue");
    }
    
    /**
     * @return two-pass allocation enabled
     */
    public String getTwoPassesAllocationEnabled() {
        return this.getProperty("two.passes.allocation.enabled");
    }

    /**
     * @return algorithm for balancing demands
     */
    public String getReallocationTriesForGarmentSingleColor() {
        return this.getProperty("garment.single.color.reallocation.tries");
    }

}
