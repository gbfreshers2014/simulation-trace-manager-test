/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.simulation.trace.manager;

import com.gwynniebee.async.job.helper.AsyncJobRestletServlet;

import javax.servlet.ServletException;

/**
 * @author dj
 */
public class RestfulServlet extends AsyncJobRestletServlet {
    private static final long serialVersionUID = 1927814328839874539L;
    
    /**
     * Number of threads for async job.
     */
    private static final int NUM_THREADS = 1;

    /**
     * Constructor.
     */
    public RestfulServlet() {
        super(NUM_THREADS, "", AllocationOrderingEngine.class);
    }

    /**
     * Initialize servlet.
     * @throws javax.servlet.ServletException ServletException.
     */
    @Override
    public void init() throws ServletException {
        super.init();
    }

    /**
     * Destroy servlet.
     */
    @Override
    public void destroy() {
        super.destroy();
    }
}
