/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.simulation.trace.manager;

import java.util.Map;
import java.util.Properties;

import com.gwynniebee.iohelpers.IOGBUtils;

/**
 * @author dj
 *
 */
public class SimulationTraceManagerProperties extends Properties {

    private static final long serialVersionUID = 1927814328839874539L;

    private static class PropertiesHolder {
        private static final SimulationTraceManagerProperties INSTANCE
            = IOGBUtils.getPropertiesFromResource("/simulation.trace.manager.properties", SimulationTraceManagerProperties.class);
    }

    /**
     * @return properties
     */
    public static SimulationTraceManagerProperties getInstance() {
        return PropertiesHolder.INSTANCE;
    }

    /**
     * default constructor.
     */
    public SimulationTraceManagerProperties() {
        super();
    }

    /** Constructor that allows overrides.
     *
     * @param jobData properties that can override default property from configuration file
     */
    public SimulationTraceManagerProperties(Map<String, Object> jobData) {
        super(SimulationTraceManagerProperties.getInstance());
        if (jobData.get("namespaceId") != null) {
            this.setProperty("namespace.id", jobData.get("namespaceId").toString());
        } else {
            this.setProperty("namespace.id", this.getProperty("simulation.s3.namespace.id"));
        }
        this.setProperty("simulation.s3.namespace.id", this.getProperty("namespace.id"));
    }

    /**
     * @return namespaceId
     */
    public int getNamespaceId() {
        String s = this.getProperty("namespace.id");
        if (s == null) {
            return 0;
        }
        return Integer.parseInt(s);
    }

    /**
     * @return simulation S3 bucket name
     */
    public String getSimulationS3BucketName() {
        return this.getProperty("simulation.s3.bucket.name");
    }

    /**
     * @return simulation S3 module name
     */
    public String getSimulationS3ModuleName() {
        return this.getProperty("simulation.s3.module.name");
    }

    /**
     * @return simulation S3 namespace id
     */
    public String getSimulationS3NamespaceId() {
        return this.getProperty("simulation.s3.namespace.id");
    }

    /**
     * @return simulation S3 sub-module trace manager name
     */
    public String getSimulationS3SubModuleTraceManagerName() {
        return this.getProperty("simulation.s3.submodule.trace.manager.name");
    }

    /**
     * @return simulation S3 sub-module matching setup name
     */
    public String getSimulationS3SubModuleMatchingSetupName() {
        return this.getProperty("simulation.s3.submodule.matching.setup.name");
    }
    
    /**
     * @return simulation S3 sub-module matching result name
     */
    public String getSimulationS3SubModuleMatchingResultName() {
        return this.getProperty("simulation.s3.submodule.matching.result.name");
    }
            
    /**
     * @return simulation start sequence id
     */
    public int getSimulationStartSequenceId() {
        String s = this.getProperty("simulation.start.sequence.id");
        if (s == null) {
            return 0;
        }
        return Integer.parseInt(s);
    }
    
    /**
     * @return simulation end sequence id
     */
    public int getSimulationEndSequenceId() {
        String s = this.getProperty("simulation.end.sequence.id");
        if (s == null) {
            return 0;
        }
        return Integer.parseInt(s);
    }

    /**
     * @return simulation closet trace location
     */
    public String getSimulationClosetTraceLocation() {
        return this.getProperty("simulation.closet.trace");
    }

    /**
     * @return simulation sku availability trace location
     */
    public String getSimulationSKUAvailabilityTraceLocation() {
        return this.getProperty("simulation.sku.availability.trace");
    }

    /**
     * @return simulation rid trace location
     */
    public String getSimulationRIDTraceLocation() {
        return this.getProperty("simulation.rid.trace");
    }
    
    /**
     * @return simulation allocation trace location
     */
    public String getSimulationAllocationTraceLocation() {
        return this.getProperty("simulation.allocation.trace");
    }
    
    /**
     * @return simulation sub-module input name
     */
    public String getSimulationS3SubModuleInputName() {
        return this.getProperty("simulation.s3.submodule.input.name");
    }

    /**
     * @return simulation closet input
     */
    public String getSimulationClosetInput() {
        return this.getProperty("simulation.closet.input");
    }
    
    /**
     * 
     * @return flag from simulation.sku.rid.snapshot
     */
    public boolean getSimulationSkuRidSnapshotFlag() {
        boolean flag = false;
        String flagString = this.getProperty("simulation.sku.rid.snapshot");
        if ("yes".equalsIgnoreCase(flagString)) {
            flag = true;
        }
        return flag;
    }
    
    /**
     * @return simulation sku availability input
     */
    public String getSimulationSKUAvailabilityInput() {
        return this.getProperty("simulation.sku.availability.input");
    }
    
    /**
     * @return simulation rid input
     */
    public String getSimulationRIDInput() {
        return this.getProperty("simulation.rid.input");
    }

    /**
     * @return simulation user plan
     */
    public String getSimulationUserPlan() {
        return this.getProperty("simulation.user.plan");
    }

    /**
     * @return simulation sku return plan
     */
    public String getSimulationSKUReturnPlan() {
        return this.getProperty("simulation.sku.return.plan");
    }

    /**
     * @return simulation customer return data
     */
    public String getSimulationCustomerReturnData() {
        return this.getProperty("simulation.customer.return.data");
    }
    
    /**
     * @return simulation metadata log name
     */
    public String getSimulationMetaDataName() {
        return this.getProperty("simulation.metadata.log.name");
    }
    
    /**
     * @return simulation notes
     */
    public String getSimulationNotes() {
        return this.getProperty("simulation.notes");
    }

    /**
     * @return async service url
     */
    public String getAsyncServiceUrl() {
        return this.getProperty("async.service.url");
    }
 
    /**
     * @return allocation engine job name
     */
    public String getAllocationEngineJobName() {
        return this.getProperty("allocation.engine.job.name");
    }
}
