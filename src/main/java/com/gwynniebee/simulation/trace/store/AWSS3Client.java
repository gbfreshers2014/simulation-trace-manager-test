/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.simulation.trace.store;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.PropertiesCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.ClientConfiguration;
import com.amazonaws.services.s3.model.Bucket;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;

/**
 * AWS S3 API Wrapper class.
 *
 */
public class AWSS3Client {
    private static final Logger LOG = LoggerFactory.getLogger(AWSS3Client.class.getName());

    public static final String AWS_CONFIG = "/aws.credentials.properties";
    public static final String CSV_CONTENT_TYPE = "application/json";

    private AmazonS3 s3client = null;
    private static PropertiesCredentials awsCedential = null;
    static {
        try {
            awsCedential = new PropertiesCredentials(AWSS3Client.class.getResourceAsStream(AWS_CONFIG));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     *
     */
    public AWSS3Client() {
        this(awsCedential);
    }

    /**
     * @param awsCredential aws credentials
     */
    public AWSS3Client(AWSCredentials awsCredential) {
        ClientConfiguration config = new ClientConfiguration();
        config.setSocketTimeout(0);
        s3client = new AmazonS3Client(awsCredential, config);
    }

    /**
     * @return AmazonS3 object wrapped inside
     */
    public AmazonS3 get() {
        return s3client;
    }

    /**
     * @param bucketName S3 bucket name
     * @param keyName S3 object key name
     * @return input stream of the S3 object, whose close() must be called to avoid a leak
     */
    public InputStream getInputStream(String bucketName, String keyName) {
        InputStream inputStream = null;

        if (s3client != null) {
            S3Object s3obj = s3client.getObject(bucketName, keyName);
            if (s3obj != null) {
                inputStream = s3obj.getObjectContent();
            }
        }

        return inputStream;
    }

    /**
     * @param bucketName bucket name
     * @param keyName object key name
     * @return String content
     */
    public String getString(String bucketName, String keyName) {
        InputStream is = null;
        try {
            is = this.getInputStream(bucketName, keyName);
            return IOUtils.toString(new InputStreamReader(is, "UTF-8"));
        } catch (Throwable e) {
            throw new RuntimeException(e);
        } finally {
            if (is != null) {
                IOUtils.closeQuietly(is);
            }
        }
    }

    /**
     * @param bucketName S3 bucket name
     * @return true if the bucket already exist
     */
    public boolean checkAndCreateBucket(String bucketName) {
        boolean result = false;

        try {
            // Check if the bucket exists. The high availability engineering of
            // Amazon S3 is focused on get, put, list, and delete operations.
            // Because bucket operations work against a centralized, global
            // resource space, it is not appropriate to make bucket create or
            // delete calls on the high availability code path of your
            // application.
            // It is better to create or delete buckets in a separate
            // initialization
            // or setup routine that you run less often.
            if (!this.get().doesBucketExist(bucketName)) {
                LOG.debug("Creating bucket " + bucketName);
                Bucket bucket = this.get().createBucket(bucketName);
                result = (bucket != null) && bucket.getName().equals(bucketName);
                if (result) {
                    LOG.info("Bucket " + bucketName + " created.");
                } else {
                    LOG.error("Bucket " + bucketName + " NOT created.");
                }
            } else {
                result = true;
                LOG.debug("Bucket " + bucketName + " already exists.");
                LOG.debug("No need to recreate.");
            }
        } catch (Exception exception) {
            LOG.error("checkAndCreateBucket() Error for bucket: " + bucketName + ": " + exception.getMessage());
        }

        return result;
    }

    /**
     * @param bucketName S3 bucket name
     * @param objectKeyName S3 object key name
     * @param bytes bytes
     * @param contentType content type
     * @return true if success
     */
    public boolean putObject(String bucketName, String objectKeyName, byte[] bytes, String contentType) {
        boolean result = false;

        LOG.debug("Putting object " + objectKeyName + " in bucket " + bucketName);

        if (this.checkAndCreateBucket(bucketName)) {
            try {
                InputStream stream = new ByteArrayInputStream(bytes);

                ObjectMetadata meta = new ObjectMetadata();
                meta.setContentType(contentType);
                int contentLength = 0;
                if (bytes != null) {
                    contentLength = bytes.length;
                }
                meta.setContentLength(contentLength);

                if (null != this.get().putObject (new PutObjectRequest (bucketName, objectKeyName, stream, meta))) {
                    result = true;
                }
            } catch (Exception exception) {
                LOG.error("putObject() Error for bucket: " + bucketName + ", object: " + objectKeyName + ": "
                        + exception.getMessage ());
            }
        } else {
            LOG.debug("Can not create bucket " + bucketName);
        }

        return result;
    }
}
