/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.simulation.trace.store;

import java.io.IOException;
import java.io.InputStream;

/** A S3 based implementation of providing inventory data stream for deployment.
 *
 * @author xyu
 *
 */
public class S3InputStreamProvider implements InputStreamProvider {
    private AWSS3Client s3Client;
    private String bucketName;

    /**
     * @param bucketName S3 bucket name
     */
    public S3InputStreamProvider(String bucketName) {
        this(new AWSS3Client(), bucketName);
    }

    /**
     * @param s3Client AWSS3Client
     * @param bucketName S3 bucket name
     */
    public S3InputStreamProvider(AWSS3Client s3Client, String bucketName) {
        this.s3Client = s3Client;
        this.bucketName = bucketName;
    }

    @Override
    public InputStream getInputStream(String location) throws IOException {
        if (s3Client != null) {
            return s3Client.getInputStream(this.bucketName, location);
        } else {
            return null;
        }
    }
    
    /**
     * 
     * @return
     */
    public AWSS3Client getS3Client() {
        return this.s3Client;        
    }
}
