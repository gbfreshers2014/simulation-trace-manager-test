/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.simulation.trace.store;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

/** A file system based implementation of providing inventory data stream
 * as mock for component and unit testing.
 *
 * @author xyu
 *
 */
public class FileInputStreamProvider implements InputStreamProvider {
    @Override
    public InputStream getInputStream(String location) throws IOException {
        return new FileInputStream(location);
    }
    
    public AWSS3Client getS3Client() {
        return null;
    }
}
