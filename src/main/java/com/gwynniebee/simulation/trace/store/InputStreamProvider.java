/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.simulation.trace.store;

import java.io.IOException;
import java.io.InputStream;

/** This interface abstract the role of providing a data stream for inventory data loading
 * so that we can support S3 based implementation and file system based implementation for testing
 * most of rest of logic other than S3.
 *
 * In order to prevent underlying resource leak, the client must call InputStream.close() explicitly
 * soon after the stream is not required.
 *
 * The application should hold one instance of this provider in application scope.
 *
 * In order to allow testing, application should allow a setter to override the provider instance.
 *
 * @author xyu
 *
 */
public interface InputStreamProvider {
    /**
     * @param location location
     * @return input stream
     * @throws IOException IOException
     */
    InputStream getInputStream(String location) throws IOException;
    
    /**
     * get s3 client in order to keep a explicit reference to avoid GC during read of large files
     * @return
     */
    AWSS3Client getS3Client();
}
