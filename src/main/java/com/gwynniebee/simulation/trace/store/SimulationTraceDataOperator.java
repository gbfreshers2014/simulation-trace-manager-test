/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.simulation.trace.store;

import java.util.Map;

import com.gwynniebee.matching.setup.MatchingSetup;
import com.gwynniebee.matching.result.RIDAllocation;
import com.gwynniebee.simulation.trace.object.Closet;
import com.gwynniebee.simulation.trace.object.ClosetTrace;
import com.gwynniebee.simulation.trace.object.SKUAvailability;
import com.gwynniebee.simulation.trace.object.SKUAvailabilityTrace;
import com.gwynniebee.simulation.trace.object.SKUReturn;
import com.gwynniebee.simulation.trace.object.RID;
import com.gwynniebee.simulation.trace.object.RIDTrace;
import com.gwynniebee.simulation.trace.object.SimulationMetrics;
import com.gwynniebee.simulation.trace.object.UserPlan;
import com.gwynniebee.simulation.trace.object.SKUReturnPlan;
import com.gwynniebee.simulation.trace.object.CustomerReturnData;


/** Simulation trace data loader interface
*
* @author dj
*
*/
public interface SimulationTraceDataOperator {
    /**
     * @return simulationLastSequenceId
     */
    public int getSimulationLastSequenceId();
    
    /**
     * @param simulationMaxRid simulationMaxRid
     */
    public void setSimulationMaxRid(int simulationMaxRid);
    
    /**
     * @return simulationMaxRid
     */
    public int getSimulationMaxRid();

    /**
     * @return s3SimulationInputPrefix
     */
    public String getS3SimulationInputPrefix();
    
    /**
     * @return s3SimulationTraceManagerPrefix
     */
    public String getS3SimulationTraceManagerPrefix();

    /**
     * @return s3SimulationMatchingSetupPrefix
     */
    public String getS3SimulationMatchingSetupPrefix();

    /**
     * @return s3SimulationMatchingResultPrefix
     */
    public String getS3SimulationMatchingResultPrefix();
    
    /**
     * @param sequenceID sequenceID
     * @param s3Location s3Location
     * @return Map<String, Closet>
     */
    Map<String, Closet> fetchClosetData(int sequenceID, String s3Location);

    /**
     * @param closetData closetData
     * @param s3Location s3Location
     * @return 
     */
    void storeClosetData(Map<String, Closet> closetData, String s3Location);

    /**
     * @param fromSequenceID fromSequenceID
     * @param toSequenceID toSequenceID
     * @param initialization initialization
     * @return ClosetTrace
     */
    ClosetTrace fetchClosetTraceData(int fromSequenceID, int toSequenceID, boolean initialization, boolean clone);

    /**
     * @param sequenceID sequenceID
     * @param s3Location s3Location
     * @return Map<String, SKUAvailability>
     */
    Map<String, SKUAvailability> fetchSKUAvailabilityData(int sequenceID, String s3Location);

    /**
     * 
     * @param skuAvailabilityData
     * @param s3Location
     */
    void storeSKUAvailabilityData(Map<String, SKUAvailability> skuAvailabilityData, String s3Location);

    /**
     * 
     * @param skuReturnData
     * @param s3Location
     */
    void storeSKUReturnData(Map<String, SKUReturn> skuReturnData, String s3Location);

    /**
     * @param fromSequenceID fromSequenceID
     * @param toSequenceID toSequenceID
     * @return SKUAvailabilityTrace
     */
    SKUAvailabilityTrace fetchSKUAvailabilityTraceData(int fromSequenceID, int toSequenceID);

    /**
     *
     */
    Map<String, RID> fetchRIDInput();

    /**
     * @param sequenceID sequenceID
     * @param s3Location s3Location
     * @return Map<String, RID>
     */
    Map<String, RID> fetchRIDData(int sequenceID, String s3Location);
    
    /**
     * 
     * @param ridData
     * @param s3Location
     * @param rounds_increment
     */
    void storeRIDData(Map<String, RID> ridData, String s3Location, int rounds_increment);

    /**
     * @param fromSequenceID fromSequenceID
     * @param toSequenceID toSequenceID
     * @return RIDTrace
     */
    RIDTrace fetchRIDTraceData(int fromSequenceID, int toSequenceID);

    /**
     * @return Map<String, UserPlan>
     */
    Map<String, UserPlan> fetchUserPlanData();
    
    /**
     * @return Map<String, SKUReturnPlan>
     */
    Map<String, SKUReturnPlan> fetchSKUReturnPlanData();

    /**
     * @return Map<String, CustomerReturnData>
     */
    Map<String, CustomerReturnData> fetchCustomerReturnData();
    
    /**
     * @param s3Location s3Location
     * @return Map<String, RIDAllocation>
     */
    Map<String, RIDAllocation> fetchRIDAllocationData(String s3Location);
    
    /**
     * 
     * @param ridAllocationData
     * @param s3Location
     */
    void storeAllocationData(Map<String, RIDAllocation> ridAllocationData, String s3Location);

    /**
     * 
     * @param matchingSetup
     * @param s3Location
     * @param dateTimeString
     */
    void storeMatchingSetup(MatchingSetup matchingSetup, String s3Location, String dateTimeString);
    
    /**
     * 
     * @param simulationMetrics
     * @param s3Location
     */
    void storeSimulationMetrics(SimulationMetrics simulationMetrics, String s3Location);
    
    /**
     * 
     */
    void storeSimulationMetaData();

}
