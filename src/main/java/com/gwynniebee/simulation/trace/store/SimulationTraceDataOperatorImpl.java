/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.simulation.trace.store;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;

import org.apache.commons.io.IOUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csvreader.CsvReader;
import com.gwynniebee.simulation.trace.manager.SimulationTraceManagerProperties;
import com.gwynniebee.simulation.trace.store.InputStreamProvider;
import com.gwynniebee.simulation.trace.store.S3InputStreamProvider;
import com.gwynniebee.simulation.trace.store.AWSS3Client;
import com.gwynniebee.simulation.trace.object.Closet;
import com.gwynniebee.simulation.trace.object.ClosetTrace;
import com.gwynniebee.simulation.trace.object.CustomerReturnData;
import com.gwynniebee.simulation.trace.object.RID;
import com.gwynniebee.simulation.trace.object.RIDTrace;
import com.gwynniebee.simulation.trace.object.SKUAvailability;
import com.gwynniebee.simulation.trace.object.SKUAvailabilityTrace;
import com.gwynniebee.simulation.trace.object.SKUReturn;
import com.gwynniebee.simulation.trace.object.SKUReturnPlan;
import com.gwynniebee.simulation.trace.object.UserPlan;
import com.gwynniebee.simulation.trace.object.SimulationMetrics;
import com.gwynniebee.matching.result.RIDAllocation;
import com.gwynniebee.matching.result.MatchingResult;
import com.gwynniebee.matching.setup.MatchingSetup;

/**
 * @author dj
 *
 */
public class SimulationTraceDataOperatorImpl implements SimulationTraceDataOperator {
    private static final Logger LOG = LoggerFactory.getLogger(SimulationTraceDataOperatorImpl.class);
     
    public static final String SIMULATION_LAST_SEQUENCE_META_DATA = "simulationLastSequenceMetaData";
    public static final String CLOSET = "Closet";
    public static final String CLOSET_TRACE = "ClosetTrace";
    public static final String SKU_AVAILABILITY_TRACE = "SKUAvailabilityTrace";
    public static final String RID_TRACE = "RIDTrace";
    public static final String ALLOCATION_TRACE = "AllocationTrace";
    public static final String LATEST_MATCHING_RESULT_FILE_KEYS = "latestMatchingResultFileKeys";
    public static final String LATEST_MATCHING_SETUP_FILE_KEYS = "latestMatchingSetupFileKeys";
    public static final String CLOSET_STORE_HEADER = "uuid,sku,orderId";
    public static final String SKU_STORE_HEADER = "sku,countAtGB,countAtCustomer,countAtLaundry";
    public static final String SKU_RETURN_STORE_HEADER = "uuid,sku,state";
    public static final String RID_STORE_HEADER = "uuid,rid,countPendingSlots,roundsInQueue";
    public static final String ALLOCATION_STORE_HEADER = "uuid,rid,allocatedSKU";

    private final SimulationTraceManagerProperties props;

    private int namespaceId = -1;
    private int simulationLastSequenceId = 0;
    private int simulationMaxRid = 0;
    private DateTime dateNow = null;
    private String simulationTimeString = null;
    private String s3SimulationInputPrefix = null;
    private String s3SimulationTraceManagerPrefix = null;
    private String s3SimulationMatchingSetupPrefix = null;
    private String s3SimulationMatchingResultPrefix = null;
    
    Splitter SPLITTER = Splitter.on(":").omitEmptyStrings().trimResults();
    Splitter SPLITTER_COMMA = Splitter.on(",").omitEmptyStrings().trimResults();

    /**
     * @param props properties
     */
    public SimulationTraceDataOperatorImpl(SimulationTraceManagerProperties props) {
        this.props = props;
        this.namespaceId = props.getNamespaceId();
        LOG.info("namespaceId >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>.>>>.   "+ this.namespaceId);

        String lastSequenceMetaDataLocation = props.getSimulationS3ModuleName() + "/" 
                                              + this.namespaceId + "/" 
                                              + props.getSimulationS3SubModuleTraceManagerName()
                                              + SIMULATION_LAST_SEQUENCE_META_DATA;
        fetchS3SimulationLastSequenceMetaData(lastSequenceMetaDataLocation);  // get simulation time string and last sequence id
        
        // prepare s3 location prefix for all the data sources
        String timeString = getSimulationTimeString();
        String datePart = timeString.substring(0,4) + "/" + timeString.substring(4,6) + "/" + timeString.substring(6,8);
        this.s3SimulationInputPrefix = props.getSimulationS3ModuleName() + "/" + this.namespaceId + "/" + props.getSimulationS3SubModuleInputName();
        this.s3SimulationTraceManagerPrefix = props.getSimulationS3ModuleName() + "/" 
                                              + this.namespaceId + "/" + props.getSimulationS3SubModuleTraceManagerName() 
                                              + "/" + datePart + "/" + timeString;
        this.s3SimulationMatchingSetupPrefix = props.getSimulationS3ModuleName() + "/" + this.namespaceId + "/" + props.getSimulationS3SubModuleMatchingSetupName();
        this.s3SimulationMatchingResultPrefix = props.getSimulationS3ModuleName() + "/" + this.namespaceId + "/" + props.getSimulationS3SubModuleMatchingResultName();        
    }
        
    /**
     * @return namespaceId
     */
    public int getNamespaceId() {
        return this.namespaceId;
    }
    
    /**
     * @param simulationTimeString simulationTimeString
     */
    public void setSimulationTime(String simulationTimeString) {
        if (simulationTimeString == null) {
            dateNow = DateTime.now();
            dateNow = dateNow.withMillisOfSecond(0).withZone(DateTimeZone.UTC);
            this.simulationTimeString = DateTimeFormat.forPattern("yyyyMMdd-HHmmss").withZoneUTC().print(dateNow);
        }
        else {
            this.simulationTimeString = simulationTimeString;
        }
    }
    
    /**
     * @return simulationTimeString
     */
    public String getSimulationTimeString() {
        return this.simulationTimeString;
    }
    
    /**
     * @param simulationLastSequenceId simulationLastSequenceId
     */
    public void setSimulationLastSequenceId(int simulationLastSequenceId) {
        this.simulationLastSequenceId = simulationLastSequenceId;
    }
    
    /**
     * @return simulationLastSequenceId
     */
    public int getSimulationLastSequenceId() {
        return this.simulationLastSequenceId;
    }
    
    /**
     * @param simulationMaxRid simulationMaxRid
     */
    public void setSimulationMaxRid(int simulationMaxRid) {
        this.simulationMaxRid = simulationMaxRid;
    }
    
    /**
     * @return simulationMaxRid
     */
    public int getSimulationMaxRid() {
        return this.simulationMaxRid;
    }

    /**
     * @return s3SimulationInputPrefix
     */
    public String getS3SimulationInputPrefix() {
        return this.s3SimulationInputPrefix;        
    }
    
    /**
     * @return s3SimulationTraceManagerPrefix
     */
    public String getS3SimulationTraceManagerPrefix() {
        return this.s3SimulationTraceManagerPrefix;        
    }

    /**
     * @return s3SimulationMatchingSetupPrefix
     */
    public String getS3SimulationMatchingSetupPrefix() {
        return this.s3SimulationMatchingSetupPrefix;        
    }

    /**
     * @return s3SimulationMatchingResultPrefix
     */
    public String getS3SimulationMatchingResultPrefix() {
        return this.s3SimulationMatchingResultPrefix;        
    }

    /**
     * simulationLastSequenceID contains simulationTimeString:sequenceID:maxRID
     * @param s3Location s3Location
     */
    public void fetchS3SimulationLastSequenceMetaData(String s3Location) {
        String location = SIMULATION_LAST_SEQUENCE_META_DATA;
        String lastSequenceMetaDataString = null;
        InputStreamProvider provider = new S3InputStreamProvider(this.props.getSimulationS3BucketName());
        InputStream is = null;
        LOG.info("Start fetchS3SimulationLastSequenceMetaData");
        // fetch simulation time, last sequence id, and max rid
        if (SIMULATION_LAST_SEQUENCE_META_DATA.equals(location)) {
            location = s3Location + "/" + location;
            LOG.info("LAST SEQUENCE META DATA LOCATION"+ location);
            try {
                is = provider.getInputStream(location);
                lastSequenceMetaDataString = IOUtils.toString(new InputStreamReader(is, "UTF-8"));
            } catch (Exception e) {
                LOG.info("fetchS3SimulationLastSequenceMetaData failed: " + e.getMessage());
            } finally {
                if (is != null) {
                    IOUtils.closeQuietly(is);
                }
                if (Strings.isNullOrEmpty(lastSequenceMetaDataString)) {  
                    // no data fetched, implying that this is the first round of simulation
                    setSimulationTime(null);    // set simulation time to now
                    setSimulationLastSequenceId(this.props.getSimulationStartSequenceId()-1); 
                    setSimulationMaxRid(0);
                    LOG.info("Create new simulation with time string = " + getSimulationTimeString() + 
                             ", last sequence id = " + getSimulationLastSequenceId() +
                             ", max rid = " + getSimulationMaxRid());
                }
                else {  // data for last sequence meta data, implying it is in the middle of simulation
                    List<String> dataEntries = Lists.newArrayList(SPLITTER.split(lastSequenceMetaDataString));
                    int entryIndex = 0;
                    for (String entry : dataEntries) {
                        if (entryIndex == 0) {
                            setSimulationTime(entry);   // carry the simulation time that was set at the beginning of simulation
                        }
                        else if (entryIndex == 1) {
                            setSimulationLastSequenceId(Integer.parseInt(entry));
                        }
                        else if (entryIndex == 2) {
                            setSimulationMaxRid(Integer.parseInt(entry));
                        }
                        entryIndex ++;
                    }
                    LOG.info("fetchS3SimulationLastSequenceId with time string = " + getSimulationTimeString() + 
                            ", last sequence id = " + getSimulationLastSequenceId() +
                            ", max rid = " + getSimulationMaxRid());
                }
            }
        }
        LOG.info("End fetchS3SimulationLastSequenceMetaData");
    }
    
    /**
     * @param sequenceID sequenceID
     * @param s3Location s3Location
     * @return Map<String, Closet>
     */
    public Map<String, Closet> fetchClosetData(int sequenceID, String s3Location) {
        Map<String, Closet> closetData = null;
        Map<String, Integer> uniqueUuidSkuPair = new HashMap<String, Integer>();    // for deduping
        String closetDataLocation = s3Location + "/" + String.valueOf(sequenceID);
        InputStreamProvider provider = new S3InputStreamProvider(this.props.getSimulationS3BucketName());
        InputStream is = null;
        // BufferedReader readerS3 = null;
        
        LOG.info("Start fetchClosetData sequence " + sequenceID + " from " + s3Location);
        // fetch closet data in csv format
        try {
            is = provider.getInputStream(closetDataLocation);
            // readerS3 = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));

            try {
                CsvReader reader = null;
                reader = new CsvReader(is, ',', Charset.forName("UTF-8"));
                reader.readHeaders();
                while (reader.readRecord()) {
                // String line = null;
                // line = readerS3.readLine(); // header
                // while ((line = readerS3.readLine()) != null) {
                    AWSS3Client s3Client = provider.getS3Client();  // keep an explicit reference to avoid GC 
                                                                    // that may close socket while reading large file
                    try {
                        String uuid = reader.get(Closet.UUID);
                        String sku = reader.get(Closet.SKU);
                        String order = reader.get(Closet.ORDER);
                        int orderId = -1;
                        if (!"NULL".equals(order)) {
                            orderId = Integer.parseInt(order);
                        }
                        // List<String> dataEntries = Lists.newArrayList(SPLITTER_COMMA.split(line));
                        // String uuid = dataEntries.get(0);
                        // String sku = dataEntries.get(1);
                        // String order = dataEntries.get(2);
                        // int orderId = -1; 
                        // if (!"NULL".equals(order)) {
                            // orderId = Integer.parseInt(order);
                        // }

                        String uuidSkuPair = uuid + ":" + sku;
                        if (!uniqueUuidSkuPair.containsKey(uuidSkuPair)) {
                            uniqueUuidSkuPair.put(uuidSkuPair, 0);
                            Closet closet;
                            if (closetData == null) {
                                closetData = new HashMap<String, Closet>();
                            }
                            if (closetData.containsKey(uuid)) {
                                closet = closetData.get(uuid);
                            }
                            else {
                                closet = new Closet();
                                closetData.put(uuid, closet);
                            }
                            closet.setUuid(uuid);
                            closet.addSku(sku);
                            if (orderId > 0) {
                                closet.addOrder(sku, orderId);
                            }
                        }
                        else {  // the same (uuid, sku) has already been processed
                            int countDup = uniqueUuidSkuPair.get(uuidSkuPair);
                            uniqueUuidSkuPair.put(uuidSkuPair, countDup + 1);   // record number of duplicates
                        }
                        
                    } catch (Exception e) {
                        LOG.info("fetchClosetData error in reading record for sequence " + sequenceID + " at " + closetDataLocation);
                        LOG.info(e.getMessage(), e);                        
                    }
                }
                reader.close();

                for (String key : uniqueUuidSkuPair.keySet()) {
                    List<String> dataEntries = Lists.newArrayList(SPLITTER.split(key));
                    int entryIndex = 0;
                    String uuid = null;
                    String sku = null;
                    for (String entry : dataEntries) {
                        if (entryIndex == 0) {
                            uuid = entry;
                        }
                        else if (entryIndex == 1) {
                            sku = entry;
                        }
                        entryIndex ++;
                    }
                    if (uniqueUuidSkuPair.get(key) > 0) {
                        LOG.info("uuid-sku pair (" + uuid + ", " + sku + ") has " + String.valueOf(uniqueUuidSkuPair.get(key) + " duplicates."));
                        LOG.info("only first instance is loaded and the rest are ignored.");
                    }
                }
            } catch (Exception e) {
                LOG.info("fetchClosetData error in obtaining csv reader for sequence " + sequenceID + " at " + closetDataLocation);
                LOG.info(e.getMessage(), e);
            }
        } catch (Exception e) {
            LOG.info("fetchClosetData error in obtaining input stream for sequence " + sequenceID + " at " + closetDataLocation);
        } finally {
            if (is != null) {
                // try {
                    // readerS3.close();
                // } catch (Exception e) {
                    // LOG.info("Buffered read readerS3 close : " + e.getMessage());
                // }
                IOUtils.closeQuietly(is);
            }
        }
        
        LOG.info("End fetchClosetData sequence " + sequenceID + " from " + s3Location);        
        return closetData;
    }
    
    /**
     * @param closetData closetData
     * @param s3Location s3Location
     * @return 
     */
    public void storeClosetData(Map<String, Closet> closetData, String s3Location) {
        StringBuilder closetToStore = null;
        LOG.info("Start storeClosetData");
        String bucketName = this.props.getSimulationS3BucketName();
        AWSS3Client s3client = new AWSS3Client();
        closetToStore = new StringBuilder();
        closetToStore.append(CLOSET_STORE_HEADER + System.getProperty("line.separator"));
        if (closetData != null) {
            for (String uuid : closetData.keySet()) {
                for (String sku : closetData.get(uuid).getSkus()) {
                    if (closetData.get(uuid).getOrders() != null && closetData.get(uuid).getOrders().containsKey(sku)) {
                        closetToStore.append(uuid + "," + sku + "," + closetData.get(uuid).getOrders().get(sku) + System.getProperty("line.separator"));
                    }
                    else {
                        closetToStore.append(uuid + "," + sku + ",NULL" + System.getProperty("line.separator"));                        
                    }
                }
            }
        }
        try {
            s3client.putObject(bucketName, s3Location, closetToStore.toString().getBytes("UTF-8"), "text/plain");    
            LOG.info("Uploaded closet data to S3 : " + bucketName + "." + s3Location);
        } catch (Exception e) {
            LOG.error("storeClosetData failed : " + e.getMessage());
            LOG.info("Cannot upload closet data " + s3Location + " to S3");
        }
        LOG.info("End storeClosetData");
    }
    
    /**
     * @param closetData closetData to be cloned
     * @return cloned closet data
     */
    private Map<String, Closet> cloneCloset(Map<String, Closet> closetData) {
        Map<String, Closet> clonedClosetData = null;

        if (closetData != null) {
            clonedClosetData = new HashMap<String, Closet>();
            for (String key : closetData.keySet()) {
                Closet closet = new Closet();
                closet.setUuid(closetData.get(key).getUuid());
                closet.setSkus(closetData.get(key).getSkus());
                closet.setOrders(closetData.get(key).getOrders());
                clonedClosetData.put(key, closet);
            }
        }
        
        return clonedClosetData;
    }

    /**
     * @param fromSequenceID fromSequenceID
     * @param toSequenceID toSequenceID
     * @param initialization whether it is initialization
     * @return ClosetTrace
     */
    public ClosetTrace fetchClosetTraceData(int fromSequenceID, int toSequenceID, boolean initialization, boolean clone) {
        ClosetTrace closetTrace = null;
        String closetTraceDataLocation = null;
        
        LOG.info("Start fetchClosetTraceData from sequence " + fromSequenceID + " to sequence " + toSequenceID);
        if (initialization) {
            // fetch from the input location
            closetTraceDataLocation = getS3SimulationInputPrefix() + "/" + CLOSET;
            LOG.info("Initialization of simulation");
        }
        else {
            // fetch from trace manager output location
            closetTraceDataLocation = getS3SimulationTraceManagerPrefix() + "/" + CLOSET_TRACE;
            LOG.info("In the middle of simulation");
        }
        
        for (int i = fromSequenceID; i <= toSequenceID; i ++) {
            Map<String, Closet> closetData = fetchClosetData(i, closetTraceDataLocation);
            if (closetData != null) {
                if (closetTrace == null) {
                    closetTrace = new ClosetTrace();
                }
            }
            else {
                if (i == fromSequenceID) {
                    LOG.error("Sequence " + i + " does not have closet data. It has to have valid data to start the sequence of simulation");
                    break;  // the sequence ID to start with has to have closet data
                }
                else {
                    Map<String, Closet> previousCloset = closetTrace.getSequence(i - 1);
                    if (clone) {
                        // clone the closet data from last sequence ID
                        LOG.info("Start clone closet from sequence " + (i-1) + " to sequence " + i);
                        closetData = cloneCloset(previousCloset);
                        LOG.info("End clone closet from sequence " + (i-1) + " to sequence " + i);
                    }
                    else {
                        closetData = previousCloset;
                    }
                }
            }
            closetTrace.setSequence(i, closetData);
        }
        
        LOG.info("End fetchClosetTraceData from sequence " + fromSequenceID + " to sequence " + toSequenceID);
        return closetTrace;
    }

    /**
     * @param sequenceID sequenceID
     * @param String s3Location s3Location
     * @return Map<String, SKUAvailability>
     */
    public Map<String, SKUAvailability> fetchSKUAvailabilityData(int sequenceID, String s3Location) {
        Map<String, SKUAvailability> skuAvailabilityData = null;
        Map<String, Integer> uniqueSku = new HashMap<String, Integer>();    // for counting duplicates
        String skuAvailabilityDataLocation;
        if (sequenceID <= 0) {  // initial input
            skuAvailabilityDataLocation = s3Location;
        }
        else {  // traces generated during simulation
            skuAvailabilityDataLocation = s3Location + "/" + String.valueOf(sequenceID);
        }
        InputStreamProvider provider = new S3InputStreamProvider(this.props.getSimulationS3BucketName());
        InputStream is = null;
                        
        LOG.info("Start fetchSKUAvailabilityData sequence " + sequenceID + " from " + s3Location);
        // fetch sku availability data in csv format
        try {
            is = provider.getInputStream(skuAvailabilityDataLocation);
            CsvReader reader = null;
            try {
                reader = new CsvReader(is, ',', Charset.forName("UTF-8"));
                reader.readHeaders();
                while (reader.readRecord()) {
                    try {
                        String sku = reader.get(SKUAvailability.SKU);
                        int atGwynnieBee = Integer.parseInt(reader.get(SKUAvailability.ATGB));
                        int atCustomer = Integer.parseInt(reader.get(SKUAvailability.ATCUSTOMER));
                        int atLaundry = Integer.parseInt(reader.get(SKUAvailability.ATLAUNDRY));
                        if (!uniqueSku.containsKey(sku)) {
                            uniqueSku.put(sku, 0);
                            SKUAvailability skuAvailability;
                            if (skuAvailabilityData == null) {
                                skuAvailabilityData = new HashMap<String, SKUAvailability>();
                            }
                            skuAvailability = new SKUAvailability();
                            skuAvailability.setSku(sku);
                            if (this.props.getSimulationSkuRidSnapshotFlag()) { // use input snapshot
                                skuAvailability.setAtGwynnieBee(atGwynnieBee);
                                skuAvailability.setAtCustomer(atCustomer);
                                skuAvailability.setAtLaundry(atLaundry);
                            }
                            else {
                                
                                LOG.info("// don't use input snapshot then treat all skus available");
                                skuAvailability.setAtGwynnieBee(atGwynnieBee + atCustomer + atLaundry);
                                skuAvailability.setAtCustomer(0);
                                skuAvailability.setAtLaundry(0);                                
                            }
                            skuAvailabilityData.put(sku, skuAvailability);
                            
                        }
                        else {  // the same sku has already been processed
                            int countDup = uniqueSku.get(sku);
                            uniqueSku.put(sku, countDup + 1);   // record number of duplicates
                        }
                        
                    } catch (Exception e) {
                        LOG.info("fetchSKUAvailabilityData error in reading record for sequence " + sequenceID + " at " + skuAvailabilityDataLocation);
                        LOG.info(e.getMessage(), e);                        
                    }
                }
                reader.close();

                for (String key : uniqueSku.keySet()) {
                    if (uniqueSku.get(key) > 0) {
                        LOG.info("sku " + key + " has " + String.valueOf(uniqueSku.get(key) + " duplicates."));
                        LOG.info("only first instance is loaded and the rest are ignored.");
                    }
                }
            } catch (Exception e) {
                LOG.info("fetchSKUAvailabilityData error in obtaining csv reader for sequence " + sequenceID + " at " + skuAvailabilityDataLocation);
                LOG.info(e.getMessage(), e);
            }
        } catch (Exception e) {
            LOG.info("fetchSKUAvailabilityData error in obtaining input stream for sequence " + sequenceID + " at " + skuAvailabilityDataLocation);
        } finally {
            if (is != null) {
                IOUtils.closeQuietly(is);
            }
        }
        
        LOG.info("End fetchSKUAvailabilityData sequence " + sequenceID + " from " + s3Location);                
        return skuAvailabilityData;
    }

    /**
     * 
     * @param skuAvailabilityData
     * @param s3Location
     */
    public void storeSKUAvailabilityData(Map<String, SKUAvailability> skuAvailabilityData, String s3Location) {    
        StringBuilder skuAvailabilityToStore = null;
        LOG.info("Start storeSKUAvailabilityData");
        String bucketName = this.props.getSimulationS3BucketName();
        AWSS3Client s3client = new AWSS3Client();
        skuAvailabilityToStore = new StringBuilder();
        skuAvailabilityToStore.append(SKU_STORE_HEADER + System.getProperty("line.separator"));
        if (skuAvailabilityData != null) {
            for (String sku : skuAvailabilityData.keySet()) {
                skuAvailabilityToStore.append(sku + "," + String.valueOf(skuAvailabilityData.get(sku).getAtGwynnieBee()) + "," + 
                                              String.valueOf(skuAvailabilityData.get(sku).getAtCustomer()) + "," +
                                              String.valueOf(skuAvailabilityData.get(sku).getAtLaundry()) +
                                              System.getProperty("line.separator"));                        
            }
        }
        try {
            s3client.putObject(bucketName, s3Location, skuAvailabilityToStore.toString().getBytes("UTF-8"), "text/plain");    
            LOG.info("Uploaded sku availability data to S3 : " + bucketName + "." + s3Location);
        } catch (Exception e) {
            LOG.error("storeSKUAvailabilityData failed : " + e.getMessage());
            LOG.info("Cannot upload sku availability data " + s3Location + " to S3");
        }
        LOG.info("End storeSKUAvailabilityData");
    }
    
    /**
     * 
     * @param skuReturnData
     * @param s3Location
     */
    public void storeSKUReturnData(Map<String, SKUReturn> skuReturnData, String s3Location) {
        StringBuilder skuReturnToStore = null;
        LOG.info("Start storeSKUReturnData");
        String bucketName = this.props.getSimulationS3BucketName();
        AWSS3Client s3client = new AWSS3Client();
        skuReturnToStore = new StringBuilder();
        skuReturnToStore.append(SKU_RETURN_STORE_HEADER + System.getProperty("line.separator"));
        if (skuReturnData != null) {
            for (String uuid : skuReturnData.keySet()) {
                for (String sku : skuReturnData.get(uuid).getSkuStates().keySet()) {
                    skuReturnToStore.append(uuid + "," + sku + "," + skuReturnData.get(uuid).getSkuStates().get(sku) + System.getProperty("line.separator"));
                }
            }
        }
        try {                
            s3client.putObject(bucketName, s3Location, skuReturnToStore.toString().getBytes("UTF-8"), "text/plain");    
            LOG.info("Uploaded sku return data to S3 : " + bucketName + "." + s3Location);
        } catch (Exception e) {
            LOG.error("storeSKUReturnData failed : " + e.getMessage());
            LOG.info("Cannot upload sku return data " + s3Location + " to S3");
        }
        LOG.info("End storeSKUReturnData");
    }
    
    /**
     * @param fromSequenceID fromSequenceID
     * @param toSequenceID toSequenceID
     * @return SKUAvailabilityTrace
     */
    public SKUAvailabilityTrace fetchSKUAvailabilityTraceData(int fromSequenceID, int toSequenceID) {
        SKUAvailabilityTrace skuAvailabilityTrace = null;
        String skuAvailabilityTraceDataLocation = getS3SimulationTraceManagerPrefix() + "/" + SKU_AVAILABILITY_TRACE;
        
        LOG.info("Start fetchSKUAvailabilityTraceData from sequence " + fromSequenceID + " to sequence " + toSequenceID);         
        for (int i = fromSequenceID; i <= toSequenceID; i ++) {
            Map<String, SKUAvailability> skuAvailabilityData = fetchSKUAvailabilityData(i, skuAvailabilityTraceDataLocation);
            if (skuAvailabilityData != null) {
                if (skuAvailabilityTrace == null) {
                    skuAvailabilityTrace = new SKUAvailabilityTrace();
                }
            }
            else {
                if (i == fromSequenceID) {
                    LOG.error("Sequence " + i + " does not have sku availability data. It has to have valid data to start the sequence of simulation");
                    break;  // the sequence ID to start with has to have sku availability data
                }
            }
            skuAvailabilityTrace.setSequence(i, skuAvailabilityData);   // if skuAvailabilityData is null, it means there is no update data
        }
        
        LOG.info("End fetchSKUAvailabilityTraceData from sequence " + fromSequenceID + " to sequence " + toSequenceID);
        return skuAvailabilityTrace;
    }

    /**
     * Fetch rid input data and re-assign rids to them
     * We have to streamline to rid generation because we will generate new rids during the simulation
     * We don't rely on the rid values from input file but rather re-assign by the simulation system
     */
    public Map<String, RID> fetchRIDInput() {
        Map<String, RID> ridInput = null;
        Map<Integer, Integer> uniqueRID = new HashMap<Integer, Integer>();    // for counting duplicates
        String RIDDataLocation = getS3SimulationInputPrefix() + "/" + this.props.getSimulationRIDInput();
        InputStreamProvider provider = new S3InputStreamProvider(this.props.getSimulationS3BucketName());
        InputStream is = null;
                        
        LOG.info("Start fetchRIDInput from " + RIDDataLocation);
        if (this.props.getSimulationSkuRidSnapshotFlag()) { // use input rid snapshot then load
            // fetch rid data in csv format
            try {
                is = provider.getInputStream(RIDDataLocation);
                CsvReader reader = null;
                try {
                    reader = new CsvReader(is, ',', Charset.forName("UTF-8"));
                    reader.readHeaders();
                    while (reader.readRecord()) {
                        try {
                            String uuid = reader.get(RID.UUID);
                            int rid = Integer.parseInt(reader.get(RID.RID));
                            int slots = Integer.parseInt(reader.get(RID.SLOTS));
                            if (!uniqueRID.containsKey(rid)) {  // remove duplicates of original rids in input
                                uniqueRID.put(rid, 0);
                                if (ridInput == null) {
                                    ridInput = new HashMap<String, RID>();
                                }
                                RID Rid;
                                if (ridInput.containsKey(uuid)) {
                                    // if the same uuid already exists merge the rids for the same uuid
                                    Rid = ridInput.get(uuid);
                                    int newSlots = Rid.getSlots() + slots;
                                    Rid.setSlots(newSlots);
                                }
                                else {
                                    Rid = new RID();
                                    // re-assign rid
                                    int newRid = getSimulationMaxRid() + 1;
                                    setSimulationMaxRid(newRid);
                                    Rid.setRid(newRid);
                                    Rid.setUuid(uuid);
                                    Rid.setSlots(slots);
                                    ridInput.put(uuid, Rid);
                                }
                            }
                            else {  // the same rid has already been processed
                                int countDup = uniqueRID.get(rid);
                                uniqueRID.put(rid, countDup + 1);   // record number of duplicates
                            }
                            
                        } catch (Exception e) {
                            LOG.info("fetchRIDInput error in reading record at " + RIDDataLocation);
                            LOG.info(e.getMessage(), e);                        
                        }
                    }
                    reader.close();
    
                    for (Integer key : uniqueRID.keySet()) {
                        if (uniqueRID.get(key) > 0) {
                            LOG.info("Rid " + key + " has " + String.valueOf(uniqueRID.get(key) + " duplicates."));
                            LOG.info("only first instance is loaded and the rest are ignored.");
                        }
                    }
                } catch (Exception e) {
                    LOG.info("fetchRIDInput error in obtaining csv reader at " + RIDDataLocation);
                    LOG.info(e.getMessage(), e);
                }
            } catch (Exception e) {
                LOG.info("fetchRIDInput error in obtaining input stream at " + RIDDataLocation);
            } finally {
                if (is != null) {
                    IOUtils.closeQuietly(is);
                }
            }
        }
        
        LOG.info("End fetchRIDInput from " + RIDDataLocation);
        return ridInput;
    }
        
    /**
     * @param sequenceID sequenceID
     * @param s3Location s3Location
     * @return Map<String, RID>
     */
    public Map<String, RID> fetchRIDData(int sequenceID, String s3Location) {
        Map<String, RID> ridData = null;
        Map<Integer, Integer> uniqueRID = new HashMap<Integer, Integer>();    // for counting duplicates
        String RIDDataLocation = s3Location + "/" + String.valueOf(sequenceID);
        InputStreamProvider provider = new S3InputStreamProvider(this.props.getSimulationS3BucketName());
        InputStream is = null;
                        
        LOG.info("Start fetchRIDData sequence " + sequenceID + "from " + RIDDataLocation);
        // fetch rid data in csv format
        try {
            is = provider.getInputStream(RIDDataLocation);
            CsvReader reader = null;
            try {
                reader = new CsvReader(is, ',', Charset.forName("UTF-8"));
                reader.readHeaders();
                while (reader.readRecord()) {
                    try {
                        String uuid = reader.get(RID.UUID);
                        int rid = Integer.parseInt(reader.get(RID.RID));
                        int slots = Integer.parseInt(reader.get(RID.SLOTS));
                        int roundsInQueue = Integer.parseInt(reader.get(RID.INQUEUE));
                        if (!uniqueRID.containsKey(rid)) {
                            uniqueRID.put(rid, 0);
                            if (ridData == null) {
                                ridData = new HashMap<String, RID>();
                            }
                            RID Rid;
                            if (ridData.containsKey(uuid)) {
                                // if the same uuid already exists merge the rids for the same uuid
                                Rid = ridData.get(uuid);
                                int newSlots = Rid.getSlots() + slots;
                                Rid.setSlots(newSlots);
                            }
                            else {
                                Rid = new RID();
                                Rid.setUuid(uuid);
                                Rid.setRid(rid);
                                Rid.setSlots(slots);
                                Rid.setRoundsInQueue(roundsInQueue);
                                ridData.put(uuid, Rid);
                            }                           
                        }
                        else {  // the same rid has already been processed
                            int countDup = uniqueRID.get(rid);
                            uniqueRID.put(rid, countDup + 1);   // record number of duplicates
                        }
                        
                    } catch (Exception e) {
                        LOG.info("fetchRIDData error in reading record for sequence " + sequenceID + " at " + RIDDataLocation);
                        LOG.info(e.getMessage(), e);                        
                    }
                }
                reader.close();

                for (Integer key : uniqueRID.keySet()) {
                    if (uniqueRID.get(key) > 0) {
                        LOG.info("Rid " + key + " has " + String.valueOf(uniqueRID.get(key) + " duplicates."));
                        LOG.info("only first instance is loaded and the rest are ignored.");
                    }
                }
            } catch (Exception e) {
                LOG.info("fetchRIDData error in obtaining csv reader for sequence " + sequenceID + " at " + RIDDataLocation);
                LOG.info(e.getMessage(), e);
            }
        } catch (Exception e) {
            LOG.info("fetchRIDData error in obtaining input stream for sequence " + sequenceID + " at " + RIDDataLocation);
        } finally {
            if (is != null) {
                IOUtils.closeQuietly(is);
            }
        }
        
        LOG.info("End fetchRIDData sequence " + sequenceID + " from " + RIDDataLocation);                        
        return ridData;
    }

    /**
     * 
     */
    public void storeRIDData(Map<String, RID> ridData, String s3Location, int rounds_increment) {
        StringBuilder ridToStore = null;
        LOG.info("Start storeRIDData");
        String bucketName = this.props.getSimulationS3BucketName();
        AWSS3Client s3client = new AWSS3Client();
        ridToStore = new StringBuilder();
        ridToStore.append(RID_STORE_HEADER + System.getProperty("line.separator"));
        if (ridData != null) {
            for (String uuid : ridData.keySet()) {
                ridToStore.append(uuid + "," + String.valueOf(ridData.get(uuid).getRid()) + "," +
                                  String.valueOf(ridData.get(uuid).getSlots()) + "," +
                                  String.valueOf(ridData.get(uuid).getRoundsInQueue() / rounds_increment) +
                                  System.getProperty("line.separator"));
            }
        }
        try {
            s3client.putObject(bucketName, s3Location, ridToStore.toString().getBytes("UTF-8"), "text/plain");    
            LOG.info("Uploaded rid data to S3 : " + bucketName + "." + s3Location);
        } catch (Exception e) {
            LOG.error("storeRIDData failed : " + e.getMessage());
            LOG.info("Cannot upload rid data " + s3Location + " to S3");
        }
        LOG.info("End storeRIDData");
    }

    /**
     * @param fromSequenceID fromSequenceID
     * @param toSequenceID toSequenceID
     * @return RIDTrace
     */
    public RIDTrace fetchRIDTraceData(int fromSequenceID, int toSequenceID) {
        RIDTrace ridTrace = null;
        String RIDTraceDataLocation = getS3SimulationTraceManagerPrefix() + "/" + RID_TRACE;
        
        LOG.info("Start fetchRIDTraceData from sequence " + fromSequenceID + " to sequence " + toSequenceID);         
        for (int i = fromSequenceID; i <= toSequenceID; i ++) {
            Map<String, RID> ridData = fetchRIDData(i, RIDTraceDataLocation);
            if (ridData != null) {
                if (ridTrace == null) {
                    ridTrace = new RIDTrace();
                }
            }
            else {
                if (i == fromSequenceID) {
                    LOG.error("Sequence " + i + " does not have rid data. It has to have valid data to start the sequence of simulation");
                    break;  // the sequence ID to start with has to have rid data
                }
            }
            ridTrace.setSequence(i, ridData);   // if ridData is null, it means there is no update data
        }
        
        LOG.info("End fetchRIDTraceData from sequence " + fromSequenceID + " to sequence " + toSequenceID);        
        return ridTrace;
    }

    /**
     * @return Map<String, UserPlan>
     */
    public Map<String, UserPlan> fetchUserPlanData() {
        Map<String, UserPlan> userPlan = null;
        Map<String, Integer> uniqueUser = new HashMap<String, Integer>();    // for counting duplicates
        String userDataLocation = getS3SimulationInputPrefix() + "/" + this.props.getSimulationUserPlan();
        InputStreamProvider provider = new S3InputStreamProvider(this.props.getSimulationS3BucketName());
        InputStream is = null;
                        
        LOG.info("Start fetchUserPlanData from " + userDataLocation);
        // fetch user data in csv format
        try {
            is = provider.getInputStream(userDataLocation);
            CsvReader reader = null;
            try {
                reader = new CsvReader(is, ',', Charset.forName("UTF-8"));
                reader.readHeaders();
                while (reader.readRecord()) {
                    try {
                        String uuid = reader.get(UserPlan.UUID);
                        int slots = Integer.parseInt(reader.get(UserPlan.SLOTS));
                        if (!uniqueUser.containsKey(uuid)) {  // remove duplicates of usr plan in input
                            uniqueUser.put(uuid, 0);
                            if (userPlan == null) {
                                userPlan = new HashMap<String, UserPlan>();
                            }
                            UserPlan user = new UserPlan();
                            user.setUuid(uuid);
                            user.setSlots(slots);
                            userPlan.put(uuid,  user);
                        }
                        else {  // the same user has already been processed
                            int countDup = uniqueUser.get(uuid);
                            uniqueUser.put(uuid, countDup + 1);   // record number of duplicates
                        }
                        
                    } catch (Exception e) {
                        LOG.info("fetchUserPlan error in reading record at " + userDataLocation);
                        LOG.info(e.getMessage(), e);                        
                    }
                }
                reader.close();

                for (String key : uniqueUser.keySet()) {
                    if (uniqueUser.get(key) > 0) {
                        LOG.info("User " + key + " has " + String.valueOf(uniqueUser.get(key) + " duplicates."));
                        LOG.info("only first instance is loaded and the rest are ignored.");
                    }
                }
            } catch (Exception e) {
                LOG.info("fetchUserPlanData error in obtaining csv reader at " + userDataLocation);
                LOG.info(e.getMessage(), e);
            }
        } catch (Exception e) {
            LOG.info("fetchUserPlanData error in obtaining input stream at " + userDataLocation);
        } finally {
            if (is != null) {
                IOUtils.closeQuietly(is);
            }
        }
        
        LOG.info("End fetchUserPlanData from " + userDataLocation);        
        return userPlan;
    }
    
    /**
     * @return Map<String, SKUReturnPlan>
     */
    public Map<String, SKUReturnPlan> fetchSKUReturnPlanData() {
        Map<String, SKUReturnPlan> skuReturnPlan = null;
        Map<String, Integer> uniqueSku = new HashMap<String, Integer>();    // for counting duplicates
        String skuReturnDataLocation = getS3SimulationInputPrefix() + "/" + this.props.getSimulationSKUReturnPlan();
        InputStreamProvider provider = new S3InputStreamProvider(this.props.getSimulationS3BucketName());
        InputStream is = null;
                        
        LOG.info("Start fetchSKUReturnPlanData from " + skuReturnDataLocation);
        // fetch sku return plan in csv format
        try {
            is = provider.getInputStream(skuReturnDataLocation);
            CsvReader reader = null;
            try {
                reader = new CsvReader(is, ',', Charset.forName("UTF-8"));
                reader.readHeaders();
                while (reader.readRecord()) {
                    try {
                        String sku = reader.get(SKUReturnPlan.SKU);
                        int cyclesReturn = Integer.parseInt(reader.get(SKUReturnPlan.RETURN));
                        int cyclesLaundry = Integer.parseInt(reader.get(SKUReturnPlan.LAUNDRY));
                        if (!uniqueSku.containsKey(sku)) {  // remove duplicates of sku return plan in input
                            uniqueSku.put(sku, 0);
                            if (skuReturnPlan == null) {
                                skuReturnPlan = new HashMap<String, SKUReturnPlan>();
                            }
                            SKUReturnPlan skuReturn = new SKUReturnPlan();
                            skuReturn.setSku(sku);
                            skuReturn.setRoundsAtCustomer(cyclesReturn);
                            skuReturn.setRoundsAtLaundry(cyclesLaundry);
                            skuReturnPlan.put(sku, skuReturn);
                        }
                        else {  // the same sku has already been processed
                            int countDup = uniqueSku.get(sku);
                            uniqueSku.put(sku, countDup + 1);   // record number of duplicates
                        }
                        
                    } catch (Exception e) {
                        LOG.info("fetchSKUReturnPlanData error in reading record at " + skuReturnDataLocation);
                        LOG.info(e.getMessage(), e);                        
                    }
                }
                reader.close();

                for (String key : uniqueSku.keySet()) {
                    if (uniqueSku.get(key) > 0) {
                        LOG.info("Sku " + key + " has " + String.valueOf(uniqueSku.get(key) + " duplicates."));
                        LOG.info("only first instance is loaded and the rest are ignored.");
                    }
                }
            } catch (Exception e) {
                LOG.info("fetchSKUReturnPlanData error in obtaining csv reader at " + skuReturnDataLocation);
                LOG.info(e.getMessage(), e);
            }
        } catch (Exception e) {
            LOG.info("fetchSKUReturnPlanData error in obtaining input stream at " + skuReturnDataLocation);
        } finally {
            if (is != null) {
                IOUtils.closeQuietly(is);
            }
        }
        
        LOG.info("End fetchSKUReturnPlanData from " + skuReturnDataLocation);        
        return skuReturnPlan;
    }

    /**
     * @return Map<String, CustomerReturnData>
     */
    public Map<String, CustomerReturnData> fetchCustomerReturnData() {
        Map<String, CustomerReturnData> customerReturnData = null;
        Map<String, Integer> uniqueUserSkuPair = new HashMap<String, Integer>();    // for counting duplicates
        String customerReturnDataLocation = getS3SimulationInputPrefix() + "/" + this.props.getSimulationCustomerReturnData();
        InputStreamProvider provider = new S3InputStreamProvider(this.props.getSimulationS3BucketName());
        InputStream is = null;
                        
        LOG.info("Start fetchCustomerReturnData from " + customerReturnDataLocation);
        // fetch customer return data in csv format
        try {
            is = provider.getInputStream(customerReturnDataLocation);
            CsvReader reader = null;
            try {
                reader = new CsvReader(is, ',', Charset.forName("UTF-8"));
                reader.readHeaders();
                while (reader.readRecord()) {
                    try {
                        String uuid = reader.get(CustomerReturnData.UUID);
                        String sku = reader.get(CustomerReturnData.SKU);
                        int sequenceIDToBeReturned = Integer.parseInt(reader.get(CustomerReturnData.RETURN));
                        String uuidSkuPair = uuid + ":" + sku;
                        if (!uniqueUserSkuPair.containsKey(uuidSkuPair)) {  // remove duplicates
                            uniqueUserSkuPair.put(uuidSkuPair, 0);
                            CustomerReturnData customerReturn;
                            if (customerReturnData == null) {
                                customerReturnData = new HashMap<String, CustomerReturnData>();
                            }
                            if (customerReturnData.containsKey(uuid)) {
                                customerReturn = customerReturnData.get(uuid);
                            }
                            else {
                                customerReturn = new CustomerReturnData();
                                customerReturnData.put(uuid, customerReturn);
                            }
                            customerReturn.addSkusToBeReturned(sku, sequenceIDToBeReturned);
                        }
                        else {  // the same user sku pair has already been processed
                            int countDup = uniqueUserSkuPair.get(uuidSkuPair);
                            uniqueUserSkuPair.put(uuidSkuPair, countDup + 1);   // record number of duplicates
                        }
                        
                    } catch (Exception e) {
                        LOG.info("fetchCustomerReturnData error in reading record at " + customerReturnDataLocation);
                        LOG.info(e.getMessage(), e);                        
                    }
                }
                reader.close();

                for (String key : uniqueUserSkuPair.keySet()) {
                    List<String> dataEntries = Lists.newArrayList(SPLITTER.split(key));
                    int entryIndex = 0;
                    String uuid = null;
                    String sku = null;
                    for (String entry : dataEntries) {
                        if (entryIndex == 0) {
                            uuid = entry;
                        }
                        else if (entryIndex == 1) {
                            sku = entry;
                        }
                        entryIndex ++;
                    }
                    if (uniqueUserSkuPair.get(key) > 0) {
                        LOG.info("uuid-sku pair (" + uuid + ", " + sku + ") has " + String.valueOf(uniqueUserSkuPair.get(key) + " duplicates."));
                        LOG.info("only first instance is loaded and the rest are ignored.");
                    }
                }
            } catch (Exception e) {
                LOG.info("fetchCustomerReturnData error in obtaining csv reader at " + customerReturnDataLocation);
                LOG.info(e.getMessage(), e);
            }
        } catch (Exception e) {
            LOG.info("fetchCustomerReturnData error in obtaining input stream at " + customerReturnDataLocation);
        } finally {
            if (is != null) {
                IOUtils.closeQuietly(is);
            }
        }
        
        LOG.info("End fetchCustomerReturnData from " + customerReturnDataLocation);                
        return customerReturnData;
    }
    
    /**
     * @param s3Location s3Location
     * @return Map<String, RIDAllocation>
     */
    public Map<String, RIDAllocation> fetchRIDAllocationData(String s3Location) {
        Map<String, RIDAllocation> ridAllocationData = null;

        LOG.info("Start fetchRIDAllocationData from " + s3Location);
        // fetch from matching result in S3
        InputStreamProvider provider = new S3InputStreamProvider(this.props.getSimulationS3BucketName());
        InputStream matchingResultInput = null;
        MatchingResult matchingResult = null;
        List<RIDAllocation> listRIDAllocations = null;

        try {
            try {
                matchingResultInput = provider.getInputStream(s3Location);
            } catch (Exception e) {
                LOG.info("fetchRIDAllocationData matchingResultInput failed : " + e.getMessage());
                matchingResultInput = null;
            }
            try {
                if (matchingResultInput != null) {
                    matchingResult = new ObjectMapper().readValue(matchingResultInput, MatchingResult.class);
                    LOG.info("Fetched matching result data");
                }
                else {
                    LOG.info("Cannot fetch matching result from S3");
                }
            } catch (Exception e) {
                LOG.info("fetchRIDAllocationData ObjectMapper failed : " + e.getMessage());
            }
        } finally {
            if (matchingResultInput != null) {
                IOUtils.closeQuietly(matchingResultInput);
            }
        }
        
        if (matchingResult != null) {
            listRIDAllocations = matchingResult.getNewAllocations();
            for (RIDAllocation r : listRIDAllocations) {
                String uuid = r.getUuid();
                int slots = r.getSlots();
                int fulfilledSlots = r.getFulfilledSlots();
                if (slots == fulfilledSlots) {  // count only fully fulfilled rid
                    if (ridAllocationData == null) {
                        ridAllocationData = new HashMap<String, RIDAllocation>();
                    }
                    ridAllocationData.put(uuid, r);
                }
            }
        }
        
        LOG.info("End fetchRIDAllocationData from " + s3Location);
        return ridAllocationData;
    }
    
    /**
     * 
     * @param allocationData
     * @param s3Location
     */
    public void storeAllocationData(Map<String, RIDAllocation> allocationData, String s3Location) {        
        StringBuilder allocationToStore = null;
        LOG.info("Start storeAllocationData");
        String bucketName = this.props.getSimulationS3BucketName();
        AWSS3Client s3client = new AWSS3Client();
        allocationToStore = new StringBuilder();
        allocationToStore.append(ALLOCATION_STORE_HEADER + System.getProperty("line.separator"));
        if (allocationData != null) {                
            for (String uuid : allocationData.keySet()) {    
                for (String sku : allocationData.get(uuid).getAllocatedSKUs()) {                        
                    allocationToStore.append(uuid + "," + String.valueOf(allocationData.get(uuid).getRid()) + "," +
                                             sku + System.getProperty("line.separator"));
                }
            }
        }
        try {
            s3client.putObject(bucketName, s3Location, allocationToStore.toString().getBytes("UTF-8"), "text/plain");    
            LOG.info("Uploaded allocation data to S3 : " + bucketName + "." + s3Location);
        } catch (Exception e) {
            LOG.error("storeAllocationData failed : " + e.getMessage());
            LOG.info("Cannot upload allocation data " + s3Location + " to S3");
        }
        LOG.info("End storeAllocationData");
    }

    /**
     * @param matchingSetup
     * @param s3Location
     */
    public void storeMatchingSetup(MatchingSetup matchingSetup, String s3Location, String dateTimePrefix) {
        LOG.info("Start storeMatchingSetup");
        if (matchingSetup != null) {
            try {
                String bucketName = this.props.getSimulationS3BucketName();
                String inputJson = new ObjectMapper().writeValueAsString(matchingSetup);
                AWSS3Client s3client = new AWSS3Client();
                String fileLocation = dateTimePrefix + "-setup.json";
                String matchingSetupLocation = s3Location + "/" + fileLocation;
                String matchingSetupLatestKey = s3Location + "/" + LATEST_MATCHING_SETUP_FILE_KEYS;
                LOG.info(matchingSetupLatestKey);
                LOG.info(matchingSetupLocation);
                s3client.putObject(bucketName, matchingSetupLocation, inputJson.getBytes("UTF-8"), "application/json");
                s3client.putObject(bucketName, matchingSetupLatestKey, fileLocation.getBytes("UTF-8"), "text/plain");                
            } catch (Exception e) {
                LOG.error("storeMatchingSetup failed : " + e.getMessage());
                LOG.info("Cannot upload matching setup " + s3Location + " to S3");
            }
        }
        else {
            // reset content in LATEST_MATCHING_SETUP_FILE_KEYS to be empty
            try {
                String bucketName = this.props.getSimulationS3BucketName();
                AWSS3Client s3client = new AWSS3Client();
                String fileLocation = "";
                String matchingSetupLatestKey = s3Location + "/" + LATEST_MATCHING_SETUP_FILE_KEYS;
                s3client.putObject(bucketName, matchingSetupLatestKey, fileLocation.getBytes("UTF-8"), "text/plain");                
            } catch (Exception e) {
                LOG.error("storeMatchingSetup failed for reseting content : " + e.getMessage());
                LOG.info("Cannot upload matching setup for reseting content " + s3Location + " to S3");
            }            
        }
        LOG.info("End storeMatchingSetup");
    }
    
    /**
     * 
     * @param simulationMetrics
     * @param s3Location
     */
    public void storeSimulationMetrics(SimulationMetrics simulationMetrics, String s3Location) {
        LOG.info("Start storeSimulationMetrics");
        if (simulationMetrics != null) {
            try {
                String bucketName = this.props.getSimulationS3BucketName();
                String input = new ObjectMapper().writeValueAsString(simulationMetrics);
                AWSS3Client s3client = new AWSS3Client();
                s3client.putObject(bucketName, s3Location, input.getBytes("UTF-8"), "text/plain");
            } catch (Exception e) {
                LOG.error("storeSimulationMetrics failed : " + e.getMessage());
                LOG.info("Cannot upload simulation metrics " + s3Location + " to S3");
            }
        }
        LOG.info("End storeSimulationMetrics");
    }
    
    /**
     * 
     */
    public void storeSimulationMetaData() {
        LOG.info("Start storeSimulationMetaData");
        String bucketName = this.props.getSimulationS3BucketName();
        String s3Location = this.getS3SimulationTraceManagerPrefix() + "/" + this.props.getSimulationMetaDataName();
        StringBuilder metaData = new StringBuilder();
        metaData.append("{");        
        metaData.append("\"bucket\":\"" + this.props.getSimulationS3BucketName() + "\",");
        metaData.append("\"module\":\"" + this.props.getSimulationS3ModuleName() + "\",");
        metaData.append("\"namespace\":\"" + this.props.getNamespaceId() + "\",");
        metaData.append("\"closetInput\":\"" + this.getS3SimulationInputPrefix() + "/Closet\",");
        metaData.append("\"skuAvailabilityInput\":\"" + this.getS3SimulationInputPrefix() + "/SKUAvailability\",");
        metaData.append("\"ridInput\":\"" + this.getS3SimulationInputPrefix() + "/RID\",");
        metaData.append("\"userPlan\":\"" + this.getS3SimulationInputPrefix() + "/UserPlan\",");
        metaData.append("\"skuReturnPlan\":\"" + this.getS3SimulationInputPrefix() + "/SKUReturnPlan\",");
        metaData.append("\"customerReturnPlan\":\"" + this.getS3SimulationInputPrefix() + "/CustomerReturnPlan\",");
        metaData.append("\"startSequenceId\":\"" + this.props.getSimulationStartSequenceId() + "\",");
        metaData.append("\"endSequenceId\":\"" + this.props.getSimulationEndSequenceId() + "\",");
        metaData.append("\"closetTrace\":\"" + this.getS3SimulationTraceManagerPrefix() + "/ClosetTrace\",");
        metaData.append("\"skuAvailabilityTrace\":\"" + this.getS3SimulationTraceManagerPrefix() + "/SKUAvailabilityTrace\",");
        metaData.append("\"ridTrace\":\"" + this.getS3SimulationTraceManagerPrefix() + "/RIDTrace\",");
        metaData.append("\"allocationTrace\":\"" + this.getS3SimulationTraceManagerPrefix() + "/AllocationTrace\",");
        metaData.append("\"simulationMetaData\":\"" + this.getS3SimulationTraceManagerPrefix() + "/SimulationMetaData\",");
        metaData.append("\"simulationNotes\":\"" + this.props.getSimulationNotes() + "\"");
        metaData.append("}");
        try {
            AWSS3Client s3client = new AWSS3Client();
            s3client.putObject(bucketName, s3Location, metaData.toString().getBytes("UTF-8"), "text/plain");
        } catch (Exception e) {
            LOG.error("storeSimulationMetaData failed : " + e.getMessage());
            LOG.info("Cannot upload simulation meta data " + s3Location + " to S3");
        }
        LOG.info("End storeSimulationMetaData");
    }

}
