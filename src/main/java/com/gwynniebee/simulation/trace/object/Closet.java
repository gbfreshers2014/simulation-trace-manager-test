/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.simulation.trace.object;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/** Closet data
 *
 * @author dj
 *
 */
public class Closet {
    public static final String UUID = "uuid";
    public static final String SKU = "sku";
    public static final String ORDER = "orderId";
    
    private String uuid = null;
    private Set<String> skus = null;
    private Map<String, Integer> orders = null;

    /**
     *
     */
    public Closet() {
        super();
    }

    /**
     * @param uuid uuid
     * @param skuList skus
     */
    public Closet(String uuid, String ... skuList) {
        super();
        this.uuid = uuid;
        this.skus = new HashSet<String>();
        if (skuList != null) {
            for (String sku : skuList) {
                this.skus.add(sku);
            }
        }
    }

    /** chaining method to assign orders.
     * @param orderId orderId
     * @return this
     */
    public Closet withOrder(String sku, int orderId) {
        if (this.orders == null) {
            this.orders = new HashMap<String, Integer>();
        }
        this.orders.put(sku, orderId);
        return this;
    }

    /** chaining method to remove a sku from orders.
     * @param sku sku
     * @return this
     */
    public Closet removeOrder(String sku) {
        if (this.orders != null) {
            this.orders.remove(sku);
            if (this.orders.isEmpty()) {
                this.orders = null;
            }
        }
        return this;
    }

    /**
     * @return the uuid
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * @param uuid the uuid to set
     */
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    /**
     * @return the skus
     */
    public Set<String> getSkus() {
        return skus;
    }

    /**
     * @param skus the skus to set
     */
    public void setSkus(Set<String> skus) {
        this.skus = skus;
    }

    /**
     * @param sku the sku to add
     */
    public void addSku(String sku) {
        if (this.skus == null) {
            this.skus = new HashSet<String>();
        }
        this.skus.add(sku);
    }
    
    /**
     * @return the orders
     */
    public Map<String, Integer> getOrders() {
        return orders;
    }

    /**
     * @param orders the orders to set
     */
    public void setOrders(Map<String, Integer> orders) {
        this.orders = orders;
    }

    /**
     * @param sku the sku to add
     * @param order the order to add
     */
    public void addOrder(String sku, Integer order) {
        if (this.orders == null) {
            this.orders = new HashMap<String, Integer>();
        }
        this.orders.put(sku,  order);
    }
    
    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "Closet [uuid=" + this.uuid + ", skus=" + this.skus + ", orders=" + this.orders + "]";
    }
}
