package com.gwynniebee.simulation.trace.object;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/** trace of rid data
*
* @author dj
*
*/
public class RIDTrace {

    // key = sequence ID, value = map of rid data
    private Map<Integer, Map<String, RID> > ridTraceData = null;

    /**
    *
    */
    public RIDTrace() {
    
    }

    /**
     * @return the ridTraceData
     */
    public Map<Integer, Map<String, RID> > getTrace() {
        return this.ridTraceData; 
    }

    /**
     * @param ridTraceData the ridTraceData to set
     */
    public void setTrace(Map<Integer, Map<String, RID> > ridTraceData) {
        this.ridTraceData = ridTraceData; 
    }
    
    /**
     * @param sequenceID sequenceID
     * @return the rid data
     */
    public Map<String, RID> getSequence(int sequenceID) {
        Map<String, RID> ridData = null;
        if (this.ridTraceData != null) {
            ridData = this.ridTraceData.get(sequenceID);
        }
        return ridData;
    }

    /**
     * @param sequenceID the sequenceID to set
     * @param ridData the ridData to set
     */
    public void setSequence(int sequenceID, Map<String, RID> ridData) {
        if (this.ridTraceData == null) {
            this.ridTraceData = new HashMap<Integer, Map<String, RID> >();
        }
        this.ridTraceData.put(sequenceID, ridData);
    }
    
    /**
     * @return the set of sequence ids in the trace
     */
    public Set<Integer> getSequenceIDs() {
        Set<Integer> sequenceIDs = null;
        if (this.ridTraceData != null) {
            sequenceIDs = this.ridTraceData.keySet();
        }
        return sequenceIDs;
    }
}
