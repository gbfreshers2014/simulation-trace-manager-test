/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.simulation.trace.object;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.Map;
import java.util.HashMap;
import java.util.Arrays;
import java.util.Comparator;

/** Allocation result that has garments (style-size) with multiple colors for one customer.
 *
 * @author dj
 *
 */
public class CustomerAllocationGarmentMultiColors {
    private String uuid = null;
    private int rid = 0;
    private String style = null;
    private String size = null;
    private Set<String> multiColorSKUs = null;
    private Map<String, Integer> multiColorSKUOrderIds = null;
    private Map<String, Integer> multiColorSKUsAvailable = null;

    /**
     *
     */
    public CustomerAllocationGarmentMultiColors() {
        super();
    }

    /**
     * @param uuid uuid
     * @param rid rid 
     * @param style style
     * @param size size
     * @param skus skus
     */
    public CustomerAllocationGarmentMultiColors(String uuid, int rid, String style, String size, 
                                                Map<String, Integer> orderIds, 
                                                Map<String, Integer> skusAvailable,
                                                String ... skus) {
        super();
        this.uuid = uuid;
        this.rid = rid;
        this.style = style;
        this.size = size;
        this.multiColorSKUOrderIds = orderIds;
        this.multiColorSKUsAvailable = skusAvailable;
        if (this.multiColorSKUs == null) {
            this.multiColorSKUs = new HashSet<String>();
        }
        for (String sku : multiColorSKUs) {
            this.multiColorSKUs.add(sku);
        }
    }
    
    /**
     * @param uuid uuid
     * @param rid rid
     * @param style style
     * @param size size
     * @param skus skus
     */
    public CustomerAllocationGarmentMultiColors(String uuid, int rid, String style, String size, 
                                                Map<String, Integer> orderIds, 
                                                Map<String, Integer> skusAvailable,
                                                Set<String> skus) {
        super();
        this.uuid = uuid;
        this.rid = rid;
        this.style = style;
        this.size = size;
        this.multiColorSKUOrderIds = orderIds;
        this.multiColorSKUsAvailable = skusAvailable;
        this.multiColorSKUs = skus;
    }

    /**
     * @return the uuid
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * @param uuid the uuid to set
     */
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
    
    /**
     * @return the rid 
     */
    public int getRid() {
        return rid;
    }
    
    /**
     * @param rid the rid to set
     */
    public void setRid(int rid) {
        this.rid = rid;
    }
    
    /**
     * @return the style 
     */
    public String getStyle() {
        return style;
    }
    
    /**
     * @param style the style to set
     */
    public void setStyle(String style) {
        this.style = style;
    }
 
    /**
     * @return the size
     */
    public String getSize() {
        return size;
    }
   
    /**
     * @param size the size to set
     */
    public void setSize(String size) {
        this.size = size;
    }

    /**
     * @return the multiColorSKUOrderIds 
     */
    public Map<String, Integer> getMultiColorSKUOrderIds() {
        return multiColorSKUOrderIds;
    }
    
    /**
     * @param multiColorSKUOrderIds the multiColorSKUOrderIds to set
     */
    public void setMultiColorSKUOrderIds(Map<String, Integer> multiColorSKUOrderIds) {
        this.multiColorSKUOrderIds = multiColorSKUOrderIds;
    }

    /**
     * @param {sku, orderId} the {sku, orderId} to add
     */
    public void addMultiColorSKUOrderId(String sku, int orderId) {
        if (this.multiColorSKUOrderIds == null) {
            this.multiColorSKUOrderIds = new HashMap<String, Integer>();
        }
        this.multiColorSKUOrderIds.put(sku, orderId);
    }

    /**
     * @return the multiColorSKUsAvailable 
     */
    public Map<String, Integer> getMultiColorSKUsAvailable() {
        return multiColorSKUsAvailable;
    }
    
    /**
     * @param multiColorSKUsAvailable the multiColorSKUsAvailable to set
     */
    public void setMultiColorSKUsAvailable(Map<String, Integer> multiColorSKUsAvailable) {
        this.multiColorSKUsAvailable = multiColorSKUsAvailable;
    }

    /**
     * @param {sku, availableCount} the {sku, availableCount} to add
     */
    public void addMultiColorSKUsAvailable(String sku, int availableCount) {
        if (this.multiColorSKUsAvailable == null) {
            this.multiColorSKUsAvailable = new HashMap<String, Integer>();
        }
        this.multiColorSKUsAvailable.put(sku, availableCount);
    }

    /**
     * @return the multiColorSKUs 
     */
    public Set<String> getMultiColorSKUs() {
        return multiColorSKUs;
    }
    
    /**
     * @param multiColorSKUs the multiColorSKUs to set
     */
    public void setMultiColorSKUs(Set<String> multiColorSKUs) {
        this.multiColorSKUs = multiColorSKUs;
    }

    /**
     * @param sku the sku to add
     */
    public void addMultiColorSKU(String sku) {
        if (this.multiColorSKUs == null) {
            this.multiColorSKUs = new HashSet<String>();
        }
        this.multiColorSKUs.add(sku);
    }
    
    /**
     * sort multiColorSKUs in ascending order of order ids and descending order of availability and keep
     * the skus that are to be removed from allocation
     */
    public void sortSKUs() {
        String[] skus = this.multiColorSKUs.toArray(new String[this.multiColorSKUs.size()]);
        Arrays.sort(skus, new Comparator<String>() {
            public int compare(String s1, String s2) {
                int s1OrderId = CustomerAllocationGarmentMultiColors.this.getMultiColorSKUOrderIds().get(s1);
                int s1Available = CustomerAllocationGarmentMultiColors.this.getMultiColorSKUsAvailable().get(s1);
                int s2OrderId = CustomerAllocationGarmentMultiColors.this.getMultiColorSKUOrderIds().get(s2);
                int s2Available = CustomerAllocationGarmentMultiColors.this.getMultiColorSKUsAvailable().get(s2);
                int c = s1OrderId - s2OrderId;  // ascending order of order ids
                if (c == 0) {
                    c = s2Available - s1Available;  // descending order of availability
                }
                return c;
            }
        });

        String firstSku = skus[0];
        // other than firstSku, the rest of the skus in multiColorSKUs will be used to be removed from allocation
        for (Iterator<String> i = this.multiColorSKUs.iterator(); i.hasNext();) {
            String sku = i.next();
            if (firstSku.equals(sku)) {
                i.remove();
            }
        }
    }
    
}
