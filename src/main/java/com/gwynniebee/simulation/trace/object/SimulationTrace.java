/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.simulation.trace.object;

/** Simulation trace that includes all data and traces for simulation
 *
 * @author dj
 *
 */
public class SimulationTrace {
    private int sequenceID = 0;
    private int maxSimulationRounds = 0;
    private ClosetTrace clostTrace = null;
    private SKUAvailabilityTrace skuAvailabilityTrace = null;
    private RIDTrace ridTrace = null;
    private UserPlan userPlan = null;
    private SKUReturnPlan skuReturnPlan = null;
    private CustomerReturnData customerReturnData = null;

    /**
     *
     */
    public void setSequenceID(int sequenceID) {
        this.sequenceID = sequenceID;
    }
    
    public void setMaxSimulationRounds(int maxSimulationRounds) {
        this.maxSimulationRounds = maxSimulationRounds;
    }
    
    public void load(int sequenceID) {
        // 1. load closet trace from sequenceID
        // 2. load sku availability trace from sequenceID
        // 3. load rid trace from sequenceID
        // 4. load user plan
        // 5. load sku return plan 
        // 6. if sequenceID == 1 then load customer return data
        // 7. load allocation plan from sequenceID-1
        
    }
    
    public void update(int sequenceID) {
        // 1. if there exists allocation plan sequenceID-1 then do the following update
        // 2. remove skus from fully allocated rids from closets trace
        // 3. deduce count of sku availability from fully allocated rids
        // 4. remove fully allocated rids
        // 5. create new rids by checking sku return plan?
    }
    
    public void store(int sequenceID) {
        // store trace and data files from sequenceID onwards
        
    }
    
}
