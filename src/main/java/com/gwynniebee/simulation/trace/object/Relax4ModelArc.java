/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.simulation.trace.object;

import java.util.HashSet;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.Collections;

import com.gwynniebee.matching.setup.RIDAllocationRequest;
import com.gwynniebee.matching.result.RIDAllocation;
import com.gwynniebee.simulation.trace.utils.RIDAllocationRequestsSortByHoursInQueue;
import com.gwynniebee.simulation.trace.utils.RIDAllocationRequestsSortBySlots;

/** Allocation result for one customer.
 *
 * @author dj
 *
 */
public class Relax4ModelArc {
    private int x;
    private int y;
    private int cost;
    private int capacity;
    
    /**
     *
     */
    public Relax4ModelArc() {
        super();
    }

    public Relax4ModelArc(int x, int y, int cost, int capacity) {
        super();
        this.x = x;
        this.y = y;
        this.cost = cost;
        this.capacity = capacity;
    }
    
    /**
     * @return the x
     */
    public int getX() {
        return this.x;
    }

    /**
     * @param x the x to set
     */
    public void setX(int x) {
        this.x = x;
    }
    
    /**
     * @return the y
     */
    public int getY() {
        return this.y;
    }

    /**
     * @param y the y to set
     */
    public void setY(int y) {
        this.y = y;
    }
    
    /**
     * @return the cost
     */
    public int getCost() {
        return this.cost;
    }

    /**
     * @param cost the cost to set
     */
    public void setCost(int cost) {
        this.cost = cost;
    }

    /**
     * @return the capacity
     */
    public int getCapacity() {
        return this.capacity;
    }

    /**
     * @param capacity the capacity to set
     */
    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

}

