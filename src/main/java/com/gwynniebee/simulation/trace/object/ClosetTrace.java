package com.gwynniebee.simulation.trace.object;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/** trace of closet data
*
* @author dj
*
*/
public class ClosetTrace {

    // key = sequence ID, value = map of closet data
    private Map<Integer, Map<String, Closet> > closetTraceData = null;

    /**
    *
    */
    public ClosetTrace() {
    
    }

    /**
     * @return the closetTraceData
     */
    public Map<Integer, Map<String, Closet> > getTrace() {
        return this.closetTraceData; 
    }

    /**
     * @param closetTraceData the closetTraceData to set
     */
    public void setTrace(Map<Integer, Map<String, Closet> > closetTraceData) {
        this.closetTraceData = closetTraceData; 
    }
    
    /**
     * @param sequenceID sequenceID
     * @return the closet data
     */
    public Map<String, Closet> getSequence(int sequenceID) {
        Map<String, Closet> closetData = null;
        if (this.closetTraceData != null) {
            closetData = this.closetTraceData.get(sequenceID);
        }
        return closetData;
    }

    /**
     * @param sequenceID the sequenceID to set
     * @param closetData the closetData to set
     */
    public void setSequence(int sequenceID, Map<String, Closet> closetData) {
        if (this.closetTraceData == null) {
            this.closetTraceData = new HashMap<Integer, Map<String, Closet> >();
        }
        this.closetTraceData.put(sequenceID, closetData);
    }
    
    /**
     * @return the set of sequence ids in the trace
     */
    public Set<Integer> getSequenceIDs() {
        Set<Integer> sequenceIDs = null;
        if (this.closetTraceData != null) {
            sequenceIDs = this.closetTraceData.keySet();
        }
        return sequenceIDs;
    }
}
