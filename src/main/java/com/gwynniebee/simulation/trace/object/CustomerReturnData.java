/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.simulation.trace.object;

import java.util.HashMap;
import java.util.Map;

/** Customer return data
 *
 * @author dj
 *
 */
public class CustomerReturnData {
    public static final String UUID = "uuid";
    public static final String SKU = "sku";
    public static final String RETURN = "returnAtSequenceID";
    
    private String uuid;
    private Map<String, Integer> skusToBeReturned = null;    // at which sequence of simulation will the uuid return the sku

    /**
     *
     */
    public CustomerReturnData() {
        super();
    }

    /**
     * @param uuid uuid
     * @param skusToBeReturned
     */
    public CustomerReturnData(String uuid, Map<String, Integer> skusToBeReturned) {
        super();
        this.uuid = uuid;
        this.skusToBeReturned = skusToBeReturned;
    }

    /**
     * @return the uuid
     */
    public String getUuid() {
        return this.uuid;
    }

    /**
     * @param uuid the uuid to set
     */
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    /**
     * @return the skusToBeReturned
     */
    public Map<String, Integer> getSkusToBeReturned() {
        return this.skusToBeReturned;
    }

    /**
     * @param skusToBeReturned the skusToBeReturned to set
     */
    public void setSkusToBeReturned(Map<String, Integer> skusToBeReturned ) {
        this.skusToBeReturned = skusToBeReturned;
    }

    /**
     * @param sku the sku to add
     * @param sequenceIDToBeReturned the sequenceIDToBeReturned to add
     */
    public void addSkusToBeReturned(String sku, Integer sequenceIDToBeReturned) {
        if (this.skusToBeReturned == null) {
            this.skusToBeReturned = new HashMap<String, Integer>();
        }
        this.skusToBeReturned.put(sku,  sequenceIDToBeReturned);
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "SkuReturnPlan [uuid=" + this.uuid + ", skusToBeReturned=" + this.skusToBeReturned + "]";
    }
}
