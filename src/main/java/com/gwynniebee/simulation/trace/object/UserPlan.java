/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.simulation.trace.object;

/** User subscription plan
 *
 * @author dj
 *
 */
public class UserPlan {
    public static final String UUID = "uuid";
    public static final String SLOTS = "countSlots";
    
    private String uuid = null;
    private int slots = 0;

    /**
     *
     */
    public UserPlan() {
        super();
    }

    /**
     * 
     * @param uuid
     * @param slots
     */
    public UserPlan(String uuid, int slots) {
        super();
        this.uuid = uuid;
        this.slots = slots;
    }

    /**
     * @return the uuid
     */
    public String getUuid() {
        return this.uuid;
    }

    /**
     * @param uuid the uuid to set
     */
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    /**
     * @return the slots
     */
    public int getSlots() {
        return slots;
    }

    /**
     * @param slots the slots to set
     */
    public void setSlots(int slots) {
        this.slots = slots;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "UserPlan [uuid=" + this.uuid + ", slots=" + this.slots + "]";
    }
}
