/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.simulation.trace.object;

import java.util.TreeMap;
import java.util.Map;

/** Cost matrix representation
 *
 * @author dj
 *
 */
public class Relax4ModelMatrix {
    private int defaultValue = 0;
    private int m;
    private int n;        
    private Map<Integer, Integer> data = new TreeMap<Integer, Integer>();
    
    /**
     * create a new matrix with m rows and n columns
     * @param m row
     * @param n column
     * @param defaultValue defaultValue
     */    
    public Relax4ModelMatrix(int m, int n, int defaultValue) {
        super();
        this.m = m;
        this.n = n;
        this.defaultValue = defaultValue;
    }
    
    /**
     * get value at [i,j]
     * @param i
     * @param j
     * @return value at [i,j]
     */
    public int getValueAt(int i, int j) {
        if (i >= m || j >= n || i < 0 || j < 0) 
            throw new IllegalArgumentException("index (" + i + ", " +j +") out of bounds");
        Integer value = data.get(i * n + j);
        return value != null ? value : defaultValue;
    }

    /**
     * set value at [i,j]
     * @param i
     * @param j
     * @param value
     */
    public void setValueAt(int i, int j, int value) {
        if (i >= m || j >= n || i < 0 || j < 0) 
            throw new IllegalArgumentException("index (" + i + ", " +j +") out of bounds");        
        data.put(i * n + j, value);
    }
    
}

