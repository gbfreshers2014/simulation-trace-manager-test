package com.gwynniebee.simulation.trace.object;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/** trace of closet data
*
* @author dj
*
*/
public class SimulationMetrics {

    private int totalAvailableSnowflakes = 0;
    private int unallocatedSnowflakes = 0;
    private int totalAvailableSKUs = 0;
    private int unallocatedSKUs = 0;
    private int totalRids = 0;
    private int unallocatedRids = 0;
    private int totalClosets = 0;
    private int belowMinClosets = 0;
    // map for <roundsInQueue, countCustomers>
    private Map<Integer, Integer> customerInQueueCounts = null;


    /**
    *
    */
    public SimulationMetrics() {
    
    }

    /**
     * 
     * @return totalAvailableSnowflakes
     */
    public int getTotalAvailableSnowflakes() {
        return this.totalAvailableSnowflakes;
    }
    
    /**
     * 
     * @param totalAvailableSnowflakes
     */
    public void setTotalAvailableSnowflakes(int totalAvailableSnowflakes) {
        this.totalAvailableSnowflakes = totalAvailableSnowflakes;
    }
    
    /**
     * 
     * @param totalAvailableSnowflakes
     */
    public void addTotalAvailableSnowflakes(int totalAvailableSnowflakes) {
        this.totalAvailableSnowflakes += totalAvailableSnowflakes;
    }

    /**
     * 
     * @return unallocatedSnowflakes
     */
    public int getUnallocatedSnowflakes() {
        return this.unallocatedSnowflakes;
    }
    
    /**
     * 
     * @param unallocatedSnowflakes
     */
    public void setUnallocatedSnowflakes(int unallocatedSnowflakes) {
        this.unallocatedSnowflakes = unallocatedSnowflakes;
    }
    
    /**
     * 
     * @param unallocatedSnowflakes
     */
    public void addUnallocatedSnowflakes(int unallocatedSnowflakes) {
        this.unallocatedSnowflakes += unallocatedSnowflakes;
    }

    /**
     * 
     * @return totalAvailableSKUs
     */
    public int getTotalAvailableSKUs() {
        return this.totalAvailableSKUs;
    }
    
    /**
     * 
     * @param totalAvailableSKUs
     */
    public void setTotalAvailableSKUs(int totalAvailableSKUs) {
        this.totalAvailableSKUs = totalAvailableSKUs;
    }
    
    /**
     * 
     * @param totalAvailableSKUs
     */
    public void addTotalAvailableSKUs(int totalAvailableSKUs) {
        this.totalAvailableSKUs += totalAvailableSKUs;
    }

    /**
     * 
     * @return unallocatedSKUs
     */
    public int getUnallocatedSKUs() {
        return this.unallocatedSKUs;
    }
    
    /**
     * 
     * @param unallocatedSKUs
     */
    public void setUnallocatedSKUs(int unallocatedSKUs) {
        this.unallocatedSKUs = unallocatedSKUs;
    }
    
    /**
     * 
     * @param unallocatedSKUs
     */
    public void addUnallocatedSKUs(int unallocatedSKUs) {
        this.unallocatedSKUs += unallocatedSKUs;
    }
    
    /**
     * 
     * @return totalRids
     */
    public int getTotalRids() {
        return this.totalRids;
    }
    
    /**
     * 
     * @param totalRids
     */
    public void setTotalRids(int totalRids) {
        this.totalRids = totalRids;
    }
    
    /**
     * 
     * @param totalRids
     */
    public void addTotalRids(int totalRids) {
        this.totalRids += totalRids;
    }

    /**
     * 
     * @return unallocatedRids
     */
    public int getUnallocatedRids() {
        return this.unallocatedRids;
    }
    
    /**
     * 
     * @param unallocatedRids
     */
    public void setUnallocatedRids(int unallocatedRids) {
        this.unallocatedRids = unallocatedRids;
    }
    
    /**
     * 
     * @param unallocatedRids
     */
    public void addUnallocatedRids(int unallocatedRids) {
        this.unallocatedRids += unallocatedRids;
    }
    
    /**
     * 
     * @return totalClosets
     */
    public int getTotalClosets() {
        return this.totalClosets;
    }
    
    public void setTotalClosets(int totalClosets) {
        this.totalClosets = totalClosets;
    }
    
    /**
     * 
     * @param totalClosets
     */
    public void addTotalClosets(int totalClosets) {
        this.totalClosets += totalClosets;
    }

    /**
     * 
     * @return belowMinClosets
     */
    public int getBelowMinClosets() {
        return this.belowMinClosets;
    }
    
    /**
     * 
     * @param belowMinClosets
     */
    public void setBelowMinClosets(int belowMinClosets) {
        this.belowMinClosets = belowMinClosets;
    }
    
    /**
     * 
     * @param belowMinClosets
     */
    public void addBelowMinClosets(int belowMinClosets) {
        this.belowMinClosets += belowMinClosets;
    }

    /**
     * 
     * @return customerInQueueCounts
     */
    public Map<Integer, Integer> getCustomerInQueueCounts() {
        return this.customerInQueueCounts;
    }
    
    /**
     * 
     * @param customerInQueueCounts
     */
    public void setCustomerInQueueCounts(Map<Integer, Integer> customerInQueueCounts) {
        this.customerInQueueCounts = customerInQueueCounts;
    }
    
    /**
     * 
     * @param roundsInQueue
     * @param countCustomers
     */
    public void addCustomerInQueueCounts(int roundsInQueue, int countCustomers) {
        if (this.customerInQueueCounts == null) {
            this.customerInQueueCounts = new HashMap<Integer, Integer>();
            this.customerInQueueCounts.put(roundsInQueue, countCustomers);
        }
        else {
            if (this.customerInQueueCounts.containsKey(roundsInQueue)) {
                this.customerInQueueCounts.put(roundsInQueue, this.customerInQueueCounts.get(roundsInQueue) + countCustomers);
                
            }
            else {
                this.customerInQueueCounts.put(roundsInQueue, countCustomers);
            }
        }
    }
}
