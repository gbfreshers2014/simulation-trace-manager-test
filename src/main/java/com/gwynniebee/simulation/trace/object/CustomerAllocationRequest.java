/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.simulation.trace.object;

import java.util.List;
import java.util.ArrayList;

import com.gwynniebee.matching.setup.RIDAllocationRequest;

/** Allocation request for one customer by merging RID allocation requests from the same customer
 *
 * @author dj
 *
 */
public class CustomerAllocationRequest {
    private String uuid = null;
    private int slots = 0;
    private int hoursInQueue = -1;
    private List<RIDAllocationRequest> ridAllocationRequests = null;

    /**
     *
     */
    public CustomerAllocationRequest() {
        super();
    }

    /**
     * @param uuid uuid
     * @param slots slots
     * @param hoursInQueue hoursInQueue
     * @param ridAllocationRequests ridAllocationRequests
     */
    public CustomerAllocationRequest(String uuid, int slots, int hoursInQueue,
                                     List<RIDAllocationRequest> ridAllocationRequests) {
        super();
        this.uuid = uuid;
        this.slots = slots;
        this.hoursInQueue = hoursInQueue;
        if (ridAllocationRequests == null) {
            this.ridAllocationRequests = new ArrayList<RIDAllocationRequest>();
        }
        else {
            this.ridAllocationRequests = ridAllocationRequests;
        }
    }

    /**
     * @param ridAllocationRequest ridAllocationRequest
     */
    public boolean addRIDAllocationRequest(RIDAllocationRequest ridAllocationRequest) {
        if (ridAllocationRequest == null) {
            return false;
        }
        
        if (this.uuid == null) {    // first rid to add
            this.uuid = ridAllocationRequest.getUuid();
            this.slots = ridAllocationRequest.getSlots();
            this.hoursInQueue = ridAllocationRequest.getHoursInQueue();
            if (ridAllocationRequests == null) {
                this.ridAllocationRequests = new ArrayList<RIDAllocationRequest>();
            }
            this.ridAllocationRequests.add(ridAllocationRequest);
            return true;
        }
        else {
            if (this.uuid != null && this.uuid.equals(ridAllocationRequest.getUuid())) {
                // check if the rid has already been added
                boolean exists = false;
                for (RIDAllocationRequest req : this.ridAllocationRequests) {
                    if (req.getRid() == ridAllocationRequest.getRid()) {
                        // skip if rid already exists                   
                        exists = true;
                        break;
                    }
                }
                if (!exists) {  // add a new rid
                    this.slots += slots;
                    this.hoursInQueue = this.hoursInQueue < ridAllocationRequest.getHoursInQueue() ?
                                            ridAllocationRequest.getHoursInQueue() : this.hoursInQueue;
                    this.ridAllocationRequests.add(ridAllocationRequest);
                    return true;
                }
                else {  // skip
                    return false;
                }
            }
            else {
                return false;
            }
        }
    }

    /**
     * @return the uuid
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * @param uuid the uuid to set
     */
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    /**
     * @return the slots
     */
    public int getSlots() {
        return slots;
    }

    /**
     * @param slots the slots to set
     */
    public void setSlots(int slots) {
        this.slots = slots;
    }

    /**
     * @return the hoursInQueue
     */
    public int getHoursInQueue() {
        return hoursInQueue;
    }

    /**
     * @param hoursInQueue the hoursInQueue to set
     */
    public void setHoursInQueue(int hoursInQueue) {
        this.hoursInQueue = hoursInQueue;
    }

    /**
     * @return the ridAllocationRequests
     */
    public List<RIDAllocationRequest> getRIDAllocationRequests() {
        return ridAllocationRequests;
    }

    /**
     * @param ridAllocationRequests the ridAllocationRequests to set
     */
    public void setRIDAllocationRequests(List<RIDAllocationRequest> ridAllocationRequests) {
        this.ridAllocationRequests = ridAllocationRequests;
    }
    
}
