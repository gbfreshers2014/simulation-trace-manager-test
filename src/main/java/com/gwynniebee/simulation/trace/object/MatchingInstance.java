/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.simulation.trace.object;

import com.gwynniebee.matching.setup.MatchingSetup;
import com.gwynniebee.matching.result.MatchingResult;

/** 
 *
 * @author dj
 *
 */
public class MatchingInstance {
    private MatchingSetup matchingSetup = null; 
    private MatchingResult matchingResult = null;

    /**
     *
     */
    public MatchingInstance() {
        super();
    }

    /**
     * @param matchingSetup matchingSetup
     * @param matchingResult matchingResult 
     */
    public MatchingInstance(MatchingSetup matchingSetup, MatchingResult matchingResult) {
        super();
        this.matchingSetup = matchingSetup;
        this.matchingResult = matchingResult;
    }
   
    /**
     * @param matchingSetup matchingSetup
     */
    public MatchingInstance(MatchingSetup matchingSetup) {
        super();
        this.matchingSetup = matchingSetup;
    }

    /**
     * @param matchingResult matchingResult
     */
    public MatchingInstance(MatchingResult matchingResult) {
        super();
        this.matchingResult = matchingResult;
    }
 
    /**
     * @return the matchingSetup 
     */
    public MatchingSetup getMatchingSetup() {
        return this.matchingSetup;
    }
    
    /**
     * @param matchingSetup the matchingSetup to set
     */
    public void setMatchingSetup(MatchingSetup matchingSetup) {
        this.matchingSetup = matchingSetup;
    }
   
    /**
     * @return the matchingResult
     */
    public MatchingResult getMatchingResult() {
        return this.matchingResult;
    }
   
    /**
     * @param matchingResult the matchingResult to set
     */
    public void setMatchingResult(MatchingResult matchingResult) {
        this.matchingResult = matchingResult;
    }
}
