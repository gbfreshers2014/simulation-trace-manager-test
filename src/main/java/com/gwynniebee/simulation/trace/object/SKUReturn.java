/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.simulation.trace.object;

import java.util.Map;
import java.util.HashMap;

/** User subscription plan
 *
 * @author dj
 *
 */
public class SKUReturn {
    public static final String UUID = "uuid";
    public static final String SKU = "sku";
    public static final String STATE = "state";
    
    private String uuid = null;
    private Map<String, String> skuStates = null;

    /**
     *
     */
    public SKUReturn() {
        super();
    }

    /**
     * 
     * @param uuid
     * @param skus
     */
    public SKUReturn(String uuid, Map<String, String> skuStates) {
        super();
        this.uuid = uuid;
        this.skuStates = skuStates;
    }

    /**
     * @return the uuid
     */
    public String getUuid() {
        return this.uuid;
    }

    /**
     * @param uuid the uuid to set
     */
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    /**
     * @return the skuState
     */
    public Map<String, String> getSkuStates() {
        return skuStates;
    }

    /**
     * @param skus the skus to set
     */
    public void setSkuStates(Map<String, String> skuStates) {
        this.skuStates = skuStates;
    }

    public void addSkuState(String sku, String state) {
        if (skuStates == null) {
            skuStates = new HashMap<String, String>();
        }
        skuStates.put(sku, state);
    }
    
    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "SKUReturn [uuid=" + this.uuid + ", skuStates=" + this.skuStates + "]";
    }
}
