/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.simulation.trace.object;

/** RID data
 *
 * @author dj
 *
 */
public class RID {    
    public static final String UUID = "uuid";
    public static final String RID = "rid";
    public static final String SLOTS = "countPendingSlots";
    public static final String INQUEUE = "roundsInQueue";    

    private String uuid;
    private int rid;
    private int slots = 0;
    private int roundsInQueue = 0;

    /**
     *
     */
    public RID() {
        super();
    }

    /**
     * @param rid rid
     * @param uuid uuid
     */
    public RID(int rid, String uuid) {
        super();
        this.rid = rid;
        this.uuid = uuid;
    }

    /**
     * @param rid rid
     * @param uuid uuid
     * @param slots slots
     * @param roundsInQueue roundsInQueue
     */
    public RID(int rid, String uuid, int slots, int roundsInQueue) {
        super();
        this.rid = rid;
        this.uuid = uuid;
        this.slots = slots;
        this.roundsInQueue = roundsInQueue;
    }

    /**
     * @return the rid
     */
    public int getRid() {
        return rid;
    }

    /**
     * @param rid the rid to set
     */
    public void setRid(int rid) {
        this.rid = rid;
    }

    /**
     * @return the uuid
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * @param uuid the uuid to set
     */
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    /**
     * @return the slots
     */
    public int getSlots() {
        return slots;
    }

    /**
     * @param slots the slots to set
     */
    public void setSlots(int slots) {
        this.slots = slots;
    }

    /**
     * @return the roundsInQueue
     */
    public int getRoundsInQueue() {
        return roundsInQueue;
    }

    /**
     * @param roundsInQueue the roundsInQueue to set
     */
    public void setRoundsInQueue(int roundsInQueue) {
        this.roundsInQueue = roundsInQueue;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "RID [rid=" + this.rid + ", uuid=" + this.uuid + ", slots=" + this.slots + 
                ", roundsInQueue=" + this.roundsInQueue + "]";
    }
}
