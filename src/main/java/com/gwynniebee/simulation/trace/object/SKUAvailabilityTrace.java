package com.gwynniebee.simulation.trace.object;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/** trace of sku availability data
*
* @author dj
*
*/
public class SKUAvailabilityTrace {

    // key = sequence ID, value = map of sku availability data
    private Map<Integer, Map<String, SKUAvailability> > skuAvailabilityTraceData = null;

    /**
    *
    */
    public SKUAvailabilityTrace() {
    
    }

    /**
     * @return the skuAvailabilityTraceData
     */
    public Map<Integer, Map<String, SKUAvailability> > getTrace() {
        return this.skuAvailabilityTraceData; 
    }

    /**
     * @param skuAvailabilityTraceData the skuAvailabilityTraceData to set
     */
    public void setTrace(Map<Integer, Map<String, SKUAvailability> > skuAvailabilityTraceData) {
        this.skuAvailabilityTraceData = skuAvailabilityTraceData; 
    }
    
    /**
     * @param sequenceID sequenceID
     * @return the sku availability data
     */
    public Map<String, SKUAvailability> getSequence(int sequenceID) {
        Map<String, SKUAvailability> skuAvailabilityData = null;
        if (this.skuAvailabilityTraceData != null) {
            skuAvailabilityData = this.skuAvailabilityTraceData.get(sequenceID);
        }
        return skuAvailabilityData;
    }

    /**
     * @param sequenceID the sequenceID to set
     * @param skuAvailabilityData the skuAvailabilityData to set
     */
    public void setSequence(int sequenceID, Map<String, SKUAvailability> skuAvailabilityData) {
        if (this.skuAvailabilityTraceData == null) {
            this.skuAvailabilityTraceData = new HashMap<Integer, Map<String, SKUAvailability> >();
        }
        this.skuAvailabilityTraceData.put(sequenceID, skuAvailabilityData);
    }
    
    /**
     * @return the set of sequence ids in the trace
     */
    public Set<Integer> getSequenceIDs() {
        Set<Integer> sequenceIDs = null;
        if (this.skuAvailabilityTraceData != null) {
            sequenceIDs = this.skuAvailabilityTraceData.keySet();
        }
        return sequenceIDs;
    }
}
