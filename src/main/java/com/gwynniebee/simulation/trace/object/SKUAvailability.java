/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.simulation.trace.object;

/** sku availability data
 *
 * @author dj
 *
 */
public class SKUAvailability {
    public static final String SKU = "sku";
    public static final String ATGB = "countAtGB";
    public static final String ATCUSTOMER = "countAtCustomer";
    public static final String ATLAUNDRY = "countAtLaundry";

    private String sku;
    private int atCustomer = 0;
    private int atGwynnieBee = 0;
    private int atLaundry = 0;

    /**
     *
     */
    public SKUAvailability() {
        super();
    }

    /**
     * @param sku sku
     * @param atCustomer atCustomer
     * @param atGwynnieBee atGwynnieBee
     * @param atLaudary atLaudary
     */
    public SKUAvailability(String sku, int atCustomer, int atGwynnieBee, int atLaundry) {
        super();
        this.sku = sku;
        this.atCustomer = atCustomer;
        this.atGwynnieBee = atGwynnieBee;
        this.atLaundry = atLaundry;
    }

    /**
     * @return the sku
     */
    public String getSku() {
        return sku;
    }

    /**
     * @param sku the sku to set
     */
    public void setSku(String sku) {
        this.sku = sku;
    }

    /**
     * @return the atCustomer
     */
    public int getAtCustomer() {
        return atCustomer;
    }

    /**
     * @param atCustomer the atCustomer to set
     */
    public void setAtCustomer(int atCustomer) {
        this.atCustomer = atCustomer;
    }

    /**
     * @return the atGwynnieBee
     */
    public int getAtGwynnieBee() {
        return atGwynnieBee;
    }

    /**
     * @param atGwynnieBee the atGwynnieBee to set
     */
    public void setAtGwynnieBee(int atGwynnieBee) {
        this.atGwynnieBee = atGwynnieBee;
    }

    /**
     * @return the atLaudary
     */
    public int getAtLaundry() {
        return atLaundry;
    }

    /**
     * @param atLaudary the atLaudary to set
     */
    public void setAtLaundry(int atLaundry) {
        this.atLaundry = atLaundry;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "SKUAvailability [sku=" + this.sku + ", atCustomer=" + this.atCustomer + 
                ", atGwynnieBee=" + this.atGwynnieBee + ", atLaundry=" + this.atLaundry + "]";
    }
}
