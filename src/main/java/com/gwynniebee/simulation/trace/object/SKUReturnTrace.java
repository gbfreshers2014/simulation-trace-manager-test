package com.gwynniebee.simulation.trace.object;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/** trace of sku availability data
*
* @author dj
*
*/
public class SKUReturnTrace {

    // key = sequence ID, value = map of sku return data
    private Map<Integer, Map<String, SKUReturn> > skuReturnTraceData = null;

    /**
    *
    */
    public SKUReturnTrace() {
    
    }

    /**
     * @return the skuReturnTraceData
     */
    public Map<Integer, Map<String, SKUReturn> > getTrace() {
        return this.skuReturnTraceData; 
    }

    /**
     * @param skuReturnTraceData the skuReturnTraceData to set
     */
    public void setTrace(Map<Integer, Map<String, SKUReturn> > skuReturnTraceData) {
        this.skuReturnTraceData = skuReturnTraceData; 
    }
    
    /**
     * @param sequenceID sequenceID
     * @return the sku return data
     */
    public Map<String, SKUReturn> getSequence(int sequenceID) {
        Map<String, SKUReturn> skuReturnData = null;
        if (this.skuReturnTraceData != null) {
            skuReturnData = this.skuReturnTraceData.get(sequenceID);
        }
        return skuReturnData;
    }

    /**
     * @param sequenceID the sequenceID to set
     * @param skuReturnData the skuReturnData to set
     */
    public void setSequence(int sequenceID, Map<String, SKUReturn> skuReturnData) {
        if (this.skuReturnTraceData == null) {
            this.skuReturnTraceData = new HashMap<Integer, Map<String, SKUReturn> >();
        }
        this.skuReturnTraceData.put(sequenceID, skuReturnData);
    }
    
    /**
     * @return the set of sequence ids in the trace
     */
    public Set<Integer> getSequenceIDs() {
        Set<Integer> sequenceIDs = null;
        if (this.skuReturnTraceData != null) {
            sequenceIDs = this.skuReturnTraceData.keySet();
        }
        return sequenceIDs;
    }
}
