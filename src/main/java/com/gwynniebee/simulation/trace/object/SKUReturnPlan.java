/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.simulation.trace.object;

/** SKU return plan
 *
 * @author dj
 *
 */
public class SKUReturnPlan {
    public static final String SKU = "sku";
    public static final String RETURN = "countCyclesReturn";
    public static final String LAUNDRY = "countCyclesLaundry";

    private String sku = null;
    private int roundsAtCustomer = 0;   // number of rounds of simulation for the sku to stay at customer
    private int roundsAtLaundry = 0;    // number of rounds of simulation for the sku to stay at laundry

    /**
     *
     */
    public SKUReturnPlan() {
        super();
    }

    /**
     * @param sku sku
     * @param roundsAtCustomer roundsAtCustomer
     * @param roundsAtLaundry roundsAtLaundry
     */
    public SKUReturnPlan(String sku, int roundsAtCustomer, int roundsAtLaundry) {
        super();
        this.sku = sku;
        this.roundsAtCustomer = roundsAtCustomer;
        this.roundsAtLaundry = roundsAtLaundry;
    }

    /**
     * @return the sku
     */
    public String getSku() {
        return this.sku;
    }

    /**
     * @param sku the sku to set
     */
    public void setSku(String sku) {
        this.sku = sku;
    }

    /**
     * @return the roundsAtCustomer
     */
    public int getRoundsAtCustomer() {
        return roundsAtCustomer;
    }

    /**
     * @param roundsAtCustomer the roundsAtCustomer to set
     */
    public void setRoundsAtCustomer(int roundsAtCustomer) {
        this.roundsAtCustomer = roundsAtCustomer;
    }

    /**
     * @return the roundsAtLaundry
     */
    public int getRoundsAtLaundry() {
        return roundsAtLaundry;
    }

    /**
     * @param roundsAtLaundry the roundsAtLaundry to set
     */
    public void setRoundsAtLaundry(int roundsAtLaundry) {
        this.roundsAtLaundry = roundsAtLaundry;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "SkuReturnPlan [sku=" + this.sku + 
                ", roundsAtCustomer=" + this.roundsAtCustomer + 
                ", roundsAtLaundry=" + this.roundsAtLaundry + "]";
    }
}
