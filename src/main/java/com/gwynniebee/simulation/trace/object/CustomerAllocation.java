/**
 * Copyright 2012 GwynnieBee Inc.
 */
package com.gwynniebee.simulation.trace.object;

import java.util.HashSet;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.Collections;

import com.gwynniebee.matching.setup.RIDAllocationRequest;
import com.gwynniebee.matching.result.RIDAllocation;
import com.gwynniebee.simulation.trace.utils.RIDAllocationRequestsSortByHoursInQueue;
import com.gwynniebee.simulation.trace.utils.RIDAllocationRequestsSortBySlots;

/** Allocation result for one customer.
 *
 * @author dj
 *
 */
public class CustomerAllocation {
    private String uuid = null;
    private int slots = 0;
    private int fulfilledSlots = 0;
    private Set<String> allocatedSKUs = null;
    private List<RIDAllocationRequest> ridAllocationRequests = null;
    private List<RIDAllocation> ridAllocations = null;

    /**
     *
     */
    public CustomerAllocation() {
        super();
    }

    /**
     * @param uuid uuid
     * @param slots slots
     * @param skus skus
     */
    public CustomerAllocation(List<RIDAllocationRequest> ridAllocationRequests,
                                String uuid, int slots, String ... skus) {
        super();
        this.uuid = uuid;
        this.slots = slots;
        this.allocatedSKUs = new HashSet<String>();
        if (skus != null) {
            for (String sku : skus) {
                this.allocatedSKUs.add(sku);
            }
        }
        this.fulfilledSlots = this.allocatedSKUs.size();
        this.ridAllocationRequests = ridAllocationRequests;
        allocateRIDs(ridAllocationRequests, skus);
    }
    
    /**
     * @param ridAllocationRequests ridAllocationRequests
     * @param skus skus
     */
    protected void allocateRIDs(List<RIDAllocationRequest> ridAllocationRequests, String ... skus) {
        // allocate the skus in customer allocation to the rids in this customer
        List<RIDAllocation> ridAllocations = null;
        
        if (ridAllocationRequests != null && ridAllocationRequests.size() >= 1) {
            // 1. sort the rids based on hoursInQueue, top with highest hours in queue
            Collections.sort(ridAllocationRequests, Collections.reverseOrder(new RIDAllocationRequestsSortByHoursInQueue()));
            // 2. then sort the rids based on slots, top with the fewest slots
            Collections.sort(ridAllocationRequests, new RIDAllocationRequestsSortBySlots());
            
            // allocate skus to RIDs in the same customer
            ridAllocations = new ArrayList<RIDAllocation>();
            int indexSKU = 0;
            int numSKUs = skus.length;
            for (RIDAllocationRequest req : ridAllocationRequests) {
                RIDAllocation ridAllocation = new RIDAllocation();
                ridAllocation.setRid(req.getRid());
                ridAllocation.setUuid(req.getUuid());
                ridAllocation.setSlots(req.getSlots());
                if (indexSKU < numSKUs) {
                    int fulfilledSlots = req.getSlots() <= numSKUs - indexSKU ?
                                         req.getSlots() : numSKUs - indexSKU;
                    Set<String> allocatedSKUs = new HashSet<String>();
                    for (int i = 0; i < fulfilledSlots; i ++) {
                        allocatedSKUs.add(skus[indexSKU+i]);
                    }
                    ridAllocation.setAllocatedSKUs(allocatedSKUs);
                    ridAllocation.setFulfilledSlots(fulfilledSlots);
                    indexSKU += fulfilledSlots;
                }
                else {  // no more skus available for allocation
                    ridAllocation.setFulfilledSlots(0);
                    ridAllocation.setAllocatedSKUs(null);
                }
                ridAllocations.add(ridAllocation);
            }
        }
        this.ridAllocations = ridAllocations;
    }
    
    /**
     * @return the uuid
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * @param uuid the uuid to set
     */
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
    
    /**
     * @return the slots
     */
    public int getSlots() {
        return slots;
    }
    
    /**
     * @param slots the slots to set
     */
    public void setSlots(int slots) {
        this.slots = slots;
    }
    
    /**
     * @return the fulfilledSlots
     */
    public int getFulfilledSlots() {
        return fulfilledSlots;
    }
    
    /**
     * @param fulfilledSlots the fulfilledSlots to set
     */
    public void setFulfilledSlots(int fulfilledSlots) {
        this.fulfilledSlots = fulfilledSlots;
    }
 
    /**
     * @return the allocatedSKUs
     */
    public Set<String> getAllocatedSKUs() {
        return allocatedSKUs;
    }
    
    /**
     * @param allocatedSKUs the allocatedSKUs to set
     */
    public void setAllocatedSKUs(Set<String> allocatedSKUs) {
        this.allocatedSKUs = allocatedSKUs;
    }

    /**
     * @return the ridAllocations
     */
    public List<RIDAllocation> getRIDAllocations() {
        return ridAllocations;
    }
    
    /**
     * @param ridAllocations the ridAllocations to set
     */
    public void setRIDAllocations(List<RIDAllocation> ridAllocations) {
        this.ridAllocations = ridAllocations;
    }
    
}
