package com.gwynniebee.simulation.trace.service;

import java.io.InputStream;

import org.apache.commons.io.IOUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gwynniebee.async.service.api.JobOperationStatus;
import com.gwynniebee.async.service.api.JobTask;
import com.gwynniebee.async.service.client.AsyncServiceRestletClient;
import com.gwynniebee.simulation.trace.manager.SimulationTraceManagerProperties;

public class BaseAsyncJob {
    private static final Logger LOG = LoggerFactory.getLogger(BaseAsyncJob.class);
    protected static final SimulationTraceManagerProperties props = SimulationTraceManagerProperties.getInstance();
    String asyncUrl = null;
    String jobType = null;
    String jobName = null;
    
    public BaseAsyncJob(String asyncUrl, String jobType) {
        this(asyncUrl, jobType, jobType);
    }

    public BaseAsyncJob(String asyncUrl, String jobType, String jobName) {
        super();
        this.asyncUrl = asyncUrl;
        this.jobType = jobType;
        this.jobName = jobName;
    }

    protected void invokeExistingJobOnce() throws Exception {
        LOG.info("Scheduling to immediately run existing job {}.{}", this.jobType, this.jobName);   
        AsyncServiceRestletClient asyncClient = new AsyncServiceRestletClient(this.asyncUrl);
        JobOperationStatus result = asyncClient.scheduleJobOnceNow(this.jobType, this.jobName);
        LOG.info("Scheduled to immediately run existing job {}.{} with code {}: {}", this.jobType, this.jobName, result.getCode(), result.getMessage());   
        asyncClient.stop();
    }

    protected void scheduleOneTimeJob(JobTask jobTask) throws Exception {
        LOG.info("Scheduling to immediately run one time job {}", this.jobType);   
        AsyncServiceRestletClient asyncClient = new AsyncServiceRestletClient(this.asyncUrl);
        JobOperationStatus result = asyncClient.scheduleJob(this.jobType, jobTask);
        LOG.info("Scheduled to immediately run one time job {} with code {}: {}", this.jobType, result.getCode(), result.getMessage());   
        asyncClient.stop();
    }
    
    public JobTask readJob(String filename) {
        InputStream is = null;
        try {
            is = getClass().getResourceAsStream(filename);
            JobTask jobTask = new ObjectMapper().readValue(is, JobTask.class);
            return jobTask;
        } catch (Throwable e) {
            LOG.error("Cannot read JobTask json from " + filename, e);
            return null;
        } finally {
            if (is != null) {
                IOUtils.closeQuietly(is);
            }
        }
    }
}
