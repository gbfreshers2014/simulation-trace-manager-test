package com.gwynniebee.simulation.trace.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.gwynniebee.async.service.api.JobTask;
import com.gwynniebee.simulation.trace.store.AWSS3Client;

public class AllocationEngineJob extends BaseAsyncJob {
    private static final Logger LOG = LoggerFactory.getLogger(AllocationEngineJob.class);
    public static final String LATEST_MATCHING_RESULT_FILE_KEYS = "latestMatchingResultFileKeys";
    public static final String LATEST_MATCHING_SETUP_FILE_KEYS = "latestMatchingSetupFileKeys";

    private String resultObjectKeyBeforeRun;
    
    public AllocationEngineJob() {
        super(props.getAsyncServiceUrl(), props.getAllocationEngineJobName());
    }

    public void recordCurrentState() {
        AWSS3Client s3Client = new AWSS3Client();
        LOG.info("Record current state: " + props.getSimulationS3BucketName() +
                 "." + props.getSimulationS3ModuleName() + "/" + props.getNamespaceId() + "/" + 
                props.getSimulationS3SubModuleMatchingSetupName() + "/" + LATEST_MATCHING_SETUP_FILE_KEYS);
        try {
            this.resultObjectKeyBeforeRun = s3Client.getString(props.getSimulationS3BucketName(),
                            props.getSimulationS3ModuleName() + "/" + props.getNamespaceId() + "/" + 
                            props.getSimulationS3SubModuleMatchingResultName() + "/" + LATEST_MATCHING_RESULT_FILE_KEYS); 
        } catch (Exception e) {
            this.resultObjectKeyBeforeRun = "";
        }
    }

    public void scheduleOnce() throws Exception {
        JobTask jobTask = readJob("/allocation-engine-job.json");
        jobTask.getJobDefinition().getParamMap().put("namespaceId", String.valueOf(props.getNamespaceId()));
        super.scheduleOneTimeJob(jobTask);
    }

    public String waitForCompletion(long timeout) throws Exception {
        AWSS3Client s3Client = new AWSS3Client();

        long start = System.currentTimeMillis();
        String resultKey = null;
        do {
            if (System.currentTimeMillis() - start > timeout) {
                LOG.info("Allocation timeout with last result key: " + resultKey);        
                return null;
            }
            Thread.sleep(2000);
            try {
                resultKey = s3Client.getString(props.getSimulationS3BucketName(), 
                                props.getSimulationS3ModuleName() + "/" + props.getNamespaceId() + "/" + 
                                props.getSimulationS3SubModuleMatchingResultName() + "/" + LATEST_MATCHING_RESULT_FILE_KEYS);
            } catch (Exception e) {
                resultKey = null;
            }
        } while(resultKey == null || resultKey.equals(this.resultObjectKeyBeforeRun));
        
        LOG.info("Allocation done with last result key: " + resultKey);
        return resultKey;
    }
}
