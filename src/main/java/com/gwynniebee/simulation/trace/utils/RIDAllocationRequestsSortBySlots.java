package com.gwynniebee.simulation.trace.utils;

import java.util.Comparator;
import com.gwynniebee.matching.setup.RIDAllocationRequest;

public class RIDAllocationRequestsSortBySlots implements Comparator<RIDAllocationRequest>  {

    public int compare(RIDAllocationRequest item1, RIDAllocationRequest item2) {
        Integer i1 = new Integer(item1.getSlots());
        Integer i2 = new Integer(item2.getSlots());
        return i1.compareTo(i2);
    }

}

