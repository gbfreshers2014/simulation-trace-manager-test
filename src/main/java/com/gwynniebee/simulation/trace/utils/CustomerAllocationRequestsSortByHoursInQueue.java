package com.gwynniebee.simulation.trace.utils;

import java.util.Comparator;
import com.gwynniebee.simulation.trace.object.CustomerAllocationRequest;

public class CustomerAllocationRequestsSortByHoursInQueue implements Comparator<CustomerAllocationRequest>  {

    public int compare(CustomerAllocationRequest item1, CustomerAllocationRequest item2) {
        Integer i1 = new Integer(item1.getHoursInQueue());
        Integer i2 = new Integer(item2.getHoursInQueue());
        return i1.compareTo(i2);
    }

}

