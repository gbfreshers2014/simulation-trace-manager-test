#! /usr/bin/env python

"""Install script for email notifications"""

import sys
import os
import tempfile
from getopt import getopt, GetoptError

import logging as log

# Logger configuration.
log.basicConfig(level=log.INFO)

gb_home = "/home/gb"
config = gb_home + "/conf/simulation-trace-manager/simulation.trace.manager.properties"
simulation_runner = gb_home + "/bin/SimulationRunner.py"
simulation_log = "/tmp/logs/console.log"

def main():
        
    # run the command for the first time
    command = """%s -c %s >>%s 2>&1""" \
                % (simulation_runner, config, simulation_log)
    print command
    # os.system(command)

if __name__ == "__main__":
    main()
