#! /usr/bin/env python

import os
import subprocess
import sys
from getopt import getopt, GetoptError
from stat import S_ISREG, ST_CTIME, ST_MODE
import time
import logging as log

# Logger configuration.
log.basicConfig(level=log.INFO)

gb_home = "/home/gb"
simulation_trace_manager_config_file = gb_home + "/conf/simulation-trace-manager/simulation.trace.manager.properties"

jarFolder = "/home/gb/lib/simulation-trace-manager/lib";
classpath = "/home/gb/conf/simulation-trace-manager:/home/gb/conf/cred-res-aws:/home/gb/lib/simulation-trace-manager/lib";
for name in os.listdir(jarFolder):
    if name.endswith(".jar"):
        classpath = classpath + ":" + jarFolder + "/" + name;
main_class = "com.gwynniebee.simulation.trace.manager.SimulationTraceManager"
log.info("classpath = " + classpath)

def usage():
    """Prints out the usage"""
    print """
Usage: %s 

-c    simulation trace manager configuration file
        
""" % (sys.argv[0])

def get_options():
    """
    Parses command line options.
    
    @return: **options
    """
    # Required options.
    required = ["c"]
    
    # Get command line options.
    try:
        option_string = ''.join(map(lambda x: x + ":", required))
        options, [] = getopt(sys.argv[1:], option_string)
    except GetoptError:
        usage()
        exit(1)
    
    # Convert options to dict and validate.
    result = dict()
    for option in options:
        (opt, value) = option
        opt = opt[1:]
        if (opt in required):
            result[opt] = value
        else:
            usage()
            exit(1)
    
    # Verify that all mandatory parameters are provided.
    for option in required:
        if not result.has_key(option):
            usage()
            exit(1)
    
    return result

def parse_config_file(filename):
    """Parse configuration file and return a hash"""
    result = dict()
    file = open(filename, "rb")
    while True:
        line = file.readline()
        if not line:
            break
        line = line.rstrip("\r\n")
        if line != "":
            try: 
                [key, value] = line.split("=")
                result[key] = value
            except:
                log.info("skip the line " + line)
    return result

def main():
    """Main function"""
    options = get_options()
    config = parse_config_file(options["c"])
              
    log.info("Run simulation trace manager")
    command = """java -Xms256m -Xmx3072m -classpath %s %s -p %s""" % (classpath, main_class, simulation_trace_manager_config_file)
    log.info("Executing: " + command)
    result = os.system(command)
    if result != 0:
        log.error("Error: " + command + " failed")
        sys.exit(1)
               
    sys.exit(0)
    
if __name__ == "__main__":
    main()
